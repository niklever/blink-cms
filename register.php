<?
	require_once('connect.php');
	$msg = "";
	
	if (isset($_REQUEST['method'])){
		$email = $_REQUEST['email'];
		$username = $_REQUEST['username'];
		$password = $_REQUEST['password'];
		$sql = "SELECT id FROM users WHERE email='$email' AND username='$username'";
		$result = mysql_query($sql);
		if ($result){
			if (mysql_num_rows($result)>0){
				$msg = "A user with that email address and username already exists in the database. Please choose another email address";
			}else{
				$dot = strstr($email, ".");
				$at = strstr($email, "@");
				if ($email==""){
					$msg = "Please enter your email address";
				}else if (!$dot || !$at){
					$msg = "Please check your email address";
				}else if ($username=="" && strlen($username)>=6){
					$msg = "Please enter a username of at least 6 characters";
				}else if ($password=="" && strlen($password)>=6){
					$msg = "Please enter a password of at least 6 characters";
				}else{
					$sql = "INSERT INTO users (email, username, password) VALUES ('$email',  '$username', '$password')";
					$result = mysql_query($sql);
					$msg = "ok";
				}
			}
		}else{
			$msg = "Problem connecting to database";
		}
		
	}
	
	mysql_close($conn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/blink_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- Preview at https://64.34.168.15:8443/sitepreview/http/blinktrainingsystem.com -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Blink Admin - Register</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link href="main.css" rel="stylesheet" type="text/css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
  $(function() {
    $( document ).tooltip();
  });
  </script>
<!-- InstanceBeginEditable name="head" -->
<script>
$(document).ready(function() {
	$(window).resize(function() {
		windowResized();
    });
	
	$(window).trigger('resize');
});

function windowResized(){
	var height = $(window).height() - $('.header').height() - $('.footer').height();
    $('.content').height(height);
	$('.sidebar1').height(height);
}
</script>
<!-- InstanceEndEditable -->
</head>

<body>
<div class="container"> 
  <div class="header"><span class="heading">Blink Training System - CMS <!-- InstanceBeginEditable name="PageName" -->- Register<!-- InstanceEndEditable --></span>
    <!-- end .header --></div>
  <div class="sidebar1">
    <ul class="nav">
      <!-- InstanceBeginEditable name="sidebar" -->
      <li><a href="login.php">Login</a></li>
      <li><a href="register.php">Register</a></li>
      <!-- InstanceEndEditable -->
    </ul>
    <!-- end .sidebar1 --></div>
  <div class="content">
    <!-- InstanceBeginEditable name="content" -->
    <div id="content-inset">
    <h1>Register</h1>
    <?
		if (isset($result) && $result && $msg=="ok"){
			echo 'Thanks for registering. Use the login button to continue. You must be an admin user to use most parts of the CMS.';
		}else{
			if ($msg!="") echo $msg.'<br><br>';
    		echo '<form action="register.php" method="post">';
    		echo '<input name="method" type="hidden" value="1" /></td></tr>';
			echo '<table>';
			echo '<tr><td class="left">Email</td><td><input name="email" type="text" size="50" maxlength="100" /></td></tr>';
			echo '<tr><td class="left">Username</td><td><input name="username" type="text" size="50" maxlength="100" /></td></tr>';
			echo '<tr><td class="left">Password</td><td><input name="password" type="text" size="50" maxlength="100" /></td></tr>';
			echo '<tr><td>&nbsp;</td><td class="left"><input name="submit" type="submit"/></td></tr>';
			echo '</table>';
			echo '</form>';
		}
	?>
    </div>
    <!-- InstanceEndEditable -->
    <!-- end .content --></div>
  <div class="footer">
    Copyright © 2014 by Blink Training Systems, LLC
    <!-- end .footer --></div> 
  <!-- end .container --></div> 
</body>
<!-- InstanceEnd --></html>
