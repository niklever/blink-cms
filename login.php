<?
	require_once('connect.php');
	$msg = "";
	$loggedIn = false;
	
	if (isset($_REQUEST['method'])){
		$email = $_REQUEST['email'];
		$password = $_REQUEST['password'];
		$sql = "SELECT * FROM users WHERE email='$email' AND password='$password'";
		$result = mysql_query($sql);
		if ($result){
			if (mysql_num_rows($result)>0){
				$row = mysql_fetch_assoc($result);
				$admin = $row['admin'];
				$msg = 'ok';
				session_start();
				ini_set("session.cookie_lifetime","7200");
				$_SESSION['loggedIn'] = true;
				$_SESSION['admin'] = $admin;
				$_SESSION['name'] = $row['firstname'];
				$name = $_SESSION['name'];
				$loggedIn = true;
			}else{
				$msg = "The user cannot be found";
			}
		}else{
			$msg = "Problem connecting to database";
		}
	}
	
	mysql_close($conn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/blink_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- Preview at https://64.34.168.15:8443/sitepreview/http/blinktrainingsystem.com -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Blink Admin - Login</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link href="main.css" rel="stylesheet" type="text/css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
  $(function() {
    $( document ).tooltip();
  });
  </script>
<!-- InstanceBeginEditable name="head" -->
<script>
$(document).ready(function() {
	var d = new Date();	
	var timeoffset = d.getTimezoneOffset();
	$.ajax({
		type: "GET",
		url: "set_time_offset.php",
		data: 'timeoffset='+ timeoffset
	});
		
	$(window).resize(function() {
		windowResized();
    });
	
	$(window).trigger('resize');
});

function windowResized(){
	var height = $(window).height() - $('.header').height() - $('.footer').height();
    $('.content').height(height);
	$('.sidebar1').height(height);
}
</script>
<!-- InstanceEndEditable -->
</head>

<body>
<div class="container"> 
  <div class="header"><span class="heading">Blink Training System - CMS <!-- InstanceBeginEditable name="PageName" -->- Login<!-- InstanceEndEditable --></span>
    <!-- end .header --></div>
  <div class="sidebar1">
    <ul class="nav">
      <!-- InstanceBeginEditable name="sidebar" -->
      <?
	  	if ($loggedIn){
			echo '<li><a href="logout.php">Logout ('.$name.')</a></li>';
      		echo '<li><a href="courses.php">Courses</a></li>';
		}else{
      		echo '<li><a href="login.php">Login</a></li>';
      		echo '<li><a href="register.php">Register</a></li>';
		}
	  ?>
      <!-- InstanceEndEditable -->
    </ul>
    <!-- end .sidebar1 --></div>
  <div class="content">
    <!-- InstanceBeginEditable name="content" -->
    <?
    	echo '<div id="content-inset">';
		if (isset($result) && $result && $msg=="ok"){
			//Logged in
			echo "<h1>Welcome $name</h1>";
			echo 'Use the buttons on the left to manage courses.';
		}else{
			echo '<h1>Login</h1>';
			if ($msg!="") echo $msg.'<br><br>';
    		echo '<form action="login.php" method="post">';
    		echo '<input name="method" type="hidden" value="1" /></td></tr>';
			echo '<table>';
			echo '<tr><td class="left">Email</td><td><input name="email" type="text" size="50" maxlength="100" /></td></tr>';
			echo '<tr><td class="left">Password</td><td><input name="password" type="text" size="50" maxlength="100" /></td></tr>';
			echo '<tr><td>&nbsp;</td><td class="left"><input name="submit" type="submit"/></td></tr>';
			echo '</table>';
			echo '</form>';
		}
		echo '</div>';
	?>
    <!-- InstanceEndEditable -->
    <!-- end .content --></div>
  <div class="footer">
    Copyright © 2014 by Blink Training Systems, LLC
    <!-- end .footer --></div> 
  <!-- end .container --></div> 
</body>
<!-- InstanceEnd --></html>
