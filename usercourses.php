<? 
	require_once('connect.php');
	$msg = "";
	
	session_start();
	$loggedIn = $_SESSION['loggedIn'];
	$admin = $_SESSION['admin'];
	$name = $_SESSION['name'];
	
	if (isset($loggedIn) && $loggedIn && $admin){
		$sql = "SELECT * FROM usercourse";
		$result = mysql_query($sql);
		if ($result){
			$data = array();
			while($row=mysql_fetch_assoc($result)){
				$userId = $row['userId'];
				$sql = "SELECT firstname, lastname FROM users WHERE id=$userId";
				$resultb = mysql_query($sql);
				if ($resultb){
					$courseId = $row['courseId'];
					$row = mysql_fetch_assoc($resultb);
					$user = $row['firstname'].' '.$row['lastname'];
					$sql = "SELECT title FROM course WHERE id=$courseId";
					$resultb = mysql_query($sql);
					if ($resultb){
						$row = mysql_fetch_row($resultb);
						$data[] = array("user"=>$user, "course"=>$row[0]);
					}else{
						$data[] = array("user"=>$user, "course"=>"Problem accessing course name");
					}
				}else{
					$data[] = array("user"=>"Problem accessing user name", "course"=>"");
				}
			}
		}else{
			echo 'Problem accessing database';
		}
	}
	
	mysql_close($conn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/blink_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- Preview at https://64.34.168.15:8443/sitepreview/http/blinktrainingsystem.com -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Blink Admin - User Courses</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link href="main.css" rel="stylesheet" type="text/css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
  $(function() {
    $( document ).tooltip();
  });
  </script>
<!-- InstanceBeginEditable name="head" -->
<script src="js/jquery.form.js"></script> 
<!-- InstanceEndEditable -->
</head>

<body>
<div class="container"> 
  <div class="header"><span class="heading">Blink Training System - CMS <!-- InstanceBeginEditable name="PageName" -->- <? echo $course['title']; ?>( <? echo $course['id']; ?> )<!-- InstanceEndEditable --></span>
    <!-- end .header --></div>
  <div class="sidebar1">
    <ul class="nav">
      <!-- InstanceBeginEditable name="sidebar" -->
      <?
	  	if ($loggedIn){
			echo '<li><a href="logout.php">Logout <span class="small">('.$name.')</span></a></li>';
      		echo '<li><a href="courses.php">Courses</a></li>';
			echo '<li><a href="usercourses.php">User Courses</a></li>';
		}else{
      		echo '<li><a href="login.php">Login</a></li>';
      		echo '<li><a href="register.php">Register</a></li>';
		}
	  ?>
      <!-- InstanceEndEditable -->
    </ul>
    <!-- end .sidebar1 --></div>
  <div class="content">
    <!-- InstanceBeginEditable name="content" -->
    <?
    	echo '<div id="content-inset">';
		if (!$loggedIn){
			//Logged in
			echo '<div id="msg" class="error" style="clear:both;">You need to be logged in to view this page.</div></div>';
		}else{
			echo '<table>';
			foreach($data as $item){
				echo '<tr><td>'.$item['user'].'</td><td>'.$item['course'].'</td></tr>';
			}
			echo '</table>';
		}
	?>
    
    <!-- InstanceEndEditable -->
    <!-- end .content --></div>
  <div class="footer">
    Copyright © 2014 by Blink Training Systems, LLC
    <!-- end .footer --></div> 
  <!-- end .container --></div> 
</body>
<!-- InstanceEnd --></html>
