<?
	require_once('connect.php');
	$msg = "";
	
	session_start();
	$loggedIn = $_SESSION['loggedIn'];
	$admin = $_SESSION['admin'];
	$name = $_SESSION['name'];
	$course;
	
	if (isset($loggedIn) && $loggedIn && $admin){
		$method = $_REQUEST['method'];
		$id = $_REQUEST['id'];
		if (isset($method)){
			
		}
			
		$sql = "SELECT * FROM blink WHERE id=$id";
		$result = mysql_query($sql);
		$blink = mysql_fetch_assoc($result);	
	}
		
	mysql_close($conn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/blink_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- Preview at https://64.34.168.15:8443/sitepreview/http/blinktrainingsystem.com -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Blink</title>
<script src="js/course.js"></script>
<script src="js/blink_gui.js"></script>
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link href="main.css" rel="stylesheet" type="text/css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
  $(function() {
    $( document ).tooltip();
  });
  </script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div class="container"> 
  <div class="header"><span class="heading">Blink Training System - CMS <!-- InstanceBeginEditable name="PageName" -->PageName<!-- InstanceEndEditable --></span>
    <!-- end .header --></div>
  <div class="sidebar1">
    <ul class="nav">
      <!-- InstanceBeginEditable name="sidebar" -->
      <?
	  	if ($loggedIn){
			echo '<li><a href="logout.php">Logout <span class="small">('.$name.')</span></a></li>';
      		echo '<li><a href="courses.php">Courses</a></li>';
		}else{
      		echo '<li>You need to be logged in to use this page</li>';
      		echo '<li><a href="register.php">Register</a></li>';
		}
	  ?>
      <!-- InstanceEndEditable -->
    </ul>
    <!-- end .sidebar1 --></div>
  <div class="content">
    <!-- InstanceBeginEditable name="content" -->
    <?
    	if ($loggedIn){
			echo '<a href="course.php?id='.$_REQUEST['id'].'">Back</a>';
			echo '<table width="90%">';
    		echo '<tr><td class="left" width="20%">Type</td><td>Type</td><td width="20%" class="buttons" title="Save changes">';
    		echo '<img src="images/btn_prev.png" />';
    		echo '<img src="images/btn_next.png" />';
    		echo '<img src="images/btn_save.png" />';
    		echo '</td></tr>';
    		echo '<tr><td class="left" width="20%">Summary</td><td colspan="2"><input id="summary" type="text" value="This is the summary" style="width:100%;"/></td></tr>';
    		echo '</table>';
		}else{
      		echo '<div id="msg" class="error">You need to be logged in to view this page.</div>';
		}
	?>
    
    
    <!-- InstanceEndEditable -->
    <!-- end .content --></div>
  <div class="footer">
    Copyright © 2014 by Blink Training Systems, LLC
    <!-- end .footer --></div> 
  <!-- end .container --></div> 
</body>
<!-- InstanceEnd --></html>
