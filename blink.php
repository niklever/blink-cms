<?
	require_once('connect.php');
	$msg = "";
	
	session_start();
	$loggedIn = $_SESSION['loggedIn'];
	$admin = $_SESSION['admin'];
	$name = $_SESSION['name'];
	$course;
	
	if (isset($loggedIn) && $loggedIn && $admin){
		$method = $_REQUEST['method'];
		
		if (isset($method)){
			switch($method){
				case 'prev':
				case 'next':
				$sql = "SELECT * FROM blink WHERE courseId=".$_REQUEST['courseId']." AND `index`=".$_REQUEST['index'];
				break;
			}
		}else{
			$id = $_REQUEST['id'];
			$sql = "SELECT * FROM blink WHERE id=$id";
		}
		
		$result = mysql_query($sql);
		$blink = mysql_fetch_assoc($result);
		
		$sql = "SELECT COUNT(id) FROM blink WHERE courseId=".$blink['courseId'];
		$result = mysql_query($sql);
		
		//Subtract last folder end blinks from the count total
		if ($result){
			$row = mysql_fetch_row($result);
			$count = $row[0];
			$first = true;
			do{
				if (!$first) $count--;
				$sql = "SELECT type FROM blink WHERE courseId=".$blink['courseId']." AND `index`=".($count-1);
				$result = mysql_query($sql);
				if (!$result) break;
				$row = mysql_fetch_row($result);
				$type = $row[0];		
				$first = false;	
			}while(($type==2 || $type==4) && $count>0);
		}
		
		//Skip folder end
		while ($blink['type']==2 || $blink['type']==4){
			if ($method=='prev'){
				$index--;
			}else if ($method=='next'){
				$index++;
			}else{
				break;
			}
			$sql = "SELECT * FROM blink WHERE courseId=".$blink['courseId']." AND `index`=".$index;
			$result = mysql_query($sql);
			$blink = mysql_fetch_assoc($result);
			if ($index>=$count) break;
		}
		
		//Get a list of all summaries and guid
		$sql = "SELECT summary, guid, type FROM blink WHERE courseid=".$blink['courseId']." ORDER BY `index` ASC";
		$result = mysql_query($sql);
		
		$blinks = array();
		
		if ($result){
			while($row=mysql_fetch_assoc($result)) $blinks[] = $row;
		}
	}
		
	mysql_close($conn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/blink_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- Preview at https://64.34.168.15:8443/sitepreview/http/blinktrainingsystem.com -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Blink</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link href="main.css" rel="stylesheet" type="text/css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
  $(function() {
    $( document ).tooltip();
  });
  </script>
<!-- InstanceBeginEditable name="head" -->
<script src="js/course.js"></script>
<script src="js/blink_gui.js"></script>
<script>
var index = <? echo $blink['index']; ?>;
var blinkTotal = <? echo $count; ?>;
var courseId = <? echo $blink['courseId']; ?>;
var id = <? echo $blink['id']; ?>;
var type = <? echo $blink['type']; ?>;
var blink = JSON.parse('<? echo $blink['json']; ?>');
var blinks = new Array();
var yesNoFunc = null;
var data;
var loggedIn = <? echo $loggedIn; ?>;

<?
	//Insert the blinks into a javascript array so we can use them to assign links.
	foreach($blinks as $tmp){
		echo 'blinks.push({ summary:"'.$tmp['summary'].'", guid:"'.$tmp['guid'].'", type:'.$tmp['type'].'});'."\n";
	}
?>

$(document).ready(function() {
	var icons = ['icon_learning_start', 'icon_learning_end', 'icon_burst_start', 'icon_burst_end', 'icon_menu', 	'icon_blink'];
	var type = <? echo $blink['type']; ?>;
	var icon = (type<icons.length) ? icons[type-1] : 'icon_blink';
	var str = "<img src=\"images/" + icon + ".png\" />&nbsp;" + Blink.types[type - 1];
	console.log("Ready type html: " + str);
	$('#type').html(str);
	
	$("#summary").keyup(function(){
        $("#summary").blur();
        $("#summary").focus();
	});
	
	$('#summary').change(function(){
		var summary = $('#summary')[0].value;
		console.log("Summary: " + summary);
		blinkModified = true;
		updateBlinkButtons();
	});
	
	$( "#blink-feedback" ).dialog({
      height: 240,
	  resizable: false,
	  autoOpen: false,
      modal: true
    });
	
	if (loggedIn){
		 $('.sidebar1').css('display', 'none');
		 $('.content').css('width', '95%');
	}
	
	/*$("#blink-confirm-yesno").dialog({ 
      height: 240,
	  resizable: false,
	  autoOpen: false,
      modal: true,
	  buttons: {
        "Yes": function() {
			if (yesNoFunc!=null) yesNoFunc();
          	$( this ).dialog( "close" );
        },
		"No": function() {
          	$( this ).dialog( "close" );
        },
        Cancel: function() {
          	$( this ).dialog( "close" );
        }
    });*/
	
	$( "#blink-confirm-yesno" ).dialog({
      resizable: false,
      height:240,
      modal: true,
	  autoOpen: false,
      buttons: {
        "Yes": function() {
			if (yesNoFunc!=null) yesNoFunc();
          	$( this ).dialog( "close" );
        },
		"No": function() {
          	$( this ).dialog( "close" );
        },
        Cancel: function() {
			blinkIdx = -1;
          	$( this ).dialog( "close" );
        }
      }
    });
	
	switch(type){
		case 5://Menu
		showMenuBlink();
		break;
		case 6://Presentation
		showPresentationBlink();
		break;
		case 7://Input
		showInputBlink();
		break;
	}
	
	updateBlinkButtons();
});
</script>
<!-- InstanceEndEditable -->
</head>

<body>
<div class="container"> 
  <div class="header"><span class="heading">Blink Training System - CMS <!-- InstanceBeginEditable name="PageName" -->PageName<!-- InstanceEndEditable --></span>
    <!-- end .header --></div>
  <div class="sidebar1">
    <ul class="nav">
      <!-- InstanceBeginEditable name="sidebar" -->
      <?
	  	if ($loggedIn){
			echo '<li><a href="logout.php">Logout <span class="small">('.$name.')</span></a></li>';
      		echo '<li><a href="courses.php">Courses</a></li>';
		}else{
      		echo '<li><a href="login.php">Login</a></li>';
      		echo '<li><a href="register.php">Register</a></li>';
		}
	  ?>
      <!-- InstanceEndEditable -->
    </ul>
    <!-- end .sidebar1 --></div>
  <div class="content">
    <!-- InstanceBeginEditable name="content" -->
    <?
    	if ($loggedIn){
			echo '<a href="course.php?id='.$blink['courseId'].'">Back</a>';
			echo '<table width="100%">';
    		echo '<tr><td class="left" width="20%">Type</td><td><div id="type"></div></td><td width="20%" class="buttons" >';
    		echo '<a href="#"><img id="btn_prev" class="button" src="images/btn_prev.png" title="Prev Blink" onclick="prevBlink()"/></a>';
    		echo '<a href="#"><img id="btn_next" class="button" src="images/btn_next.png" title="Next Blink" onclick="nextBlink()"/></a>';
    		echo '<a href="#"><img id="btn_save" class="button" src="images/btn_save.png" title="Save changes" onclick="saveBlink()"/></a>';
    		echo '</td></tr>';
    		echo '<tr><td class="left" width="20%">Summary</td><td colspan="2"><input id="summary" type="text" value="'.$blink['summary'].'" style="width:90%;"/></td></tr>';
			echo '<tr><td colspan="3"></td></tr>';
    		echo '</table>';
			echo '<div id="blink-content"><table width="100%"><tr><td class="left" width="20%">Content</td><td colspan="2">This will be type specific content</td></tr></table></div>';
			echo '<div id="blink-confirm-yesno" title="Confirm">';
			echo '	<div id="blink-confirm-yesno-msg">msg</div>';
			echo '</div>';
			echo '<div id="blink-feedback" title="Blink saved">';
			echo '	<div id="blink-feedback-msg">msg</div>';
			echo '</div>';
		}else{
      		echo '<div id="msg" class="error">You need to be logged in to view this page.</div>';
		}
	?>
    <!--<table width="90%">
    <tr><td class="left" width="20%">Add</td>
    <td>
    <select id="menu-type" style="width:100%;">
    <option>Select menu type</option>
    <option>Learning units</option>
    <option>Growing Activities</option>
    <option>Burst</option>
    <option>Personal Dossiers</option>
    </select>
    <td class="buttons"><img src="images/btn_create.png" title="Create" onclick="createMenu()" /></td>
    </tr>
    </table>-->
    
    <!-- InstanceEndEditable -->
    <!-- end .content --></div>
  <div class="footer">
    Copyright © 2014 by Blink Training Systems, LLC
    <!-- end .footer --></div> 
  <!-- end .container --></div> 
</body>
<!-- InstanceEnd --></html>
