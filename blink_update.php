<?
require_once('connect.php');
	
$msg = "";

session_start();
$loggedIn = $_SESSION['loggedIn'];
$admin = $_SESSION['admin'];

if (isset($loggedIn) && $loggedIn && $admin){
	switch($_REQUEST['method']){
		case 'conversation_image':
		case 'indexed_image':
		$path = saveImage($_FILES['image']);
		if ($path!=""){
			$msg = '{"success":true, "path":"'.$path.'", "id":'.$_REQUEST['id'].'}';
		}else{
			$msg = '{"success":false, "msg":"'.$sql.'"}';
		}
		break;
		case 'intro_image':
		$path = saveImage($_FILES['image']);
		if ($path!=""){
			$msg = '{"success":true, "path":"'.$path.'"}';
		}else{
			$msg = '{"success":false, "msg":"'.$sql.'"}';
		}
		break;
		case 'drag_image':
		$path = saveImage($_FILES['image']);
		if ($path!=""){
			$msg = '{"success":true, "path":"'.$path.'", "goal":'.$_REQUEST['goal'].', "index":'.$_REQUEST['index'].'}';
		}else{
			$msg = '{"success":false, "msg":"'.$sql.'"}';
		}
		break;
		case 'ebook':
		$path = saveEbook($_FILES['ebook']);
		if ($path!=""){
			$msg = '{"success":true, "path":"'.$path.'"}';
		}else{
			$msg = '{"success":false, "msg":"'.$sql.'"}';
		}
		break;
		case 'sound':
		$path = saveSound($_FILES['sound']);
		if ($path!=""){
			$id = (isset($_REQUEST['id'])) ? $_REQUEST['id'] : 0;
			$msg = '{"success":true, "id":'.$id.', "path":"'.$path.'"}';
		}else{
			$msg = '{"success":false, "msg":"'.$sql.'"}';
		}
		break;
		case 'update':
		$str = mysql_real_escape_string($_REQUEST['summary']);
		$json = json_encode($_REQUEST['json']);
		while(substr($json, 0, 1)!="{" && strlen($json)>0) $json = substr($json, 1);
		while(substr($json, strlen($json)-1, 1)!="}" && strlen($json)>0) $json = substr($json, 0, strlen($json)-1);
		$json = mysql_real_escape_string($json);
		if (isset($_REQUEST['id'])){
			$sql = "UPDATE blink SET summary='$str', json='$json' WHERE id=".$_REQUEST['id'];
		}else{
			$courseId = $_REQUEST['courseId'];
			$type = $_REQUEST['type'];
			$json = mysql_real_escape_string($_REQUEST['json']);
			$summary = mysql_real_escape_string($_REQUEST['summary']);
			$index = $_REQUEST['index'];
			$growth = $_REQUEST['growth'];
			$sql = "INSERT INTO blink (courseId, type, summary, json, guid, `index`, growth_activity) VALUES($courseId, $type, '$summary', '$json', '$guid', $index, $growth)"; 
		}
		//echo $sql;
		break;
		case 'copy':
		$id = $_REQUEST['id'];
		$guid = $_REQUEST['guid'];
		$sql = "SELECT * FROM blink WHERE id=$id";
		$result = mysql_query($sql);
		if ($result){
			$row = mysql_fetch_assoc($result);
			$courseId = $row['courseId'];
			$type = $row['type'];
			$json = mysql_real_escape_string($row['json']);
			//while(substr($json, 0, 1)!="{" && strlen($json)>0) $json = substr($json, 1);
			//while(substr($json, strlen($json)-1, 1)!="}" && strlen($json)>0) $json = substr($json, 0, strlen($json)-1);
			//$json = str_replace("\\", "", $json);
			//$json = mysql_real_escape_string($json);
			//echo $json;
			$summary = mysql_real_escape_string($row['summary']);
			$index = $_REQUEST['index'];
			$sql = "INSERT INTO blink (courseId, type, summary, json, guid, `index`) VALUES($courseId, $type, '$summary', '$json', '$guid', $index)";
			$sql = mysql_real_escape_string($sql);
			$result = mysql_query($sql);
			if ($result){
				$id = mysql_insert_id();
				$msg = '{"msg":"Blink copied", "id":'.$id.'}';
			}else{
				$sql = str_replace('`', "", $sql);
				$msg = '{"msg":"Problem copying blink '.$sql.'"}';
			}
		}else{
			$msg = '{"msg":"Problem selecting blink '.$sql.'"}';
		}
		break;
		case 'get_summary':
		$id = $_REQUEST['id'];
		$sql = "SELECT summary FROM blink WHERE id=$id";
		$result = mysql_query($sql);
		if ($result){
			$row = mysql_fetch_row($result);
			$msg = '{"success":true, "msg":"Success", "summary":"'.$row[0].'"}';
		}else{
			$msg = '{"success":false, "msg":"Problem '.$sql.'"}';
		}
		break;
		case 'get':
		$id = $_REQUEST['id'];
		$sql = "SELECT * FROM blink WHERE id=$id";
		$result = mysql_query($sql);
		if ($result){
			$row = mysql_fetch_assoc($result);
			$msg = '{"success":true, "msg":"Success", "type":'.$row['type'].', "summary":"'.$row['summary'].'", "lite":'.$row['lite'].', "guid":"'.$row['guid'].'", "id":'.$row['id'].', "json":"'.$row['json'].'"}';
		}else{
			$msg = '{"success":false, "msg":"Problem '.$sql.'"}';
		}
		break;
	}
	if ($msg==""){
		$result = mysql_query($sql);
		if (isset($_REQUEST['id'])){
			$msg = ($result) ? "Update successful" : ("Update failed type:".$_REQUEST['type']." sql:".$sql);
		}else if ($result){
			$id = mysql_insert_id();
			$msg = '{"success":true, "msg":"Insert successful", "id":'.$id.'}';
		}else{
			$msg = '{"success":false, "msg":"'.$sql.'"}';
		}
	}
}else{
	$msg = 'User not logged in or not admin';
}
	
mysql_close($conn);

echo $msg;

function saveEbook($data){
	if (empty($data['tmp_name'])) return;
	$uploads_dir = '../courses/pdfs';
	$tmp_name = $data["tmp_name"];
	$name = trim(guid(), '{}');
	move_uploaded_file($tmp_name, "$uploads_dir/$name.pdf");
	return $name.'.pdf';
}

function saveSound($data){
	if (empty($data['tmp_name'])) return;
	$uploads_dir = '../courses/sounds';
	$tmp_name = $data["tmp_name"];
	$name = trim(guid(), '{}');
	move_uploaded_file($tmp_name, "$uploads_dir/$name.mp3");
	return $name.'.mp3';
}

function saveImage($data){
	if (empty($data['tmp_name'])) return;
	$uploads_dir = '../courses/images';
	$tmp_name = $data["tmp_name"];
	$name = trim(guid(), '{}');
	move_uploaded_file($tmp_name, "$uploads_dir/$name.png");
	return $name.'.png';
}

function guid(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }else{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12)
                .chr(125);// "}"
        return $uuid;
    }
}
?>