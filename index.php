<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/blink_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- Preview at https://64.34.168.15:8443/sitepreview/http/blinktrainingsystem.com -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Blink Admin - Login</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link href="main.css" rel="stylesheet" type="text/css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
  $(function() {
    $( document ).tooltip();
  });
  </script>
<!-- InstanceBeginEditable name="head" -->
<script>
$(document).ready(function() {
	$(window).resize(function() {
		windowResized();
    });
	
	$(window).trigger('resize');
});

function windowResized(){
	var height = $(window).height() - $('.header').height() - $('.footer').height();
    $('.content').height(height);
	$('.sidebar1').height(height);
}
</script>
<!-- InstanceEndEditable -->
</head>

<body>
<div class="container"> 
  <div class="header"><span class="heading">Blink Training System - CMS <!-- InstanceBeginEditable name="PageName" -->- Home<!-- InstanceEndEditable --></span>
    <!-- end .header --></div>
  <div class="sidebar1">
    <ul class="nav">
      <!-- InstanceBeginEditable name="sidebar" -->
      <li><a href="login.php">Login</a></li>
      <li><a href="register.php">Register</a></li>
      <!-- InstanceEndEditable -->
    </ul>
    <!-- end .sidebar1 --></div>
  <div class="content">
    <!-- InstanceBeginEditable name="content" -->
    <div id="content-inset">
        <h1>Intro</h1>
        <p>Login or register to continue</p>  
    </div>
    <!-- InstanceEndEditable -->
    <!-- end .content --></div>
  <div class="footer">
    Copyright © 2014 by Blink Training Systems, LLC
    <!-- end .footer --></div> 
  <!-- end .container --></div> 
</body>
<!-- InstanceEnd --></html>
