<?
	require_once('connect.php');
	require_once('simplejson.php');
	
	$msg = "";
	
	session_start();
	$loggedIn = $_SESSION['loggedIn'];
	$admin = $_SESSION['admin'];
	
	if (isset($loggedIn) && $loggedIn && $admin){
		switch($_REQUEST['type']){
			case 'title':
			$str = mysql_real_escape_string($_REQUEST['value']);
			$sql = "UPDATE course SET title='$str' WHERE id=".$_REQUEST['id'];
			break;
			case 'summary':
			$str = mysql_real_escape_string($_REQUEST['value']);
			$sql = "UPDATE course SET summary='$str' WHERE id=".$_REQUEST['id'];
			break;
			case 'description':
			$str = mysql_real_escape_string($_REQUEST['value']);
			$sql = "UPDATE course SET description='$str' WHERE id=".$_REQUEST['id'];
			break;
			case 'image':
			$path = saveImage('image', $_FILES['image'], $_REQUEST['id']);
			if ($path!=""){
				$sql = "SELECT imageURL FROM course WHERE id=".$_REQUEST['id'];
				$result = mysql_query($sql);
				if (!$result){
					$msg = 'Problem retrieving imageURL '.$sql;
				}else{
					$row = mysql_fetch_assoc($result);
					if (isset($row['imageURL'])){
						$sql = "UPDATE course SET imageURL='".$path."' WHERE id=".$_REQUEST['id'];
					}else{
						$sql = "INSERT INTO course (imageURL) VALUES('".$path.")' WHERE id=".$_REQUEST['id'];
					}
				}
			}
			break;
			case 'icon':
			$path = saveImage('icon', $_FILES['icon'], $_REQUEST['id']);
			if ($path!=""){
				$sql = "SELECT iconURL FROM course WHERE id=".$_REQUEST['id'];
				$result = mysql_query($sql);
				if (!$result){
					$msg = 'Problem retrieving iconURL '.$sql;
				}else{
					$row = mysql_fetch_assoc($result);
					if (isset($row['iconURL'])){
						$sql = "UPDATE course SET iconURL='".$path."' WHERE id=".$_REQUEST['id'];
					}else{
						$sql = "INSERT INTO course (iconURL) VALUES('".$path.")' WHERE id=".$_REQUEST['id'];
					}
				}
			}
			break;
			case 'new_blink':
			$courseId = $_REQUEST['courseId'];
			$type = $_REQUEST['type'];
			$summary = mysql_real_escape_string($_REQUEST['summary']);
			$index = $_REQUEST['index'];
			$sql = "INSERT INTO blink ( courseId, type, summary, index ) VALUES ( $courseId, $type, '$summary', $index )";
			$result = mysql_query($sql);
			$id = mysql_insert_id();
			$msg = "{\"id\":$id}";
			break;
			case 'update_blink':
			$id = $_REQUEST['id'];
			$summary = mysql_real_escape_string($_REQUEST['summary']);
			$index = $_REQUEST['index'];
			$lite = $_REQUEST['lite'];
			$json = mysql_real_escape_string($_REQUEST['json']);
			$sql = "UPDATE blink SET summary='$summary', index=$index, lite=$lite, json='$json'";
			break;
			case 'publish':
			$id = $_REQUEST['id'];
			$index = $_REQUEST['index'];
			$mode = $_REQUEST['mode'];
			if ($mode=='true'){
				$sql = "UPDATE course SET published=1 WHERE id=$id";
			}else{
				$sql = "UPDATE course SET published=0 WHERE id=$id";
			}
			mysql_query($sql);
			if ($mode=='true'){
				$msg = '{ "success":true, "mode":false, "id":'.$id.', "tmp":"'.$mode.'", "index":'.$index.', "sql":"'.$sql.'"}';
			}else{
				$msg = '{ "success":true, "mode":true, "id":'.$id.', "tmp":"'.$mode.'", "index":'.$index.', "sql":"'.$sql.'"}';
			}
			break;
			case 'course':
			updateCourse();
			break;
			case 'duplicate_course':
			duplicateCourse();
			break;
			case 'build_course':
			$msg = buildCourse(true);
			break;
			case 'build_unit':
			$msg = buildCourse(false);
			break;
		}
		if (isset($path)){
			//var_dump($_FILES);
			$result = mysql_query($sql);
			$msg = ($path!="" && $result) ? "{ \"success\":true, \"path\":\"$path\", \"msg\":\"$msg\" }" : "{ \"success\":false, \"type\":\"".$_REQUEST['type']."\", \"error\":\"".$msg."\"}";
		}else if ($msg==""){
			$result = mysql_query($sql);
			$msg = ($result) ? "Update successful" : ("Update failed type:".$_REQUEST['type']." sql:".$sql);
		}
	}else{
		$msg = 'User not logged in or not admin';
	}
		
	mysql_close($conn);
	
	echo $msg;

function buildCourse($full){ //full:true - course build, false - unit build
	global $create_msg;
	
	$create_msg = "";
	$courseId = $_REQUEST['courseId'];
	if (empty($courseId)) return '{"success":false, "error":"no course id"}';
	if (!$full){
		$index = $_REQUEST['index'];
		if (empty($index)){
			return '{"success":false, "error":"No blink index supplied for sub course"}';
		}
	}
	$sql = "SELECT * FROM course WHERE id=$courseId";
	$result = mysql_query($sql);
	$course = mysql_fetch_assoc($result);
	$sql = "SELECT * FROM blink WHERE courseId=$courseId ORDER BY `index` ASC";
	$result = mysql_query($sql); 
	
	$blinks = array();
	$lite_blinks = array();
	$growth = array();
	$lite_growth = array();
	
	if ($result){
		$i = 0;
		while($row=mysql_fetch_assoc($result)){
			if ($full){
				if ($row['growth_activity']){
					$growth[] = $row;
					if ($row['lite']) $lite_growth[] = $row;
				}else{
					$blinks[] = $row;
					if ($row['lite']) $lite_blinks[] = $row;
				}
			}else if ($i==$index){
				$type = $row['type'];
				$endtype = $type + 2;
				if ($row['growth_activity']){
					$growth[] = $row;
					if ($row['lite']) $lite_growth[] = $row;
				}else{
					$blinks[] = $row;
					if ($row['lite']) $lite_blinks[] = $row;
				}
			}else if ($i>$index){
				if ($row['growth_activity']){
					$growth[] = $row;
					if ($row['lite']) $lite_growth[] = $row;
				}else{
					$blinks[] = $row;
					if ($row['lite']) $lite_blinks[] = $row;
				}
				if ($row['type']==$endtype) break;
			}
			$i++;
		}
		//var_dump($blinks);
		//$create_msg .= count($blinks).' blinks '.count($lite_blinks).' lite blinks ';
		$files = array();
		
		if (count($blinks)>0){	
			$files = createFileList($blinks);
		}
		if (count($growth)>0){
			$tmp = createFileList($growth);
			if (count($tmp)>0) $files = array_merge($files, $tmp);
		}
		
		$lite_files = array();
		
		if (count($lite_blinks)>0){	
			$lite_files = createFileList($lite_blinks);
		}
		if (count($lite_growth)>0){
			$tmp = createFileList($lite_growth);
			if (count($tmp)>0) $lite_files = array_merge($lite_files, $tmp);
		}
	}else{
		return '{"success":false, "error":"No blinks '.$sql.'"}'; 
	}
	
	$courseJSON = json_encode($course);
	$blinksJSON = json_encode($blinks);
	$liteJSON = json_encode($lite_blinks);
	$growthJSON = json_encode($growth);
	$liteGrowthJSON = json_encode($lite_growth);
	$icon = $course['iconURL'];
	//$create_msg .= count($files).' files '.count($lite_files).' lite files ';
	
	if ($full){
		if (!create_zip($files, $courseJSON, $blinksJSON, $growthJSON, $courseId, $icon)) return '{"success":false, "error":"Problem creating course. '.$create_msg.'"}';
		$blinksJSON = json_encode($lite_blinks);
		if (!create_zip($lite_files, $courseJSON, $liteJSON, $liteGrowthJSON, $courseId, $icon, true)) return '{"success":false, "error":"Problem creating lite course"}';
		$sql = "UPDATE course SET buildAvailable=1 WHERE id=$courseId";
		$result = mysql_query($sql);
		if (!$result) return '{"success":false, "error":"Problem setting buildAvailable in database"}';
	}else{
		if (!create_zip($files, $courseJSON, $blinksJSON, $growthJSON, $courseId, $icon, false, $index)){
			return '{"success":false, "error":"Problem creating unit"}';
		}else{
			$filepath = "course$courseId_$index.zip";
			$sql = "SELECT * FROM units WHERE filepath='$filepath'";
			$result = mysql_query($sql);
			if (!$result) return '{"success":false, "error":"Problem checking existence of unit file"}';
			if (mysql_num_rows($result)==0){
				$title = $course['title'];
				$sql = "INSERT INTO units (title, filepath) VALUES ('$title', '$filepath')";
				$result = mysql_query($sql);
				if (!$result) return '{"success":false, "error":"problem inserting unit into database"}';
			}
		}
	}
	
	return '{"success":true, "msg":"'.$create_msg.'"}';
}

function createFileList($blinks){
	$files = array();
	
	foreach($blinks as $blink){
		$type = intval($blink['type']);
		$str = str_replace("\\", "", $blink['json']);
		$json = json_decode($str, true);
		switch($type){
			case 1:
			case 3:
			if ($json['intro']['image']!="") $files[] = '../courses/images/'.$json['intro']['image'];
			if ($type==3) if ($json['intro']['sound']!="") $files[] = '../courses/sounds/'.$json['intro']['sound'];
			break;
			case 5://ebook
			$files[] = '../courses/pdfs/'.$json['ebook'];
			break;
			case 6://presentation
			foreach($json['presentation'] as $presentation){
				//echo $presentation['sound'];
				if ($presentation['sound']!="") $files[] = '../courses/sounds/'.$presentation['sound'];
			}
			if (isset($json['image']) && $json['image'] != "") $files[] = '../courses/images/'.$json['image'];
			break;
			case 7://input
			$inputType = $json['input']['type'];
			if ($inputType==6 || $inputType==7){
				$useImages = $json['input']['useImages'];
				if ($useImages){
					//var_dump($json['input']);
					if (isset($json['input']['left']) && $json['input']['left'] != "") $files[] = '../courses/images/'.$json['input']['left'];
					if (isset($json['input']['right']) && $json['input']['right'] != "") $files[] = '../courses/images/'.$json['input']['right'];
				}
			}
			$files[] = '../courses/sounds/'.$json['input']['sound'];
			break;
			case 8://statement
			$files[] = '../courses/sounds/'.$json['statement']['sound'];
			break;
			case 9://wordfill
			$files[] = '../courses/sounds/'.$json['wordfill']['sound'];
			break;
			case 10://Drag drop
			$files[] = '../courses/sounds/'.$json['dragdrop']['sound'];
			foreach($json['dragdrop']['goals'] as $goal){
				if ($goal['isImage']){
					$files[] = '../courses/images/'.$goal['image'];
				}
			}
			foreach($json['dragdrop']['items'] as $item){
				if ($item['isImage']){
					$files[] = '../courses/images/'.$item['image'];
				}
			}
			break;
			case 11://Simon
			$files[] = '../courses/sounds/'.$json['simon']['sound'];
			foreach($json['simon']['sounds'] as $sound){
				if ($sound != "") $files[] = '../courses/sounds/'.$sound;
			}
			break;
			case 12://Order items
			$files[] = '../courses/sounds/'.$json['orderitems']['sound'];
			break;
			case 14://Question
			if (isset($json['image']) && $json['image'] != "") $files[] = '../courses/images/'.$json['image'];
			$files[] = '../courses/sounds/'.$json['question']['sound'];
			break;
			case 15://Rotator
			$files[] = '../courses/sounds/'.$json['rotator']['sound'];
			break;
			case 16://Wordheat
			$files[] = '../courses/sounds/'.$json['wordheat']['sound'];
			break;
			case 18://Catch
			$files[] = '../courses/sounds/'.$json['catch']['sound'];
			foreach($json['catch']['items'] as $item){
				if ($item['isImage'] && $item['image']!="") $files[] = '../courses/images/'.$item['image'];
			}
			break;
			case 19://Conversation
			foreach($json['conversation'] as $section){
				if ($section['sound']!="")$files[] = '../courses/sounds/'.$section['sound'];
				if ($section['image']!="") $files[] = '../courses/images/'.$section['image'];
			}
			break;
		}
	}
	
	return $files;
}

function create_zip($files = array(), $courseJSON, $blinksJSON, $growthJSON, $courseId, $icon, $lite, $index) {
	global $create_msg;
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files) && count($files)>0) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			$exists = false;
			if(file_exists($file)) {
				$exists = true;
				$valid_files[] = $file;
			}
			//echo "File: ".$file." $exists\n";
		}
	}
	//create the archive
	$zip = new ZipArchive();
	if (empty($index)){
		$destination = ($lite) ? "../courses/lite/course$courseId.zip" : "../courses/full/course$courseId.zip";
	}else{
		$destination = "../courses/unit/course$courseId_$index.zip";
	}
	//$create_msg .= $destination.' ';
	
	if (file_exists($destination)) unlink($destination);
	//echo "Destination:".$destination."\nOverwrite:".$overwrite."\n";
	if($zip->open($destination, ZIPARCHIVE::CREATE) !== true) {
		$create_msg .= "Problem creating zip file ";
		return false;
	}
	//$create_msg .= $growthJSON.' ';
	$zip->addFromString("course.json", $courseJSON);
	$zip->addFromString("blinks.json", $blinksJSON);
	$zip->addFromString("growth.json", $growthJSON);
	//add the files
	if (count($valid_files)>0){
		foreach($valid_files as $file){
			$pos = strrpos($file, "/");
			$localname = substr($file, $pos);
			if(file_exists($file)){
				$zip->addFile($file, $localname);
			}else{
				$create_msg .= $file.' missing. ';
			}
		}
	}
	$file = "../courses/images/".$icon;
	$zip->addFile($file, "icon.jpg");
	//debug
	if ($lite){
		$create_msg .= 'The lite course zip archive contains '.$zip->numFiles." files.";
	}else{
		$create_msg .= 'The full course zip archive contains '.$zip->numFiles." files.     ";
	}
	
	//close the zip -- done!
	$zip->close();
	
	//check to make sure the file exists
	return file_exists($destination);
}

function duplicateCourse(){
	global $msg;
	
	$course = json_decode($_REQUEST['value']);
	
	$title = mysql_real_escape_string($course->title);
	
	$pos = strpos($title, ' - copy');
	if ($pos!=false){
		$title = substr($title, 0, $pos);
		$pos = strrpos($title, " ");
		if ($pos!=false) $index = intval(substr($title, $pos));
	}
	
	$sql = "SELECT title FROM course WHERE title LIKE '$title%'";
	$result = mysql_query($sql);
	if (!$result){
		$msg = 'Problem counting current examples of this course '.$sql;
		return;
	}
	
	$count = 0;
	
	while($row = mysql_fetch_row($result)){
		$count++;
		$pos = strrpos($row[0], " ");
		if ($pos!=false){
			$idx = intval(substr($row[0], $pos));
			if ($idx>$index) $index = $idx;
		}
	}
	
	$index++;
	$title = "$title - copy $index";
	
	$summary = mysql_real_escape_string($course->summary);
	$description = mysql_real_escape_string($course->description);
	
	$sql = "INSERT INTO course (title, summary, description, iconURL, imageURL) VALUES ('".$title."', '".$summary."', '".$description."', '".$course->iconUrl."', '".$course->imageUrl."')";
	//$msg .= $sql.' ';
	$result = mysql_query($sql);
	if (!$result){
		var_dump($course);
		echo $_REQUEST['value'];
		$msg = "Problem adding course ".$sql;
		return;
	}
	
	$id = mysql_insert_id();
	$index = 0;
	foreach($course->blinks as $blink){
		$json = json_encode($blink->json);
		while(substr($json, 0, 1)!="{" && strlen($json)>0) $json = substr($json, 1);
		while(substr($json, strlen($json)-1, 1)!="}" && strlen($json)>0) $json = substr($json, 0, strlen($json)-1);
		$lite = (isset($blink->lite) && $blink->lite) ? 1 : 0;
		$summary = mysql_real_escape_string($blink->summary);
		$json = mysql_real_escape_string($json);
		$sql = "INSERT INTO blink (`index`, courseId, lite, type, summary, json, guid) VALUES ( $index, $id, $lite, $blink->type, '$summary', '$json', '$blink->guid')";
		$result = mysql_query($sql);
		if (!$result){
			$msg .= " $sql Problem inserting blink index=$index";
			return;
		}
		$index++;
	}
	
	$index = 0;
	foreach($course->growth as $blink){
		$json = json_encode($blink->json);
		while(substr($json, 0, 1)!="{" && strlen($json)>0) $json = substr($json, 1);
		while(substr($json, strlen($json)-1, 1)!="}" && strlen($json)>0) $json = substr($json, 0, strlen($json)-1);
		$lite = (isset($blink->lite) && $blink->lite) ? 1 : 0;
		$summary = mysql_real_escape_string($blink->summary);
		$json = mysql_real_escape_string($json);
		$sql = "INSERT INTO blink (`index`, courseId, lite, type, summary, json, guid, growth_activity) VALUES ( $index, $id, $lite, $blink->type, '$summary', '$json', '$blink->guid', 1)";
		$result = mysql_query($sql);
		if (!$result){
			$msg .= " $sql Problem inserting blink index=$index";
			return;
		}
		$index++;
	}
	
	$msg = "{ \"id\":$id, \"msg\":\"New course saved to database\", \"title\":\"$title\" }";
}

function updateCourse(){
	global $msg;
	
	$course = json_decode($_REQUEST['value']);
	
	$title = mysql_real_escape_string($course->title);
	$summary = mysql_real_escape_string($course->summary);
	$description = mysql_real_escape_string($course->description);
	$editDate = date('Y-m-d H:i:s',time());
	
	$sql = "UPDATE course SET title='".$title."', summary='".$summary."', description='".$description."', editDate='$editDate' WHERE id=".$course->id;
	//$msg .= $sql.' ';
	$result = mysql_query($sql);
	if (!$result){
		var_dump($course);
		echo $_REQUEST['value'];
		$msg = "Problem updating course details ".$sql;
		return;
	}
	
	//Save the current course in case anything went wrong
	$sql = "UPDATE blink SET courseId=2 WHERE courseId=$course->id";
	$result = mysql_query($sql);
	if (!$result){
		$msg = "Problem saving existing blinks ".$sql;
		return;
	}
	$index = 0;
	foreach($course->blinks as $blink){
		$json = json_encode($blink->json);
		while(substr($json, 0, 1)!="{" && strlen($json)>0) $json = substr($json, 1);
		while(substr($json, strlen($json)-1, 1)!="}" && strlen($json)>0) $json = substr($json, 0, strlen($json)-1);
		$lite = (isset($blink->lite) && $blink->lite) ? 1 : 0;
		$summary = mysql_real_escape_string($blink->summary);
		$sql = "INSERT INTO blink (`index`, courseId, lite, type, summary, json, guid) VALUES ( $index, $course->id, $lite, $blink->type, '$summary', '$json', '$blink->guid')";
		$result = mysql_query($sql);
		if (!$result){
			//Restore the old course
			$sql = "UPDATE blink SET courseId=$course->id WHERE courseId=2";
			$result = mysql_query($sql);
			if (!$result){
				$msg = "Problem restoring existing blinks ".$sql;
			}
			$msg .= " $sql Problem updating or inserting blink index=$index";
			return;
		}
		$index++;
	}
	
	$index = 0;
	
	foreach($course->growth as $blink){
		$json = json_encode($blink->json);
		while(substr($json, 0, 1)!="{" && strlen($json)>0) $json = substr($json, 1);
		while(substr($json, strlen($json)-1, 1)!="}" && strlen($json)>0) $json = substr($json, 0, strlen($json)-1);
		$lite = (isset($blink->lite) && $blink->lite) ? 1 : 0;
		$summary = mysql_real_escape_string($blink->summary);
		$sql = "INSERT INTO blink (`index`, courseId, lite, type, summary, json, guid, growth_activity) VALUES ( $index, $course->id, $lite, $blink->type, '$summary', '$json', '$blink->guid', 1)";
		$result = mysql_query($sql);
		if (!$result){
			//Restore the old course
			$sql = "UPDATE blink SET courseId=$course->id WHERE courseId=2";
			$result = mysql_query($sql);
			if (!$result){
				$msg = "Problem restoring existing blinks ".$sql;
			}
			$msg .= " $sql Problem updating or inserting blink index=$index";
			return;
		}
		$index++;
	}
	//All ok so delete the old course
	$sql = "DELETE FROM blink WHERE courseId=2";
	$result = mysql_query($sql);
	if (!$result){
		$msg = "Problem deleting existing blinks ".$sql;
		return;
	}
	$msg .= " course saved to database";
}

function json_validate($string) {
	if (is_string($string)) {
		@json_decode($string);
		return (json_last_error() === JSON_ERROR_NONE);
	}
	return false;
}
	
function showJSONError(){
	switch (json_last_error()) {
        case JSON_ERROR_NONE:
            echo ' - No errors';
        break;
        case JSON_ERROR_DEPTH:
            echo ' - Maximum stack depth exceeded';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Underflow or the modes mismatch';
        break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Unexpected control character found';
        break;
        case JSON_ERROR_SYNTAX:
            echo ' - Syntax error, malformed JSON';
        break;
        case JSON_ERROR_UTF8:
            echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
        break;
        default:
            echo ' - Unknown error';
        break;
    }
}

function saveImage($type, $data, $id){
	global $msg;
	
	if ($data['type'] != "image/jpeg"){
		$msg .= "$type is not a jpeg image<br/>"; 
		return "";
	}
	
	$name = "$type$id.jpg";
	$path = "../courses/images/$name";
	
	// VARS FOR FILE 
	$dwidth = ($type=='image') ? 480 : 127;
	$dheight = ($type=='image') ? 320 : 127;

	$img = imagecreatefromjpeg($data['tmp_name']);
	$swidth = imagesx( $img );
	$sheight = imagesy( $img );

	$tmpimg = imagecreatetruecolor( $dwidth, $dheight );
	imagecopyresampled( $tmpimg, $img, 0, 0, 0, 0, $dwidth, $dheight, $swidth, $sheight );

	if (file_exists($path)) unlink($path);
	
	// Save image into a file.
	if (imagejpeg( $tmpimg, $path, 80)){
		$msg .= " save ok";
	}else{
		$msg .= " problem with save.";
	}

	// release the memory
	imagedestroy($tmpimg);
	imagedestroy($img);

	return $name;
}
?>