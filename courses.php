<?
	require_once('connect.php');
	$msg = "";
	$courses = array();
	
	session_start();
	$loggedIn = $_SESSION['loggedIn'];
	$admin = $_SESSION['admin'];
	$name = $_SESSION['name'];
	$timeoffset = (isset($_SESSION['timeoffset'])) ? $_SESSION['timeoffset'] : 0;
	
	if (isset($loggedIn) && $loggedIn){
		if (isset($_REQUEST['method'])){
			$id = $_REQUEST['id'];
			switch($_REQUEST['method']){
				case 'delete':
				$sql = "DELETE FROM course WHERE id=$id";
				$result = mysql_query($sql);
				if (!$result){
					$msg = 'Problem deleting course';
				}else{
					$sql = "DELETE FROM blink WHERE courseId=$id";
					$result = mysql_query($sql);
					if (!$result){
						$msg = 'Problem deleting blinks for course';
					}else{
						$msg = 'Course deleted';
					}
				}
				break;
				case 'publish':
				$id = $_REQUEST['id'];
				$sql = "UPDATE course SET published=1 WHERE id=$id";
				$result = mysql_query($sql);
				if (!$result){
					$msg = 'Problem publishing course';
				}else{
					$msg = 'Course published';
				}
				break;
			}
		}
		$sql = "SELECT * FROM course";
		$result = mysql_query($sql);
		if ($result){
			//$msg = 'Course total is '.mysql_num_rows($result).'<br/>';
			while($row=mysql_fetch_assoc($result)){
				$time = $timestamp = strtotime($row['editDate']) - $timeoffset*60;
				$row['modifiedDate'] = date('h:ia M d - Y', $time);
				$time = $timestamp = strtotime($row['date']) - $timeoffset*60;
				$row['createDate'] = date('h:ia M d - Y', $time);
				$courses[] = $row;
			}
		}
	}
	
	mysql_close($conn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/blink_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- Preview at https://64.34.168.15:8443/sitepreview/http/blinktrainingsystem.com -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Blink Admin - Courses</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link href="main.css" rel="stylesheet" type="text/css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
  $(function() {
    $( document ).tooltip();
  });
  </script>
<!-- InstanceBeginEditable name="head" -->
<script src="js/course.js"></script>
<script type="text/javascript">
var courseIdx = -1;
var timeofset;

console.log("<? echo $msg; ?>");

$(document).ready(function() {	
	$( "#delete-course-confirm" ).dialog({
		  resizable: false,
		  height:240,
		  modal: true,
		  autoOpen: false,
		  buttons: {
			"Delete course": function() {
				deleteCourse(courseIdx, false);
				courseIdx = -1;
				$( this ).dialog( "close" );
			},
			Cancel: function() {
				courseIdx = -1;
				$( this ).dialog( "close" );
			}
		  }
		});
	
	$( "#courses-feedback" ).dialog({
		  height: 240,
		  resizable: false,
		  autoOpen: false,
		  modal: true
		});
		
	$(window).resize(function() {
		windowResized();
    });
	
	$(window).trigger('resize');
});

function windowResized(){
	var height = $(window).height() - $('.header').height() - $('.footer').height();
	var content_height = $('#content-inset').height();
	$('.content').height(height);
	$('.sidebar1').height(height);
	if (content_height>height) $('#content-inset').height(height-20);
}
		
function newCoursePressed(){
	$('#content-inset').height('auto');
	var btn = $('#new_course');
	if (btn.text()=="New course"){
		btn.html('<a href="#" onclick="newCoursePressed()">Close</a>');
		$('#new_course_form').show("slow", function(){
			windowResized();
		});
	}else{
		btn.html('<a href="#" onclick="newCoursePressed()">New course</a>');
		$('#new_course_form').hide("slow", function(){
			windowResized();
		});
	}
}
function validateForm(frm){
	if (frm.title.value==""){
		showError("Please enter a title for the course");
	}else if (frm.summary.value==""){
		showError("Please enter a summary for the course");
	}else if (frm.description.value==""){
		showError("Please enter a description for the course");
	}else if (frm.icon.value==""){
		showError("Please enter choose an icon for the course");
	}else if (frm.image.value==""){
		showError("Please enter choose an image for the course");
	}else{
		return true;
	}
	return false;
}
function showError(str){
	var msg = $('#msg');
	msg.text(str);
	msg.css('display', 'block');
}
function deleteCourse(idx, showConfirm){
	if (showConfirm){
		courseIdx = idx;
		$('#delete-course-confirm').dialog('open');
	}else{
		window.location = "courses.php?method=delete&id=" + idx;
	}
}

function showFeedback(msg){
	console.log("showFeedback:" + msg);
	var elm = $("#courses-feedback-msg");
	elm.text(msg);
	$( "#courses-feedback" ).dialog('open');
}
</script>
<!-- InstanceEndEditable -->
</head>

<body>
<div class="container"> 
  <div class="header"><span class="heading">Blink Training System - CMS <!-- InstanceBeginEditable name="PageName" -->- Courses<!-- InstanceEndEditable --></span>
    <!-- end .header --></div>
  <div class="sidebar1">
    <ul class="nav">
      <!-- InstanceBeginEditable name="sidebar" -->
      <?
	  	if ($loggedIn){
			echo '<li><a href="logout.php">Logout ('.$name.')</a></li>';
      		echo '<li><a href="courses.php">Courses</a></li>';
			echo '<li><a href="usercourses.php">User Courses</a></li>';
		}else{
      		echo '<li><a href="login.php">Login</a></li>';
      		echo '<li><a href="register.php">Register</a></li>';
		}
	  ?>
      <!-- InstanceEndEditable -->
    </ul>
    <!-- end .sidebar1 --></div>
  <div class="content">
    <!-- InstanceBeginEditable name="content" -->
    <div id="content-inset">
    <?
    	echo '<h1>Courses</h1>';
		if (!$loggedIn){
			//Logged in
			echo '<div id="msg" class="error">You need to be logged in to view this page.</div>';
		}else{
			if ($msg!=""){
				echo '<div id="msg" class="error" style="display:block;">'.$msg.'</div>';
			}else{
				echo '<div id="msg" class="error" style="display:none;"></div>';
			}
			echo '<div id="new_course"><a href="#" onclick="newCoursePressed()">New course</a></div>';
    		echo '<div id="new_course_form" style="display:none;">';
    		echo '<form action="course.php" method="post"  onsubmit="return validateForm(this)" enctype="multipart/form-data">';
        	echo '<input type="hidden" name="method" value="new" />';
        	echo '<table width="100%">';
        	echo '<tr><td class="left" width="20%">Title</td><td><input type="text" id="title" name="title" placeholder="Enter the course title" class="text_field"/></td></tr>';
            echo '<tr><td class="left">Summary</td><td><textarea placeholder="Enter the course summary" id="summary" name="summary"></textarea></td></tr>';
            echo '<tr><td class="left">Description</td><td><textarea placeholder="Enter the course description" id="description" name="description"></textarea></td></tr>';
            echo '<tr><td class="left">Icon</td><td><input type="file" name="icon" accept="image/jpeg" value="Choose icon (512x512) file..." class="text_field" ></tr>';
            echo '<tr><td class="left">Image</td><td><input type="file" name="image" accept="image/jpeg" value="Choose image (1136 x 640) file..." class="text_field" ></tr>';
            echo '<tr><td>&nbsp;</td><td class="left"><input type="submit" /></td></tr>';
        	echo '</table></form></div><br />';
			
    		echo '<table width="100%">';
			$index = 1;
			foreach($courses as $course){
				echo '<tr>';
				echo '<td width="50px"><img src="'.$imagePath.$course['iconURL'].'" width="40px" height="40px"/></td>';
				echo '<td class="left">'.$course['title'].'</td><td width="45%" onclick="window.document.location=\'course.php?id='.$course['id'].'\';"><span class="small">'.$course['summary'].'<br>(Created:'.$course['createDate'].' Modified:'.$course['modifiedDate'].')</span></td>';
				echo '<td class="buttons" width="10%">';
				if ($course['published']){
					echo '<img src="images/btn_unpublish.png" id="publish'.$index.'" class="button" title="(Un)Publish course" onclick="publishCourse('.$course['id'].', false, '.$index.')" />';
				}else{
					echo '<img src="images/btn_publish.png" id="publish'.$index.'" class="button" title="(Un)Publish course" onclick="publishCourse('.$course['id'].', true, '.$index.')" />';
				}
				echo '<img src="images/btn_delete.png" class="button" title="Delete course" onclick="deleteCourse('.$course['id'].', true)" />';
				echo '</td></tr>';
				$index++;
			}
			echo '</table>';
    		
			echo '<div id="delete-course-confirm" title="Delete course?">';
			echo '  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This course will be permanently deleted and cannot be recovered. Are you sure?</p>';
			echo '</div>';
			echo '<div id="courses-feedback" title="Course published">';
			echo '	<div id="courses-feedback-msg">msg</div>';
			echo '</div>';
		}
	?>
    </div>
    <!-- InstanceEndEditable -->
    <!-- end .content --></div>
  <div class="footer">
    Copyright © 2014 by Blink Training Systems, LLC
    <!-- end .footer --></div> 
  <!-- end .container --></div> 
</body>
<!-- InstanceEnd --></html>
