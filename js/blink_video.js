// JavaScript Document
function showVideoBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getVideoBlink());
	setTimeout(setScrollHeight, 10);
}

function addVideoEvents(blink){
	$("#video-prompt").keyup(function(){
		$("#video-prompt").blur();
		$("#video-prompt").focus();
	});

	$('#video-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.video.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	$("#video-vimeo").keyup(function(){
		$("#video-vimeo").blur();
		$("#video-vimeo").focus();
	});

	$('#video-vimeo').change( function(){
		console.log("blink vimeo changed " + this.value);
		blink.video.vimeo = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
}

function getVideoBlink(){
	var blink = blinkData;
	if (blink.video==null){
		blink.video = new Object();
		blink.video.prompt = "";
		blink.video.vimeo = "";
	}
	
	var str = '<table width="100%"><tr><td class="left" width="20%">Prompt</td>';
    str += '<td>';
    str += '<textarea id="video-prompt" placeholder="Enter text prompt." style="width:100%; height:60px;">' + blink.video.prompt + '</textarea>';
    str += '<td class="buttons"></td>';
	str += '<tr><td class="left" width="20%">Vimeo</td><td><input id="video-vimeo" value="' + blink.video.vimeo + '" style="width:200px"></td><td class="buttons"></td></tr>';
	str += '</table>';
	return str;
}