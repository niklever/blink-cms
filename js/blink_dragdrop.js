// JavaScript Document
function showDragdropBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getDragdropBlink());
	setTimeout(setScrollHeight, 10);
}

function addDragdropEvents(blink){
	var elm = $('#dragdrop-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.dragdrop.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.dragdrop.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.dragdrop.timer);
	elm.width(60);
	
	elm = $('#dragdrop-prompt');
	elm.keyup(function(){
		var elm = $('#' + event.target.id);
		elm.blur();
		elm.focus();
	});
	
	elm.change(function(){
		blink.dragdrop.prompt = this.value;
		console.log("Prompt changed " + this.value);
		blinkModified = true;
		updateBlinkPanelButtons();
	});
		
	for(var i=0; i<blink.dragdrop.goals.length; i++){		
		elm = $('#dragdrop-goal-form' + (i+1));
		elm.ajaxForm(function(data) {
			console.log("goal form " + data); 
			var json = JSON.parse(data);
			if (json.success){
				var src = "../courses/images/" + json.path;
				blinkData.dragdrop.goals[json.index].image = json.path;
				$('#dragdrop-goal-image' + (json.index+1)).attr("src", src+"?timestamp=" + new Date().getTime());
			}
		});
		
		elm = $('#dragdrop-goal-text' + (i+1));
		elm.keyup(function(){
			var elm = $('#' + event.target.id);
			elm.blur();
			elm.focus();
		});
		
		elm.change(function(){
			var idx = parseInt(this.id.substr(18));
			blink.dragdrop.goals[idx-1].text = this.value;
			console.log("Item text changed " + this.value);
			blinkModified = true;
			updateBlinkPanelButtons();
		});
	}
	
	for(var i=0; i<blink.dragdrop.items.length; i++){
		elm = $('#dragdrop-goal-select' + (i+1));
		elm.change(function(){
			var idx = parseInt(this.id.substr(20));
			var index = $('#' + this.id + ' option:selected').index();
			blink.dragdrop.items[idx-1].goal = index;
			console.log("Item goal changed " + index);
			blinkModified = true;
			updateBlinkPanelButtons();
		});
		
		elm = $('#dragdrop-item-form' + (i+1));
		elm.ajaxForm(function(data) {
			console.log("goal form " + data); 
			var json = JSON.parse(data);
			if (json.success){
				var src = "../courses/images/" + json.path;
				blinkData.dragdrop.items[json.index].image = json.path;
				$('#dragdrop-item-image' + (json.index+1)).attr("src", src+"?timestamp=" + new Date().getTime());
			}
		});
		
		elm = $('#dragdrop-item-text' + (i+1));
		elm.keyup(function(){
			var elm = $('#' + event.target.id);
			elm.blur();
			elm.focus();
		});
		
		elm.change(function(){
			var idx = parseInt(this.id.substr(18));
			blink.dragdrop.items[idx-1].text = this.value;
			console.log("Goal text changed " + this.value);
			blinkModified = true;
			updateBlinkPanelButtons();
		});
		
		addFeedbackEventsForItem(blink.dragdrop.items[i], i, true);
	}
	
	addFeedbackEvents();
	addPromptSoundEvents(blink.dragdrop);
}

function getDragdropBlink(){
	var blink = blinkData;
	if (blink.dragdrop==null){
		blink.dragdrop = new Object();
		blink.dragdrop.timer = 60;
		blink.dragdrop.layout = 0;
		blink.dragdrop.goals = new Array();
		blink.dragdrop.items = new Array();
		blink.dragdrop.prompt = "Drag each item to the correct learning goal";
		blink.dragdrop.sound = "";
	}else if (blink.dragdrop.prompt == null){
		blink.dragdrop.prompt = "Drag each item to the correct learning goal";
	}
	
	str = '<table width="100%" style="background-color:#FFF;">';
	str += '<tr>';
	str += '<tr><td class="left" width="20%">Prompt</td><td><textarea id="dragdrop-prompt" placeholder="Enter your text here" style="width:100%; height:60px;">' + blink.dragdrop.prompt + '</textarea></td></tr>';
	str += getPromptSoundHTML(blink.dragdrop);
	str += '<td class="left" width="20%">Drag and Drop</td>';
    str += '<td>Time (in secs, 0 for not timed)&nbsp;<input id="dragdrop-timer"></td>';
    str += '<td class="buttons" width="20%">'
    str += '	Goal<img src="images/btn_plus.png" title="Add Goal" onclick="dragdropAddGoal()" class="button"/>';
    str += '	Item<img src="images/btn_plus.png" title="Add Item" onclick="dragdropAddItem()" class="button"/>';
    str += '</td></tr>';
	
	for (var i=0; i<blink.dragdrop.goals.length; i++){
		str += '<tr><td class="left">Goal ' + (i+1) + '</td>';
    	//str += '<td>Left<input id="dragdrop-goal-left' + (i+1) + '">&nbsp;Top<input id="dragdrop-goal-top' + (i+1) + '">&nbsp;';
		var checked = (blink.dragdrop.goals[i].isImage) ? "checked" : "";
    	str += '<td>Image<input type="checkbox" id="dragdrop-goal-imagechk' + (i+1) + '"' + checked + ' onclick="dragdropImageChk(this, true,' + (i+1) + ')"/>&nbsp;';
		str += 'Text <input type="text" id="dragdrop-goal-text' + (i+1) + '" value="' + blink.dragdrop.goals[i].text + '" style="width:300px;"/>&nbsp;';
		str += '<div id="dragdrop-goal-data' + (i+1) + '">';
		if (blink.dragdrop.goals[i].isImage){
			str += '<form id="dragdrop-goal-form' + (i+1) + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
			str += '<input type="hidden" name="method" value="drag_image">';
			str += '<input type="hidden" name="goal" value="true">';
			str += '<input type="hidden" name="array" value="' + i + '">';
			str += '<br/>Choose Image<input type="file" id="image"  />';
			str += '<input type="submit" value="Upload" />';
			str += '</form>';
    		str += '<img id="dragdrop-goal-image' + (i+1) + '" src="../courses/images/' + blink.dragdrop.goals[i].image + '"  />';
		}
    	str += '</div></td><td class="buttons"><img src="images/btn_delete.png" title="Delete Goal" onclick="dragdropDeleteGoal(' + i + ')" class="button"/></td></tr>';
	}
    
	for (var i=0; i<blink.dragdrop.items.length; i++){
		str += '<tr><td class="left">Item ' + (i+1) + '</td>';
    	//str += '<td>Left<input id="dragdrop-item-left' + (i+1) + '">&nbsp;Top<input id="dragdrop-item-top' + (i+1) + '">&nbsp;';
		str += '<td>Goal<select id="dragdrop-goal-select' + (i+1) + '">';
		for(var j=0; j<=blink.dragdrop.goals.length; j++){
			var selected = (blink.dragdrop.items[i].goal==j) ? " selected" : "";
			if (j==0){
	    		str += '<option' + selected + '>None</option>';
			}else{
    			str += '<option' + selected + '>Goal ' + j + '</option>';
			}
		}
    	str += '</select>';
		var checked = (blink.dragdrop.items[i].isImage) ? "checked" : "";
    	str += 'Image<input type="checkbox" id="dragdrop-item-imagechk' + (i+1) + '"' + checked + ' onclick="dragdropImageChk(this, false,' + (i+1) + ')"/>&nbsp;';
		str += 'Text <input type="text" id="dragdrop-item-text' + (i+1) + '" value="' + blink.dragdrop.items[i].text + '" style="width:300px;"/>&nbsp;';
		str += '<div id="dragdrop-item-data' + (i+1) + '">';
		if (blink.dragdrop.items[i].isImage){
			str += '<form id="dragdrop-item-form' + (i+1) + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
			str += '<input type="hidden" name="method" value="drag_image">';
			str += '<input type="hidden" name="goal" value="false">';
			str += '<input type="hidden" name="index" value="' + i + '">';
			str += '</br>Choose Image<input type="file" id="dragdrop-item-file' + (i+1) + '"  />';
			str += '<input type="submit" value="Upload" />';
			str += '</form>';
    		str += '<img id="dragdrop-item-image' + (i+1) + '" src="../courses/images/' + blink.dragdrop.items[i].image + '"  />';
		}
    	str += '</div></td><td class="buttons"><img src="images/btn_delete.png" title="Delete Item" onclick="dragdropDeleteItem(' + i + ')" class="button"/></td></tr>';
		str += getFeedbackRowsForItem(blink.dragdrop.items[i], i, "Drag", true);
	}
	
	str += getFeedbackRows();
	str += '</table>';

	return str;
}

function dragdropImageChk(chk, goal, idx){
	blinkModified = true;
	var blink = blinkData;
	var elm = (goal) ? $('#dragdrop-goal-data' + idx) : $('#dragdrop-item-data' + idx);
	var str = "";
	if (goal){
		blink.dragdrop.goals[idx-1].isImage = chk.checked;
		//str += 'Text <input type="text" id="dragdrop-goal-text' + idx + '" value="' + blink.dragdrop.goals[idx-1].text + '" style="width:300px;"/>&nbsp;';
		if (blink.dragdrop.goals[idx-1].isImage){
			str += '<form id="dragdrop-goal-form' + idx + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
			str += '<input type="hidden" name="method" value="drag_image">';
			str += '<input type="hidden" name="goal" value="true">';
			str += '<input type="hidden" name="index" value="' + (idx-1) + '">';
			str += '<br/>Choose Image<input type="file" name="image"/>';
			str += '<input type="submit" value="Upload" />';
			str += '</form>';
    		str += '<img id="dragdrop-goal-image' + idx + '" src="../courses/images/' + blink.dragdrop.goals[idx-1].image + '"  />';
		}
	}else{
		blink.dragdrop.items[idx-1].isImage = chk.checked;
		//str += 'Text <input type="text" id="dragdrop-item-text' + idx + '" value="' + blink.dragdrop.items[idx-1].text + '" style="width:300px;"/>&nbsp;';
		if (blink.dragdrop.items[idx-1].isImage){
			str += '<form id="dragdrop-item-form' + idx + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
			str += '<input type="hidden" name="method" value="drag_image">';
			str += '<input type="hidden" name="goal" value="false">';
			str += '<input type="hidden" name="index" value="' + (idx-1) + '">';
			str += '</br>Choose Image<input type="file" name="image"/>';
			str += '<input type="submit" value="Upload"/>';
			str += '</form>';
    		str += '<img id="dragdrop-item-image' + idx + '" src="../courses/images/' + blink.dragdrop.items[idx-1].image + '"  />';
		}	
	}
	elm.html(str);
	updateBlinkButtons();
	addBlinkPanelEvents();
}

function dragdropAddGoal(){
	var blink = blinkData;
	blink.dragdrop.goals.push( { isImage:false, text:"", image:"" });
	showDragdropBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function dragdropDeleteGoal(idx){
	var blink = blinkData;
	blink.dragdrop.goals.splice(idx, 1);
	showDragdropBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function dragdropAddItem(){
	var blink = blinkData;
	blink.dragdrop.items.push( { goal:0, isImage:false, text:"", image:"" });
	showDragdropBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function dragdropDeleteItem(idx){
	var blink = blinkData;
	blink.dragdrop.items.splice(idx, 1);
	showDragdropBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}