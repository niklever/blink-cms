// JavaScript Document
function showWordHeatBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getWordHeatBlink());
	setTimeout(setScrollHeight, 10);
}

function addWordHeatEvents(blink){
	var elm = $('#wordheat-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.wordheat.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.wordheat.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.wordheat.timer);
	elm.width(60);
	
	$("#wordheat-prompt").keyup(function(){
		$("#wordheat-prompt").blur();
		$("#wordheat-prompt").focus();
	});

	$('#wordheat-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.wordheat.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	for(var i=0; i<blink.wordheat.words.length; i++){
		elm = $('#wordheat-word' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink word changed " + this.value);
			var idx = parseInt(this.id.substr(13));
			blink.wordheat.words[idx-1].word = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		elm = $('#wordheat-layer' + (i+1));
		elm.spinner({min:0, max:5, stop: function( event, ui ) {
			var idx = parseInt(this.id.substr(14));
			blink.wordheat.words[idx].layer = $(this).spinner("value");
			console.log("Spinner change to " + blink.wordheat.words[idx].layer);
			blinkModified=true;
			updateBlinkPanelButtons(); 
		}});
		elm.spinner("value", blink.wordheat.words[i].layer);
		elm.width(60);
	}
	
	addFeedbackEvents();
	addPromptSoundEvents(blink.wordheat);
}

function getWordHeatBlink(){
	var blink = blinkData;
	if (blink.wordheat==null){
		blink.wordheat = new Object();
		blink.wordheat.prompt = "";
		blink.wordheat.words = new Array();
		blink.wordheat.timer = 0;
		blink.wordheat.sound = "";
	}
	
	var str = '<table width="100%"><tr><td class="left" width="20%">Prompt</td>';
    str += '<td>';
    str += '<textarea id="wordheat-prompt" placeholder="Enter text prompt." style="width:100%; height:60px;">' + blink.wordheat.prompt + '</textarea>';
    str += '<td class="buttons">';
	str += '<a href="#"><img class="button" src="images/btn_plus.png" title="Add Word" onclick="wordheatAddWord()" /></a></td></tr>';
	str += getPromptSoundHTML(blink.wordheat);
	str += '<tr><td class="left" width="20%">WordHeat</td><td>Time (in secs, 0 for not timed)&nbsp;<input id="wordheat-timer"></td><td class="buttons"></td></tr>';
	if (blink.wordheat!=null && blink.wordheat.words!=null){
		for(var i=0; i<blink.wordheat.words.length; i++){
			str += '<tr><td class="left">Word ' + (i+1) + '</td>';
			str += '<td><input id="wordheat-word' + (i+1) + '" value="' + blink.wordheat.words[i].word + '" style="width:200px">&nbsp;Recommended layer (0 if unconcerned)&nbsp;<input id="wordheat-layer' + (i+1) + '"></td>';
			str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this word" onclick="wordheatDeleteWord(' + i + ')" ></td></tr>';
		}
	}
	
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function wordheatAddWord(){
	var blink = blinkData;
	blink.wordheat.words.push( { word:"", layer:5 } );
	showWordHeatBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function wordheatDeleteWord(idx){
	var blink = blinkData;
	blink.wordheat.words.splice(idx, 1);
	showWordHeatBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}