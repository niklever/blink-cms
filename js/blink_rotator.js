// JavaScript Document
function showRotatorBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getRotatorBlink());
	setTimeout(setScrollHeight, 10);
}

function addRotatorEvents(blink){
	var elm = $('#rotator-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.rotator.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.rotator.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.rotator.timer);
	elm.width(60);
	
	$("#rotator-prompt").keyup(function(){
		$("#rotator-prompt").blur();
		$("#rotator-prompt").focus();
	});

	$('#rotator-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.rotator.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	for(var i=0; i<blink.rotator.statements.length; i++){
		elm = $('#rotator' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink statement changed " + this.value);
			var idx = parseInt(this.id.substr(7));
			blink.rotator.statements[idx-1].text = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		$('#rotator-correct' + (i+1)).on("click", function() {
			var idx = parseInt(this.id.substr(15));
			console.log("blink correct changed " + idx);
			blink.rotator.statements[idx-1].correct = $(this).is(':checked');
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}
	
	addFeedbackEvents();
	addPromptSoundEvents(blink.rotator);
}

function getRotatorBlink(){
	var blink = blinkData;
	if (blink.rotator==null){
		blink.rotator = new Object();
		blink.rotator.prompt = "";
		blink.rotator.statements = new Array();
		blink.rotator.timer = 0;
		blink.rotator.sound = "";
	}
	var str = '<table width="100%"><tr><td class="left" width="20%">Prompt</td>';
    str += '<td>';
    str += '<textarea id="rotator-prompt" placeholder="Enter text prompt." style="width:100%; height:60px;">' + blink.rotator.prompt + '</textarea>';
    str += '<td class="buttons">';
	str += '<a href="#"><img class="button" src="images/btn_plus.png" title="Add Rotator" onclick="rotatorAddStatement()" /></a></td></tr>';
	str += getPromptSoundHTML(blink.rotator);
	str += '<tr><td class="left" width="20%">Statement</td><td>Time (in secs, 0 for not timed)&nbsp;<input id="rotator-timer"></td><td class="buttons"></td></tr>';
	if (blink.rotator!=null && blink.rotator.statements!=null){
		for(var i=0; i<blink.rotator.statements.length; i++){
			if (typeof blink.rotator.statements[i] == 'string'){
				blink.rotator.statements[i] = { text:blink.rotator.statements[i], correct:true };
			}
			str += '<tr><td class="left">Statement ' + (i+1) + '</td>';
			str += '<td><input id="rotator' + (i+1) + '" value="' + blink.rotator.statements[i].text + '" style="width:80%">';
			var checked = (blink.rotator.statements[i].correct) ? "checked" : "";
			str += '&nbsp;Correct? <input id="rotator-correct' + (i+1) + '" type="checkbox" ' + checked + '></td>';
			str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this statement" onclick="rotatorDeleteStatement(' + i + ')" ></td></tr>';
		}
	}
	
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function rotatorAddStatement(){
	var blink = blinkData;
	blink.rotator.statements.push( { text:"", correct:true } );
	showRotatorBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function rotatorDeleteStatement(idx){
	var blink = blinkData;
	blink.rotator.statements.splice(idx, 1);
	showRotatorBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}