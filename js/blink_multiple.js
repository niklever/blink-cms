// JavaScript Document
function showMultipleBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getMultipleBlink());
	setTimeout(setScrollHeight, 10);
}

function questionUpdateTimer(){
	var elm = $('#multiple-timer');
	blink.multiple.timer = elm.spinner("value");
}

function addMultipleEvents(blink){
	var elm = $('#multiple-text');
	elm.keyup(function(){
		var elm = $('#' + event.target.id);
		elm.blur();
		elm.focus();
	});
	
	elm.change(function(){
		blink.multiple.text = this.value;
		console.log("Multiple text changed " + this.value);
		blinkModified = true;
		updateBlinkPanelButtons();
	});
	
	var elm = $('#multiple-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.multiple.timer = $(this).spinner("value");
		blinkModified=true;
		updateBlinkPanelButtons(); 
		console.log("Spinner change to " + blink.multiple.timer);
    }});
	elm.spinner("value", blink.multiple.timer);
	elm.width(60);
		
	for(var i=0; i<blink.multiple.answers.length; i++){
		var elm = $('#answer-text' + (i+1));
		elm.keyup(function(){
			var elm = $('#' + event.target.id);
			elm.blur();
			elm.focus();
		});
		
		elm.change(function(){
			var idx = parseInt(this.id.substr(11));
			blink.multiple.answers[idx-1].text = this.value;
			console.log("Answer text changed " + this.value);
			blinkModified = true;
			updateBlinkPanelButtons();
		});
		
		addFeedbackEventsForItem(blink.multiple.answers[i], i, false);
	}
	
	addFeedbackEvents();
	addPromptSoundEvents(blink.question);
}

function getMultipleBlink(){
	var blink = blinkData;
	if (blink.multiple==null){
		blink.multiple = new Object();
		blink.multiple.timer = 60;
		blink.multiple.answers = new Array();
		blink.multiple.sound = "";
	}
	
	str = '<table width="100%" style="background-color:#FFF;">';
	str += '<tr>';
	str += '<td class="left" width="20%">Question</td>';
    str += '<td><textarea id="multiple-text" placeholder="Enter your text here" style="width:100%; height:60px;">' + blink.multiple.text + '</textarea><br/>';
	str += 'Time (in secs, 0 for not timed)&nbsp;<input id="multiple-timer">';
	str += '<td class="buttons" width="20%">'
    str += '	<img src="images/btn_plus.png" title="Add Answer" onclick="multipleAddAnswer()" class="button"/>';
    str += '</td></tr>';
	str += getPromptSoundHTML(blink.multiple);
	
	for (var i=0; i<blink.multiple.answers.length; i++){
		str += '<tr><td class="left">Answer ' + (i+1) + '</td>';
    	//str += '<td>Left<input id="dragdrop-goal-left' + (i+1) + '">&nbsp;Top<input id="dragdrop-goal-top' + (i+1) + '">&nbsp;';
		var checked = (blink.multiple.answers[i].correct) ? "checked" : "";
		str += '<td>Text <input type="text" id="answer-text' + (i+1) + '" value="' + blink.multiple.answers[i].text + '" placeholder="Enter answer" style="width:300px;"/>&nbsp;';
    	str += 'Correct<input type="checkbox" id="answer-correctchk' + (i+1) + '"' + checked + ' onclick="multipleAnswerChk(this, ' + i + ')"/></td>';
    	str += '</td><td class="buttons"><img src="images/btn_delete.png" title="Delete Answer" onclick="multipleDeleteAnswer(' + i + ')" class="button"/></td></tr>';
		str += getFeedbackRowsForItem(blink.multiple.answers[i], i, "Answer", false);
	}
	
	str += getFeedbackRows(true);
	str += '</table>';

	return str;
}

function multipleAnswerChk(chk, idx){
	blinkModified = true;
	var blink = blinkData;
	blink.multiple.answers[idx].correct = chk.checked;
	updateBlinkButtons();
	addBlinkPanelEvents();
}

function multipleAddAnswer(){
	var blink = blinkData;
	blink.multiple.answers.push( { correct:false, text:"" });
	showMultipleBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function multipleDeleteAnswer(idx){
	var blink = blinkData;
	blink.multiple.answers.splice(idx, 1);
	showMultipleBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}