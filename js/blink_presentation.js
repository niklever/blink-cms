// JavaScript Document
function getPresentationBlink(){
	if (blinkData.image==null) blinkData.image="";
	var str = '<table width="90%"><tr><td class="left" width="20%">Add Text</td>';
    str += '<td>';
    str += '<textarea id="presentation-text" placeholder="Enter your text here" style="width:100%; height:60px;"></textarea>';
    str += '<td width="25%" class="buttons"><a href="#"><img src="images/btn_plus.png" title="Create" onclick="addPresentationText()" /></a></td></tr>';
	str += '<form id="presentation-image-form" method="post" action="blink_update.php" enctype="multipart/form-data">';
	str += '<tr><td class="left" width="20%">Image</td><td><input type="file" name="image"/>';
	str += '<input type="hidden" name="method" value="intro_image" />';
	str += '<input type="submit" value="Upload" />';
	str += '</form>';
	str += '<br/><img id="presentation-image" src="../courses/images/' + blinkData.image + '" /></td><td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete image" onclick="presentationDeleteImage()" ></td></tr>';
    str += getPresentationHTML();
	str += '</table>';
	return str;
}

function presentationDeleteImage(){
	blinkData.image = "";
	showPresentationBlink();
	addPresentationEvents(blinkData);
}

function showPresentationBlink(){
	$('#blink-content').html(getPresentationBlink());
	$("blinks").attr("overflow", "hidden");
	setTimeout(setScrollHeight, 10);
}

function getPresentationTextFormatting(idx){
	var str = "<br>";
	str += 'Font <select id="presentation-font' + idx + '">';
	str += '<option>Regular</option>';
	str += '<option>Bold</option>';
	str += '<option>Italic</option></select>';
	str += 'Alignment <select id="presentation-alignment' + idx + '">';
	str += '<option>Left</option>';
	str += '<option>Center</option>';
	str += '<option>Justified</option></select>';
	str += 'Size <select id="presentation-size' + idx + '">';
	str += '<option>Regular</option>';
	str += '<option>Small</option>';	
	str += '<option>Large</option></select>';
	str += 'Color <select id="presentation-color' + idx + '">';
	str += '<option value="#000000">Black</option>';
	str += '<option value="#256ED7">Blue</option>';//Was 6D6DCC
	str += '<option value="#FF9500">Orange</option></select>';//was FF9e02
	return str;
}

function getPresentationHTML(){
	var str = "";
	var blink = blinkData;
	if (blink.presentation!=null){
		for(var i=0; i<blink.presentation.length; i++){
			if (blink.presentation[i].in == null) blink.presentation[i].in = 0;
			if (blink.presentation[i].time == null) blink.presentation[i].time = 0;
			if (blink.presentation[i].sound == null) blink.presentation[i].sound = "";
			str += '<tr><td class="left">Text ' + (i+1) + '</td><td><textarea id="presentation-text' + (i+1) + '" onkeyup="presentationTextKeyup(this)" onchange="presentationTextChange(this, ' + i + ')" style="width:100%; height:40px;">' + blink.presentation[i].text + '</textarea>';
			str += '<form id="presentation-sound-form' + i + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
			str += '<input type="file" name="sound"/>';
			str += '<input type="hidden" name="method" value="sound" />';
			str += '<input type="hidden" name="id" value="' + i + '" />';
			str += '<input type="submit" value="Upload" />';
			str += '</form>';
			str += getPresentationTextFormatting(i);
			str += '</td><td class="buttons">';
			str += '<img class="button" id="presentation_time' + i + '" src="images/btn_time' + blink.presentation[i].time + '.png" >';
			str += '<img class="button" src="images/btn_timer.png" title="Set timer" onclick="presentationTimer(' + i + ')" >';
			str += '<img id="presentation_anim_in' + i + '" class="button" src="images/btn_anim' + blink.presentation[i].in + '.png" title="Set animation in" onclick="presentationAnimation(' + i + ',false)" >';
			str += '<img class="button" src="images/btn_delete.png" title="Delete text" onclick="presentationDelete(' + i + ')" >';
			str += '<img class="button" src="images/btn_delete_sound.png" id="presentation-sound-delete' + i + '" title="Delete this sound">';
			str += '<audio id="presentation-sound' + i + '" controls><source src="../courses/sounds/' + blink.presentation[i].sound + '" type="audio/mpeg"></audio></td></tr>';
			str += '</td></tr>';
		}
	}
	return str;
}

function addPresentationEvents(blink){
	var elm = $('#presentation-image-form');
	elm.ajaxForm(function(data) {
		console.log("Presentation image form " + data); 
		var json = JSON.parse(data);
		if (json.success){
			var src = "../courses/images/" + json.path;
			blinkData.image = json.path;
			var elm = $('#presentation-image');
			if (elm!=null){
				elm.attr("src", src+"?timestamp=" + new Date().getTime());
			}
			blinkModified=true;
			updateBlinkPanelButtons();
		}
	});
	
	for(var index in blink.presentation ){	
		var elm = $('#presentation-sound-form' + index);
		elm.ajaxForm(function(data) {
			console.log("Presentation sound form " + data); 
			var json = JSON.parse(data);
			if (json.success){
				var src = "../courses/sounds/" + json.path;
				blinkData.presentation[json.id].sound = json.path;
				var elm = $('#presentation-sound' + json.id);
				if (elm!=null){
					elm.attr("src", src+"?timestamp=" + new Date().getTime());
				}
				blinkModified=true;
				updateBlinkPanelButtons();
			}
		});
		
		$('#presentation-sound-delete' + index).on("click", function(){
			var idx = parseInt(this.id.substr(25));
			blinkData.presentation[idx].sound = "";
			var elm = $('#presentation-sound' + idx);
			if (elm!=null) elm.attr("src", "");
			blinkModified=true;
			updateBlinkPanelButtons();
		});
		
		elm = $( '#presentation-font' + index );
		elm.change(function () {
			var elm = document.getElementById(this.id);
			var idx = parseInt(this.id.substr(17));
			console.log("Presentation font change index:" + idx + " value:" + elm.selectedIndex);
			blinkData.presentation[idx].font = elm.selectedIndex;
			blinkModified=true;
			updateBlinkPanelButtons();
		 });
		 if (blinkData.presentation[index].font!=null) $('#presentation-font' + index + " option")[blinkData.presentation[index].font].selected = true;
		 
		elm = $( '#presentation-alignment' + index );
		elm.change(function () {
			var elm = document.getElementById(this.id);
			var idx = parseInt(this.id.substr(22));
			console.log("Presentation alignment change index:" + idx + " value:" + elm.selectedIndex);
			blinkData.presentation[idx].alignment = elm.selectedIndex;
			blinkModified=true;
			updateBlinkPanelButtons();
		 });
		 if (blinkData.presentation[index].alignment!=null) $('#presentation-alignment' + index + " option")[blinkData.presentation[index].alignment].selected = true;
		 
		 elm = $( '#presentation-size' + index );
		 elm.change(function () {
			var elm = document.getElementById(this.id);
			var idx = parseInt(this.id.substr(17));
			console.log("Presentation size change index:" + idx + " value:" + elm.selectedIndex);
			blinkData.presentation[idx].size = elm.selectedIndex;
			blinkModified=true;
			updateBlinkPanelButtons();
		 });
		 if (blinkData.presentation[index].size!=null) $('#presentation-size' + index + " option")[blinkData.presentation[index].size].selected = true;
		 
		 elm = $( '#presentation-color' + index );
		 elm.change(function () {
			var elm = document.getElementById(this.id);
			var idx = parseInt(this.id.substr(18));
			console.log("Presentation color change index:" + idx + " value:" + elm.selectedIndex);
			blinkData.presentation[idx].color = elm.value;
			blinkModified=true;
			updateBlinkPanelButtons();
		 });
		 if (blinkData.presentation[index].color!=null){
			 if (typeof blinkData.presentation[index].color === 'string'){
				 $('#presentation-color' + index).val(blinkData.presentation[index].color);
			 }else{
			 	$('#presentation-color' + index + " option") [blinkData.presentation[index].color].selected = true;
			 }
		 }
	}
}

function presentationTextKeyup(elm){
	$('#'+elm.id).blur();
    $('#'+elm.id).focus();
}

function presentationTextChange(elm, idx){
	console.log("presentationTextChange index:" + idx + " value:" + elm.value);
	var blink = blinkData;
	blink.presentation[idx].text = elm.value;
	blinkModified = true;
	updateBlinkPanelButtons();
}

function presentationTimer(idx){
	var blink = blinkData;
	blink.presentation[idx].time++;
	if (blink.presentation[idx].time>8) blink.presentation[idx].time = 0;
	$('#presentation_time' + idx).attr('src', 'images/btn_time' + blink.presentation[idx].time + '.png');
	blinkModified = true;
	updateBlinkPanelButtons();
}

function presentationAnimation( idx, out ){
	var blink = blinkData;
	if (out){
		blink.presentation[idx].out++;
		if (blink.presentation[idx].out>5) blink.presentation[idx].out = 0;
		$('#presentation_anim_out' + idx).attr('src', 'images/btn_anim' + blink.presentation[idx].out + '.png');
	}else{
		blink.presentation[idx].in++;
		if (blink.presentation[idx].in>5) blink.presentation[idx].in = 0;
		$('#presentation_anim_in' + idx).attr('src', 'images/btn_anim' + blink.presentation[idx].in + '.png');
	}
	blinkModified = true;
	updateBlinkPanelButtons();
}

function presentationDelete(idx){
	var blink = blinkData;
	blink.presentation.splice(idx, 1);
	blinkModified = true;
	showPresentationBlink();
}

function addPresentationText(){
	var data = new Object();
	data.text = $('#presentation-text')[0].value;
	data.in = 0;
	data.time = 0;
	data.sound = "";
	var blink = blinkData;
	if (blink.presentation==null) blink.presentation = new Array();
	blink.presentation.push(data);
	blinkModified = true;
	updateBlinkPanelButtons();
	showPresentationBlink();
	addPresentationEvents(blink);
}