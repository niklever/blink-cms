// JavaScript Document

function showCatchBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getCatchBlink());
	setTimeout(setScrollHeight, 10);
}

function addCatchEvents(blink){
	var elm = $('#catch-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.catch.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.catch.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.catch.timer);
	elm.width(60);
	
	var elm = $('#catch-speed');
	elm.spinner({min:1, max:10, stop: function( event, ui ) {
		blink.catch.speed = $(this).spinner("value");
		console.log("Spinner change to " + blink.catch.speed);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.catch.speed);
	elm.width(60);
	
	var elm = $('#catch-frequency');
	elm.spinner({min:1, max:10, stop: function( event, ui ) {
		blink.catch.frequency = $(this).spinner("value");
		console.log("Spinner change to " + blink.catch.frequency);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.catch.frequency);
	elm.width(60);
	
	$("#catch-prompt").keyup(function(){
		$("#catch-prompt").blur();
		$("#catch-prompt").focus();
	});

	$('#catch-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.catch.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	elm = $('#catch-ordered');	
	elm.change( function(){
		console.log("blink catch ordered changed " + this.checked);
		blink.catch.ordered = (this.checked);
		showCatchBlink();
		addCatchEvents(blink);
		return;
	});
		
	for(var i=0; i<blink.catch.items.length; i++){
		if (blink.catch.ordered){
			var elm = $('#catch-slot' + (i+1));
			elm.spinner({min:0, max:6, stop: function( event, ui ) {
				var idx = parseInt(this.id.substr(10));
				blink.catch.items[idx-1].slot = $(this).spinner("value");
				console.log("Spinner change to " + blink.catch.items[idx-1].slot);
				blinkModified=true;
				updateBlinkPanelButtons(); 
			}});
			elm.spinner("value", blink.catch.items[i].slot);
			elm.width(60);
		}
		
		elm = $('#catch-bonus' + (i+1));
		elm.change(function(){
			var idx = parseInt(this.id.substr(11));
			var index = $('#' + this.id + ' option:selected').index();
			blink.catch.items[idx-1].bonus = index;
			console.log("Item bonus changed " + index);
			blinkModified = true;
			updateBlinkPanelButtons();
		});
		elm.prop("selectedIndex", blink.catch.items[i].bonus);
		
		if (blink.catch.items[i].isImage){
			var elm = $('#catch-image-form' + (i+1));
			elm.ajaxForm(function(data) {
				console.log("Catch image form " + data); 
				var json = JSON.parse(data);
				if (json.success){
					var src = "../courses/images/" + json.path;
					blink.catch.items[json.id].image = json.path;
					var elm = $('#catch-image' + (json.id+1));
					if (elm!=null){
						elm.attr("src", src+"?timestamp=" + new Date().getTime());
					}
					blinkModified=true;
					updateBlinkPanelButtons();
				}
			});
		}else{
			elm = $('#catch' + (i+1));	
			elm.keyup(function(){
				//console.log("Keyup " + this.id);
				$("#" + this.id).blur();
				$("#" + this.id).focus();
			});
		
			elm.change( function(){
				console.log("blink catch changed " + this.value);
				var idx = parseInt(this.id.substr(5));
				blink.catch.items[idx-1].text = this.value;
				blinkModified=true;
				updateBlinkPanelButtons(); 
			});
		}
		
		elm = $('#catch-isimage' + (i+1));	
		elm.change( function(){
			console.log("blink catch isImage changed " + this.checked);
			var idx = parseInt(this.id.substr(13));
			blink.catch.items[idx-1].isImage = (this.checked);
			showCatchBlink();
			addCatchEvents(blink);
			return;
		});
		
		elm = $('#catch-correct' + (i+1));	
		elm.change( function(){
			console.log("blink catch correct changed " + this.checked);
			var idx = parseInt(this.id.substr(13));
			blink.catch.items[idx-1].correct = (this.checked);
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}
	
	addFeedbackEvents();
	addPromptSoundEvents(blink.catch);
}

function getCatchBlink(){
	var blink = blinkData;
	if (blink.catch==null){
		blink.catch = new Object();
		blink.catch.prompt = "";
		blink.catch.items = new Array();
		blink.catch.ordered = true;
		blink.catch.timer = 0;
		blink.catch.sound = "";
		blink.catch.speed = 5;
		blink.catch.frequency = 5;
	}
	var str = '<table width="100%"><tr><td class="left" width="20%">Prompt</td>';
    str += '<td>';
    str += '<textarea id="catch-prompt" placeholder="Enter text prompt.." style="width:100%; height:60px;">' + blink.catch.prompt + '</textarea>';
    str += '<td class="buttons"></td></tr>';
	str += getPromptSoundHTML(blink.catch);
	var ordered = (blink.catch.ordered) ? "checked" : "";
	str += '<tr><td class="left" width="20%">Catch</td><td>Ordered<input id="catch-ordered" type="checkbox" ' + ordered + '> Time (in secs, 0 for not timed)&nbsp;<input id="catch-timer"></td><td class="buttons"></td></tr>';
	str += '<tr><td class="left">Difficulty</td><td> Speed&nbsp;<input id="catch-speed"> Frequency&nbsp;<input id="catch-frequency"></td><td class="buttons"><a href="#"><img class="button" src="images/btn_plus.png" title="Add Item" onclick="catchAddItem()" /></a></td></tr>';
	if (blink.catch!=null && blink.catch.items!=null){
		for(var i=0; i<blink.catch.items.length; i++){
			str += '<tr><td class="left">Item ' + (i+1) + '</td>';
			var isImage = (blink.catch.items[i].isImage) ? "checked" : "";
			var correct = (blink.catch.items[i].correct) ? "checked" : "";
			str += '<td>Is image?<input id="catch-isimage' + (i+1) +'" type="checkbox" ' + isImage + '> Correct?<input id="catch-correct' + (i+1) +'" type="checkbox" ' + correct + '>';
			str += ' Bonus <select id="catch-bonus' + (i+1) + '"><option>None</option><option>Extra points</option><option>Extra time</option></select>';
			if (ordered){
				if (blink.catch.items[i].slot == null) blink.catch.items[i].slot = 0;
				str += ' Slot (0 for no slot)<input id="catch-slot' + (i+1) + '"><br/>';
			}else{
				str += '<br/>';
			}
			if (isImage){
				str += '<form id="catch-image-form' + (i+1) + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
				str += '<input type="file" name="image"/>';
				str += '<input type="hidden" name="method" value="indexed_image" />';
				str += '<input type="hidden" name="id" value="' + i + '" />';
				str += '<input type="submit" value="Upload" />';
				str += '</form>';
				str += '<br/><img id="catch-image' + (i+1) + '" src="../courses/images/' + blink.catch.items[i].image + '" /></td>';
			}else{
			    str += '<input id="catch' + (i+1) + '"type="text" value="' + blink.catch.items[i].text + '" style="width:200px"></td>';
			}
			str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this item" onclick="catchDeleteItem(' + i + ')" ></td></tr>';
		}
	}
	
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function catchAddItem(){
	var blink = blinkData;
	var obj = { isImage:false, correct:true, text:"", image:"", bonus:0 };
	blink.catch.items.push( obj );
	showCatchBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function catchDeleteItem(idx){
	var blink = blinkData;
	blink.catch.items.splice(idx, 1);
	showCatchBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}