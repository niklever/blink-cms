// JavaScript Document
var blinkModified = false;
var blinkHeight;
var burstNotUnit;

function getBlinkHTML(blink){
	var str = blink.json.replace(/[\\]/g, "");
	blinkData = JSON.parse(str);
	//str = '<div style="background-color:#FFF;">';
	str = '<table width="100%">';
	str += '<tr><td class="left" width="20%">Summary</td><td colspan="2"><input id="blink-summary" type="text" value="' + blink.summary + '" style="width:90%;"/></td>';
	str += '<td class="buttons"><a href="#"><img id="btn_blink_save" class="button" src="images/btn_save.png" title="Save changes" onclick="saveBlink()"/></a></td></tr>';
	str += '<tr><td colspan="3"></td></tr>'
	str += '</table>';
	str += '<div id="blink-content">';
	
	switch(blink.type){
		case 1://Learning Unit start
		str += getIntroBlink(false) + '</div>';
		break;
		case 3://Burst start
		str += getIntroBlink(true) + '</div>';
		break;
		case 5://E Book
		str += getEbookBlink() + '</div>';
		break;
		case 6://Presentation
		str += getPresentationBlink() + '</div>';
		break;
		case 7://Input
		str += getInputBlink() + '</div>';
		break;
		case 8://Build a statement
		str += getStatementBlink() + '</div>';
		break;
		case 9://Word fill
		str += getWordfillBlink() + '</div>';
		break;
		case 10://Drag and drop
		str += getDragdropBlink() + '</div>';
		break;
		case 11://Simon
		str += getSimonBlink() + '</div>';
		break;
		case 12://Order Items
		str += getOrderItemsBlink() + '</div>';
		break;
		case 13://This or that
		str += getThisThatBlink() + '</div>';
		break;
		case 14://Question
		str += getQuestionBlink() + '</div>';
		break;
		case 15://Statement Rotator
		str += getRotatorBlink() + '</div>';
		break;
		case 16://Wordheat
		str += getWordHeatBlink() + '</div>';
		break;
		case 17://Video
		str += getVideoBlink() + '</div>';
		break;
		case 18://Catch Game
		str += getCatchBlink() + '</div>';
		break;
		case 19://Conversation
		str += getConversationBlink() + '</div>';
		break;
		case 20://Multiple
		str += getMultipleBlink() + '</div>';
		break;
		default:
		str += '<table width="100%"><tr><td class="left" width="20%">Content</td><td colspan="2">This will be type specific content</td></table></div>';
		break;
	}
	
	//str += '</table></div>';
	
	return str;
}

function getFeedbackRowsForItem(item, index, str, multiple){
	if (item.feedback == null){
		if (multiple){
		 	item.feedback = [ "", "" ];
		}else{
			item.feedback = "";
		}
	}else{
		if (multiple){
			if (item.feedback instanceof String) item.feedback = [ "", "" ];
		}else{
			if (item.feedback instanceof Array) item.feedback = "";
		}
	}
	var title = str + ' ' + (index+1);
	if (multiple){
		str = '<tr><td class="left">' + title + ' Correct</td><td><textarea id="feedback-correct' + index + '" placeholder="Enter the text the user will see for correct. Leave blank for no feedback." style="width:100%; height:60px;">' + item.feedback[0] + '</textarea></td><td class="buttons"></td></tr>';
		str += '<tr><td class="left">' + title + ' Incorrect</td><td><textarea id="feedback-incorrect' + index + '" placeholder="Enter the text the user will see for incorrect. Leave blank for no feedback." style="width:100%; height:60px;">' + item.feedback[1] + '</textarea></td><td class="buttons"></td></tr>';
	}else{
			str = '<tr><td class="left">' + title + ' Feedback</td><td><textarea id="blink-feedback' + index + '" placeholder="Enter the text the user will see. Leave blank for no feedback." style="width:100%; height:60px;">' + item.feedback + '</textarea></td><td class="buttons"></td></tr>';
	}
	return str;
}

function getFeedbackRows(feedback, score, multiple){
	if (blinkData.feedback == null){
		blinkData.feedback = [ "", "" ];
		blinkData.score = 0;
		blinkData.gems = 0;
	}
	
	if (feedback==null) feedback = true;
	if (score==null) score = true;
	if (multiple==null) multiple = true;
	
	var str = "";
	
	if (feedback){
		if (multiple){
			str += '<tr><td class="left" width="20%">Pass</td><td><textarea id="blink-feedback-pass" placeholder="Enter the text the user will see if they succeed. Leave blank for no feedback." style="width:100%; height:60px;">' + blinkData.feedback[0] + '</textarea></td><td class="buttons"></td></tr>';
			str += '<tr><td class="left" width="20%">Fail</td><td><textarea id="blink-feedback-fail" placeholder="Enter the text the user will see if they fail. Leave blank for no feedback." style="width:100%; height:60px;">' + blinkData.feedback[1] + '</textarea></td><td class="buttons"></td></tr>';
		}else{
			str += '<tr><td class="left" width="20%">Feedback</td><td><textarea id="blink-feedback" placeholder="Enter the text the user will see at the end of this Blink. Leave blank for no feedback." style="width:100%; height:60px;">' + blinkData.feedback + '</textarea></td><td class="buttons"></td></tr>';
		}
	}
	
	if (score){
		str += '<tr><td class="left" width="20%">Score</td><td><input type="text" id="blink-score" value="' + blinkData.score + '" /></td><td class="buttons"></td></tr>';
		str += '<tr><td class="left" width="20%">Gems</td><td><input type="text" id="blink-gems" value="' + blinkData.gems + '" /></td><td class="buttons"></td></tr>';
	}
	
	return str;
}

function addFeedbackEvents(feedback, score, multiple){
	if (feedback==null) feedback = true;
	if (score==null) score = true;
	if (multiple==null) multiple = true;
	
	if (feedback){
		if (multiple){
			$("#blink-feedback-pass").keyup(function(){
					$("#blink-feedback-pass").blur();
					$("#blink-feedback-pass").focus();
				});
					
			$('#blink-feedback-pass').change( function(){
				console.log(this.value);
				blinkData.feedback[0] = this.value;
				blinkModified=true;
				updateBlinkPanelButtons(); 
			});
			
			$("#blink-feedback-fail").keyup(function(){
				$("#blink-feedback-fail").blur();
				$("#blink-feedback-fail").focus();
			});
				
			$('#blink-feedback-fail').change( function(){
				console.log(this.value);
				blinkData.feedback[1] = this.value;
				blinkModified=true;
				updateBlinkPanelButtons(); 
			});
		}else{
			$("#blink-feedback").keyup(function(){
					$("#blink-feedback").blur();
					$("#blink-feedback").focus();
				});
					
			$('#blink-feedback').change( function(){
				console.log(this.value);
				blinkData.feedback = this.value;
				blinkModified=true;
				updateBlinkPanelButtons(); 
			});
		}
	}
	
	if (score){
		$("#blink-score").keyup(function(){
			$("#blink-score").blur();
			$("#blink-score").focus();
		});
			
		$('#blink-score').change( function(){
			console.log(this.value);
			blinkData.score = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		$("#blink-gems").keyup(function(){
			$("#blink-gems").blur();
			$("#blink-gems").focus();
		});
			
		$('#blink-gems').change( function(){
			console.log(this.value);
			blinkData.gems = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}
}

function getPromptSoundHTML(blinkdata){
	str = '<tr><td class="left">Prompt Sound</td>';
	str += '<td><form id="prompt-sound-form" method="post" action="blink_update.php" enctype="multipart/form-data">';
	str += '<input type="file" name="sound"/>';
	str += '<input type="hidden" name="method" value="sound" />';
	str += '<input type="submit" value="Upload" />';
	str += '</form></td>';
	str += '<td class="buttons"><audio id="prompt-sound" controls><source src="../courses/sounds/' + blinkdata.sound + '" type="audio/mpeg"></audio>';
	str += '<img class="button" src="images/btn_delete_sound.png" id="prompt-sound-delete" title="Delete this sound"></td></tr>';
	return str;
}

function addPromptSoundEvents(blinkdata){
	var elm = $('#prompt-sound-form');
	elm.ajaxForm(function(data) {
		console.log("Prompt sound form " + data); 
		var json = JSON.parse(data);
		if (json.success){
			var src = "../courses/sounds/" + json.path;
			blinkdata.sound = json.path;
			var elm = $('#prompt-sound');
			if (elm!=null){
				elm.attr("src", src+"?timestamp=" + new Date().getTime());
			}
			blinkModified=true;
			updateBlinkPanelButtons();
		}
	});
	
	elm = $('#prompt-sound-delete');
	elm.on("click", function(){
		blinkdata.sound = "";
		var elm = $('#prompt-sound');
		if (elm!=null) elm.attr("src", "");
		blinkModified=true;
		updateBlinkPanelButtons();
	});
}

function addBlinkPanelEvents(){ 
	var blink = blinkData;
	
	if (blink.intro!=null) addIntroEvents(blink);
	if (blink.presentation != null) addPresentationEvents(blink);
	if (blink.input!=null) addInputEvents(blink);
	if (blink.wordfill!=null) addWordfillEvents(blink);
	if (blink.statement!=null) addStatementEvents(blink);
	if (blink.dragdrop !=null) addDragdropEvents(blink);
	if (blink.question !=null) addQuestionEvents(blink);
	if (blink.rotator !=null) addRotatorEvents(blink);
	if (blink.wordheat !=null) addWordHeatEvents(blink);
	if (blink.video !=null) addVideoEvents(blink);
	if (blink.thisthat !=null) addThisThatEvents(blink);
	if (blink.simon !=null) addSimonEvents(blink);
	if (blink.orderitems !=null) addOrderItemsEvents(blink);
	if (blink.ebook !=null) addEbookEvents(blink);
	if (blink.catch !=null) addCatchEvents(blink);
	if (blink.conversation !=null) addConversationEvents(blink);
	if (blink.multiple !=null) addMultipleEvents(blink);
	
	$("#blink-summary").keyup(function(){
        $("#blink-summary").blur();
        $("#blink-summary").focus();
	});
	
	$('#blink-summary').change(function(){
		var summary = $('#blink-summary')[0].value;
		//console.log("Summary: " + summary);
		blink = course.get(blinkEditInfo.index);
		blink.summary = summary;
		blinkModified = true;
		updateBlinkPanelButtons();
	});
}

function addFeedbackEventsForItem(item, index, multiple){
	if (multiple){
		$("#feedback-correct" + index).keyup(function(){
				$("#feedback-correct" + index).blur();
				$("#feedback-correct" + index).focus();
			});
				
		$('#feedback-correct' + index).change( function(){
			console.log(this.value);
			item.feedback[0] = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		$("#feedback-incorrect" + index).keyup(function(){
				$("#feedback-incorrect" + index).blur();
				$("#feedback-incorrect" + index).focus();
			});
				
		$('#feedback-incorrect' + index).change( function(){
			console.log(this.value);
			item.feedback[1] = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}else{
		$("#blink-feedback" + index).keyup(function(){
				$("#blink-feedback" + index).blur();
				$("#blink-feedback" + index).focus();
			});
				
		$('#blink-feedback' + index).change( function(){
			console.log(this.value);
			item.feedback = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}
}

function setScrollHeight(){
	$("blinks").attr("overflow", "auto");
}

function enableButton(id, mode){
	var elm = $(id);
	elm[0].style.pointerEvents = (mode) ? 'auto' : 'none';
	var opacity = (mode) ? 1 : 0.5;
	elm.css('opacity', opacity);
}

function updateBlinkPanelButtons(){
	enableButton('#btn_blink_save', blinkModified);
	//enableButton('#btn_prev', index>0);
	//enableButton('#btn_next', index<(blinkTotal-1));
}

function prevBlink(){
	window.location = "blink.php?method=prev&courseId=" + courseId + "&index=" + (index-1);
}

function nextBlink(){
	window.location = "blink.php?method=next&courseId=" + courseId + "&index=" + (index+1);
}

function escapeJSON(str) {
	if (str==null) return;
	
	var str = str.replace(/\\n/g, '&#10;');
	//console.log(str);
	
	str1 = str.replace(/[\\\\]/g, '');
	
	var str2 = str1
		.replace(/[']/g, '&#39;')
	  	.replace(/[\\]/g, '\\\\')
		.replace(/[\"]/g, '\\\"')
		.replace(/[\b]/g, '\\b')
		.replace(/[\f]/g, '\\f')
		.replace(/[\r]/g, '\\r')
		.replace(/[\t]/g, '\\t');
	
	//.replace(/[\/]/g, '\\/')	
	//console.log("escapeJSON:%s\n%s\n%s", str, str1, str2);
	
	return str2;
}

function replaceQuotes(object){
	if (typeof(object) === 'string'){
		object = object.replace(/[\"]/g, '&#34;');
	}else{
		var index = 0, item;
		for(var prop in object){
			item = object[prop];
			if(item!= null && typeof item=='object'){
				item = replaceQuotes(item);
			}else if (item!=null && typeof item=='string'){
				object[prop] = item.replace(/[\"]/g, '&#34;');
			}
		}
	}
    return object;
}

function saveBlink(){
	var blink = replaceQuotes(blinkData);
	var summary = $('#blink-summary')[0].value;
	var json = JSON.stringify(blink);
	var safe_json = escapeJSON(json);
	$('#overlay').show();
	var growth = 0;
	if (blinkEditInfo.growth != null && blinkEditInfo.growth){
		var blk = course.getGA(blinkEditInfo.index);
		growth = 1;
	}else{
		var blk = course.get(blinkEditInfo.index);
	}
	//var blk = blinkData;
	console.log("saveBlink:%d, %s", blk.id, safe_json);
	blk.json = safe_json;
	$.post("blink_update.php", { id: blk.id, method:'update', summary:summary, index:blinkEditInfo.index, type:blk.type, courseId:course.id, json: safe_json, growth:growth })
	  .done(function( msg ) {
		  console.log('saveBlink ' + msg);
		if (blinkEditInfo.growth != null && blinkEditInfo.growth){
			var blk = course.getGA(blinkEditInfo.index);
		}else{
			var blk = course.get(blinkEditInfo.index);
		}
		if (blk.id==undefined || blk.id==null){
			var tmp = JSON.parse(msg);
			if (tmp.success) blk.id = tmp.id;
		}
		//blinkData = blk;
		blinkModified = false;
		updateBlinkPanelButtons();
		$('#overlay').hide();
	  });
}

function showFeedback(msg){
	console.log("showFeedback:" + msg);
	var elm = $("#blink-feedback-msg");
	elm.text(msg);
	$( "#blink-feedback" ).dialog('open');
}