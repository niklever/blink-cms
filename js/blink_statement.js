// JavaScript Document

function showStatementBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getStatementBlink());
	setTimeout(setScrollHeight, 10);
}

function addStatementEvents(blink){
	var elm = $('#statement-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.statement.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.statement.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.statement.timer);
	elm.width(60);
	
	$("#statement-prompt").keyup(function(){
		$("#statement-prompt").blur();
		$("#statement-prompt").focus();
	});

	$('#statement-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.statement.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	elm = $('#statement-wrong');	
	elm.keyup(function(){
		//console.log("Keyup " + this.id);
		$("#" + this.id).blur();
		$("#" + this.id).focus();
	});

	elm.change( function(){
		console.log("blink statement wrong changed " + this.value);
		var idx = parseInt(this.id.substr(9));
		blink.statement.wrong = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	elm = $('#statement-ordered');	
	elm.change( function(){
		console.log("blink statement ordered changed " + this.checked);
		blink.statement.ordered = (this.checked);
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
		
	for(var i=0; i<blink.statement.statements.length; i++){
		elm = $('#statement' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink statement changed " + this.value);
			var idx = parseInt(this.id.substr(9));
			blink.statement.statements[idx-1] = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}
	
	addFeedbackEvents();
	addPromptSoundEvents(blink.statement);
}

function getStatementBlink(){
	var blink = blinkData;
	if (blink.statement==null){
		blink.statement = new Object();
		blink.statement.prompt = "";
		blink.statement.wrong = "";
		blink.statement.statements = new Array();
		blink.statement.ordered = true;
		blink.statement.timer = 0;
		blink.statement.sound = "";
	}
	var str = '<table width="100%"><tr><td class="left" width="20%">Prompt</td>';
    str += '<td>';
    str += '<textarea id="statement-prompt" placeholder="Enter text prompt. For each statement use | to indicate block break." style="width:100%; height:60px;">' + blink.statement.prompt + '</textarea>';
    str += '<td class="buttons">';
	var ordered = (blink.statement.ordered) ? "checked" : "";
	str += '<a href="#"><img class="button" src="images/btn_plus.png" title="Add Statement" onclick="statementAddStatement()" /></a></td></tr>';
	str += getPromptSoundHTML(blink.statement);
	str += '<tr><td class="left" width="20%">Statement</td><td>Ordered<input id="statement-ordered" type="checkbox" ' + ordered + '> Time (in secs, 0 for not timed)&nbsp;<input id="statement-timer"></td><td class="buttons"></td></tr>';
	str += '<tr><td class="left">Wrong</td><td><input id="statement-wrong" value="' + blink.statement.wrong + '" style="width:100%"></td><td class="buttons"></td></tr>';
	if (blink.statement!=null && blink.statement.statements!=null){
		for(var i=0; i<blink.statement.statements.length; i++){
			str += '<tr><td class="left">Statement ' + (i+1) + '</td>';
			str += '<td><input id="statement' + (i+1) + '" value="' + blink.statement.statements[i] + '" style="width:100%"></td>';
			str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this statement" onclick="statementDeleteStatement(' + i + ')" ></td></tr>';
		}
	}
	
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function statementAddStatement(){
	var blink = blinkData;
	if (blink.statement.statements.length>=6){
		alert("The maximum statements is 6");
	}else{
		blink.statement.statements.push( "");
		showStatementBlink();
		addBlinkPanelEvents();
		blinkModified = true;
		updateBlinkPanelButtons();
	}
}

function statementDeleteStatement(idx){
	var blink = blinkData;
	blink.statement.statements.splice(idx, 1);
	showStatementBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}