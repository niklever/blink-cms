// JavaScript Document
function Course(obj){
	this.id = obj.id;
	this.title = obj.title;
	this.summary = obj.summary;
	this.description = obj.description;
	this.iconUrl = obj.iconUrl;
	this.imageUrl = obj.imageUrl;
	this.blinks = obj.blinks;
	this.growth = obj.growth;
	
	this.move = function(idx, up){
		if (idx==0 && up) return;
		if (idx==(this.blinks.length-1) && !up) return;
		if (idx<0 || idx>=this.blinks.length) return;
		var count = 1;
		var blink = this.blinks[idx];
		if (blink.type==1 || blink.type==3){
			var closingType = blink.type + 1;
			while(this.blinks[idx + count].type != closingType && (idx+count)<this.blinks.length) count++;
			//Include the closing blink
			count++;
		}
		var extracted = this.blinks.splice(idx, count);
		if (up){
			var insertIdx = idx-1;
			while(insertIdx>0 && !this.blinks[insertIdx].visible  && !this.blinks[insertIdx].lastExpanded) insertIdx--;
			if (count==1){
				this.blinks.splice(insertIdx, 0, extracted[0]);
			}else{
				while(extracted.length>0){
					this.blinks.splice(insertIdx, 0, extracted.pop());
				}
			}
		}else{
			var insertIdx = idx + 1;
			if (insertIdx<this.blinks.length){
				if (this.blinks[insertIdx].type!=2 || this.blinks[insertIdx].type!=4){
					while(insertIdx<this.blinks.length && !this.blinks[insertIdx].visible) insertIdx++;
				}else{
					insertIdx--;
				}
			}
			var type = (insertIdx<this.blinks.length) ? this.blinks[insertIdx].type : 'end';
			console.log("Moving down idx:" + idx + " insertIdx:" + insertIdx + " blinkTotal:" + this.blinks.length + " insertIdx type:" + type);
			//if (insertIdx<this.blinks.length && (this.blinks[insertIdx].type==2 || this.blinks[insertIdx].type==4)) insertIdx--;
			if (count==1){
				if (insertIdx==this.blinks.length){
					this.blinks.push(extracted[0]);
				}else{
					this.blinks.splice(insertIdx, 0, extracted[0]);
				}
			}else{
				while(extracted.length>0){
					if (insertIdx==this.blinks.length){
						this.blinks.push(extracted.pop());
					}else{
						this.blinks.splice(insertIdx, 0, extracted.pop());
					}
				}
			}
		}
		
		//console.log(this.toString());
	}
	
	this.delete = function(idx){	
		var count = 1;
		var blink = this.blinks[idx];
		if (blink.type==1 || blink.type==3){
			var closingType = blink.type + 1;
			while(this.blinks[idx + count].type != closingType && (idx+count)<this.blinks.length) count++;
			//Include the closing blink
			count++;
		}
		this.blinks.splice(idx, count);
	}
	
	this.push = function(blink){
		this.blinks.push(blink);
	}
	
	this.add = function(idx, blink){
		this.blinks.splice(idx, 0, blink);
	}
	
	this.get = function(idx){
		return this.blinks[idx];
	}
	
	this.count = function(){
		return this.blinks.length;
	}
	
	this.toString = function(){
		var str = this.title + "\n";
		for(var i=0; i<this.blinks.length; i++){
			str += this.blinks[i].toString() + ", ";
		}
		str += "Growing Activities\n";
		for(var i=0; i<this.growth.length; i++){
			str += this.growth[i].toString() + ", ";
		}
		return str;
	}
	
	this.moveGA = function(idx, up){
		if (idx==0 && up) return;
		if (idx==(this.growth.length-1) && !up) return;
		if (idx<0 || idx>=this.growth.length) return;
		var count = 1;
		var blink = this.growth[idx];
		if (blink.type==1 || blink.type==3){
			var closingType = blink.type + 1;
			while(this.growth[idx + count].type != closingType && (idx+count)<this.blinks.length) count++;
			//Include the closing blink
			count++;
		}
		var extracted = this.growth.splice(idx, count);
		if (up){
			var insertIdx = idx-1;
			while(insertIdx>0 && !this.growth[insertIdx].visible  && !this.growth[insertIdx].lastExpanded) insertIdx--;
			if (count==1){
				this.growth.splice(insertIdx, 0, extracted[0]);
			}else{
				while(extracted.length>0){
					this.growth.splice(insertIdx, 0, extracted.pop());
				}
			}
		}else{
			var insertIdx = idx + 1;
			if (insertIdx<this.growth.length){
				if (this.growth[insertIdx].type!=2 || this.growth[insertIdx].type!=4){
					while(insertIdx<this.growth.length && !this.growth[insertIdx].visible) insertIdx++;
				}else{
					insertIdx--;
				}
			}
			var type = (insertIdx<this.growth.length) ? this.growth[insertIdx].type : 'end';
			console.log("Moving down idx:" + idx + " insertIdx:" + insertIdx + " growthTotal:" + this.growth.length + " insertIdx type:" + type);
			//if (insertIdx<this.blinks.length && (this.blinks[insertIdx].type==2 || this.blinks[insertIdx].type==4)) insertIdx--;
			if (count==1){
				if (insertIdx==this.growth.length){
					this.growth.push(extracted[0]);
				}else{
					this.growth.splice(insertIdx, 0, extracted[0]);
				}
			}else{
				while(extracted.length>0){
					if (insertIdx==this.growth.length){
						this.growth.push(extracted.pop());
					}else{
						this.growth.splice(insertIdx, 0, extracted.pop());
					}
				}
			}
		}
		
		//console.log(this.toString());
	}
	
	this.countGA = function(){
		return this.growth.length;
	}
	
	this.deleteGA = function(idx){	
		var count = 1;
		var blink = this.growth[idx];
		if (blink.type==1 || blink.type==3){
			var closingType = blink.type + 1;
			while(this.growth[idx + count].type != closingType && (idx+count)<this.growth.length) count++;
			//Include the closing blink
			count++;
		}
		this.growth.splice(idx, count);
	}
	
	this.addGA = function(idx, blink){
		this.growth.splice(idx, 0, blink);
	}
	
	this.getGA = function(idx){
		return this.growth[idx];
	}
}
						   
function Blink(type, summary, lite, guid, id, json){
	this.type = type;
	this.summary = summary;
	this.expanded = false;
	this.lastExpanded = false;
	this.visible = false;
	
	if (json!=null){
		this.lite = lite;
		this.json = json;
		
		try{
			this.json = json.replace(/[\n]/g, "\\n").replace(/[\r]/g,"\\r").replace(/[\\]/g, "");
			var tokens = this.json.split("<br />");
			if (tokens.length>1){
				this.json = tokens.join("&#10;");
			}
			var data = JSON.parse(this.json);
			this.guid = guid;
			this.id = id;
			if (this.guid==null || this.guid===undefined || this.guid == ""){
				this.guid = Blink.makeGuid();
				console.log("No guid>> guid set to " + this.guid);
			}
		}catch(e){
			this.lite = lite;
			this.json = '{ "type":' + type + '}';
			this.guid = this.guid;
			console.log("Blink id:" + id + " error:" + e + " json:" + json);	
		}
	}else{
		this.lite = 0;
		this.json = '{ "type":' + type + '}';
		this.guid = Blink.makeGuid();
	}
	
	this.toString = function(){
		return "type:" + type + " expanded:" + this.expanded + " lastExpanded:" + this.lastExpanded + " visible:" + this.visible + " summary:" + this.summary;
	}
}

Blink.types = new Array("Learning unit start", "Learning unit end", "Burst start", "Burst end", "E-book", "Presentation", "Input", 
						   "Building a statement", "Word fill", "Drag and drop", "Simon", "Order items", "This and that", "Question", "Statement Rotator",
						   "Word heat", "Video", "Catch Game", "Conversation", "Multiple Answers", "Menu");
						   

Blink.makeGuid = function() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		return v.toString(16);
	});
};

function publishCourse(idx, mode, index){
	$.post("course_update.php", { id:idx, type:'publish', mode:mode, index:index })
	  .done(function( data ) {
		console.log( "publishCourse: " + data );
		var json = JSON.parse(data);
		var elm = $("#publish" + json.index);
		elm.attr('onclick','').unbind('click');
		elm.unbind( "click" );
		elm.click( function(){
				publishCourse(json.id, json.mode, json.index);
			});
		if (json.mode){
			elm.attr("src", "images/btn_publish.png");
			elm.attr('tooltipText', "Publish Course");
			elm.attr("title", "Publish Course");
		}else{
			elm.attr("src", "images/btn_unpublish.png");
			elm.attr("title", "Unpublish Course");
			elm.attr('tooltipText', "Unpublish Course");
		}
		//$( document ).tooltip();
		//showFeedback(data);
		//$('#overlay').hide();
	  });
}
