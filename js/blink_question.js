// JavaScript Document
function showQuestionBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getQuestionBlink());
	setTimeout(setScrollHeight, 10);
}

function questionUpdateTimer(){
	var elm = $('#question-timer');
	blink.question.timer = elm.spinner("value");
}

function addQuestionEvents(blink){
	var elm = $('#question-image-form');
	elm.ajaxForm(function(data) {
		console.log("Question image form " + data); 
		var json = JSON.parse(data);
		if (json.success){
			var src = "../courses/images/" + json.path;
			blinkData.image = json.path;
			var elm = $('#question-image');
			if (elm!=null){
				elm.attr("src", src+"?timestamp=" + new Date().getTime());
			}
			blinkModified=true;
			updateBlinkPanelButtons();
		}
	});
	
	var elm = $('#question-text');
	elm.keyup(function(){
		var elm = $('#' + event.target.id);
		elm.blur();
		elm.focus();
	});
	
	elm.change(function(){
		blink.question.text = this.value;
		console.log("Question text changed " + this.value);
		blinkModified = true;
		updateBlinkPanelButtons();
	});
	
	var elm = $('#question-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.question.timer = $(this).spinner("value");
		blinkModified=true;
		updateBlinkPanelButtons(); 
		console.log("Spinner change to " + blink.question.timer);
    }});
	elm.spinner("value", blink.question.timer);
	elm.width(60);
		
	for(var i=0; i<blink.question.answers.length; i++){
		var elm = $('#question-answer-text' + (i+1));
		elm.keyup(function(){
			var elm = $('#' + event.target.id);
			elm.blur();
			elm.focus();
		});
		
		elm.change(function(){
			var idx = parseInt(this.id.substr(20));
			blink.question.answers[idx-1].text = this.value;
			console.log("Answer text changed " + this.value);
			blinkModified = true;
			updateBlinkPanelButtons();
		});
		
		addFeedbackEventsForItem(blink.question.answers[i], i, false);
	}
	
	addFeedbackEvents();
	addPromptSoundEvents(blink.question);
}

function getQuestionBlink(){
	var blink = blinkData;
	if (blink.question==null){
		blink.question = new Object();
		blink.question.timer = 60;
		blink.question.multiplechoice = true;
		blink.question.answers = new Array();
		blink.question.sound = "";
	}
	
	var checked = (blink.question.multiplechoice) ? "checked" : "";
	str = '<table width="100%" style="background-color:#FFF;">';
	str += '<tr>';
	str += '<td class="left" width="20%">Question</td>';
    str += '<td><textarea id="question-text" placeholder="Enter your text here" style="width:100%; height:60px;">' + blink.question.text + '</textarea><br/>';
	str += 'Time (in secs, 0 for not timed)&nbsp;<input id="question-timer">';
	str += '&nbsp;Multiple Choice<input type="checkbox" id="question-typechk" ' + checked + ' onclick="questionTypeChk(this)"/></td>';
    str += '<td class="buttons" width="20%">'
    str += '	<img src="images/btn_plus.png" title="Add Answer" onclick="questionAddAnswer()" class="button"/>';
    str += '</td></tr>';
	str += getPromptSoundHTML(blink.question);
	str += '<form id="question-image-form" method="post" action="blink_update.php" enctype="multipart/form-data">';
	str += '<tr><td class="left" width="20%">Image</td><td><input type="file" name="image"/>';
	str += '<input type="hidden" name="method" value="intro_image" />';
	str += '<input type="submit" value="Upload" />';
	str += '</form>';
	str += '<br/><img id="question-image" src="../courses/images/' + blinkData.image + '" /></td><td class="buttons"></td></tr>';
	
	for (var i=0; i<blink.question.answers.length; i++){
		str += '<tr><td class="left">Answer ' + (i+1) + '</td>';
    	//str += '<td>Left<input id="dragdrop-goal-left' + (i+1) + '">&nbsp;Top<input id="dragdrop-goal-top' + (i+1) + '">&nbsp;';
		var checked = (blink.question.answers[i].correct) ? "checked" : "";
		str += '<td>Text <input type="text" id="question-answer-text' + (i+1) + '" value="' + blink.question.answers[i].text + '" placeholder="Enter answer" style="width:300px;"/>&nbsp;';
    	str += 'Correct<input type="checkbox" id="question-answer-correctchk' + (i+1) + '"' + checked + ' onclick="questionAnswerChk(this, ' + i + ')"/></td>';
    	str += '</td><td class="buttons"><img src="images/btn_delete.png" title="Delete Answer" onclick="questionDeleteAnswer(' + i + ')" class="button"/></td></tr>';
		str += getFeedbackRowsForItem(blink.question.answers[i], i, "Answer", false);
	}
	
	str += getFeedbackRows(false);
	str += '</table>';

	return str;
}

function questionTypeChk(chk, idx){
	blinkModified = true;
	var blink = blinkData;
	blink.question.multiplechoice = chk.checked;
	updateBlinkButtons();
	addBlinkPanelEvents();
}

function questionAnswerChk(chk, idx){
	blinkModified = true;
	var blink = blinkData;
	blink.question.answers[idx].correct = chk.checked;
	updateBlinkButtons();
	addBlinkPanelEvents();
}

function questionAddAnswer(){
	var blink = blinkData;
	blink.question.answers.push( { correct:false, text:"" });
	showQuestionBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function questionDeleteAnswer(idx){
	var blink = blinkData;
	blink.question.answers.splice(idx, 1);
	showQuestionBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}