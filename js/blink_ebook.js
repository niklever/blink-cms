// JavaScript Document

function showEbookBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getEbookBlink());
	setTimeout(setScrollHeight, 10);
}

function addEbookEvents(blink){
	var elm = $('#ebook-form');
	elm.ajaxForm(function(data) {
		console.log("E-book form " + data); 
		var json = JSON.parse(data);
		if (json.success){
			var src = "../courses/sounds/" + json.path;
			blinkData.ebook = json.path;
			showEbookBlink();
		}
	});
}

function getEbookBlink(){
	var blink = blinkData;
	if (blink.ebook==null) blink.ebook = "";
	var str = '<table width="100%"><tr><td>';
    str += '<form id="ebook-form" method="post" action="blink_update.php" enctype="multipart/form-data">';
	str += '<tr><td class="left" width="20%">E-book</td><td><input type="file" name="ebook"/>';
	str += '<input type="hidden" name="method" value="ebook" />';
	str += '<input type="submit" value="Upload" />';
	str += '</form></td>';
	if (blink.ebook==""){
		str += '<td class="buttons"></td></tr>';
	}else{
		str += '<td class="buttons" width="15%"><a href="../courses/pdfs/' + blink.ebook + '" target="_blank"><img class="button" src="images/btn_doc.png" title="View e-book" ></a></td></tr>';
	}
	str += '</table>';
	return str;
}