// JavaScript Document
function getMenuBlink(){
	var str = '<table width="100%"><tr><td class="left" width="20%">New</td>';
    str += '<td>';
    str += '<select id="menutype" style="width:100%;">';
    str += '<option>Select menu type</option>';
    str += '<option>Learning units</option>';
	str += '<option>All Bursts</option>';
	str += '<option>Bursts in Learning unit</option>';
    str += '<option>Growing Activities</option>';
    str += '<option>Personal Dossiers</option>';
    str += '</select>';
    str += '<td class="buttons"><a href="#"><img src="images/btn_create.png" title="Create" onclick="createMenu()" /></a></td></tr>';
    str += getMenuHTML();
	str += '</table>';
	return str;
}

function showMenuBlink(){
	$('#blink-content').html(getMenuBlink());
	$("blinks").attr("overflow", "hidden");
	setTimeout(setScrollHeight, 10);
}

function getMenuHTML(){
	var blink = blinkData;
	str = "";
	if (blink.menu!=null){
		for(var i=0; i<blink.menu.length; i++){
			str += '<tr><td class="left">Menu Item ' + (i+1) + '</td><td>' + blink.menu[i].name + '</td><td class="buttons">';
			if (blink.menu[i].dynamic){
				str += '&nbsp;';
			}else{
				str += '<a href="#"><img id="btn_locked' + (i+1) + '" src="images/btn_unlocked.png" onclick="menuItemLocked(' + (i+1) + ')" ></a>'; 
			}
			str += '</td></tr>';
		}
	}
	return str;
}

function menuItemLocked(i){
	console.log('menuItemLocked:' + i);
}

function selectLearningUnitForBurstMenu(){
	var elm = $('#learning-units-selector');
	if (elm.length==0){
		$('<div id="learning-units-selector" title="Select learning unit"/>').appendTo('body');
		var elm = $('#learning-units-selector');
		var str = '<select id="learning-units-select"><option>Select learning unit</option>';
		for(var i=0; i<course.blinks.length; i++){
			if (course.blinks[i].type==1){
				str += ('<option value="' + i + '">' + course.blinks[i].summary + '</option>');
			}
		}
		str += '</select>';
		elm = $('#learning-units-selector');
		elm.html(str);
		elm.dialog({ 
		  height: 240,
		  resizable: false,
		  modal: true,
		  buttons: {
			"OK": function() {
				var firstIdx = parseInt($('#learning-units-select').val());
				var menu = new Array();
				for(var i=firstIdx+1; i<course.blinks.length; i++){
					if (course.blinks[i].type==3){
						menu.push({ name:course.blinks[i].summary, guid:course.blinks[i].guid });
					}
					if (course.blinks[i].type==2)  break;
				}
				blink.menu = menu;
				showMenuBlink();
				$( this ).dialog( "close" );
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		  }
		});
	}else{
		elm.dialog('open');
	}
}

function createMenu(){
	yesNoFunc = createMenuConfirm;
	$('#blink-confirm-yesno').attr("title", "Create Menu");
	$('#blink-confirm-yesno-msg').text("Do you want to make a new menu?");
	$('#blink-confirm-yesno').dialog('open');
}

function createMenuConfirm(){
	blinkModified = true;
	var elm = $('#menutype');
	var type = $('#menutype option:selected').index();
	menu = new Array();
	switch(type){
		case 1://Learning units
		for(var i=0; i<course.blinks.length; i++){
			if (course.blinks[i].type==1){
				menu.push({ name:course.blinks[i].summary, guid:course.blinks[i].guid, dynamic:false });
			}
		}
		break;
		case 2://All bursts
		for(var i=0; i<course.blinks.length; i++){
			if (course.blinks[i].type==3){
				menu.push({ name:course.blinks[i].summary, guid:course.blinks[i].guid, dynamic:false });
			}
		}
		break;
		case 3://Burst in unit
		selectLearningUnitForBurstMenu();
		return;
		break;
		case 4://Growing Activity
		menu.push({name:"Growing Activity", dynamic:true});
		break;
		case 5://Personal dossier
		menu.push({name:"Personal Dossier", dynamic:true});
		break;
	}
	var blink = blinkData;
	blink.menu = menu;
	showMenuBlink();
	updateBlinkPanelButtons();
	yesNoFunc=null;
}