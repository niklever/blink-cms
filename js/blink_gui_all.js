// JavaScript Document
var blinkModified = false;
var blinkHeight;
var burstNotUnit;

function getBlinkHTML(blink){
	var str = blink.json.replace(/[\\]/g, "");
	blinkData = JSON.parse(str);
	//str = '<div style="background-color:#FFF;">';
	str = '<table width="100%">';
	str += '<tr><td class="left" width="20%">Summary</td><td colspan="2"><input id="blink-summary" type="text" value="' + blink.summary + '" style="width:90%;"/></td>';
	str += '<td class="buttons"><a href="#"><img id="btn_blink_save" class="button" src="images/btn_save.png" title="Save changes" onclick="saveBlink()"/></a></td></tr>';
	str += '<tr><td colspan="3"></td></tr>'
	str += '</table>';
	str += '<div id="blink-content">';
	
	switch(blink.type){
		case 1://Learning Unit start
		str += getIntroBlink(false) + '</div>';
		break;
		case 3://Burst start
		str += getIntroBlink(true) + '</div>';
		break;
		case 5://Menu
		str += getMenuBlink() + '</div>';
		break;
		case 6://Presentation
		str += getPresentationBlink() + '</div>';
		break;
		case 7://Input
		str += getInputBlink() + '</div>';
		break;
		case 8://Build a statement
		str += getStatementBlink() + '</div>';
		break;
		case 9://Word fill
		str += getWordfillBlink() + '</div>';
		break;
		case 10://Drag and drop
		str += getDragdropBlink() + '</div>';
		break;
		case 13://This or that
		str += getThisThatBlink() + '</div>';
		break;
		case 14://Question
		str += getQuestionBlink() + '</div>';
		break;
		case 15://Statement Rotator
		str += getRotatorBlink() + '</div>';
		break;
		case 16://Wordheat
		str += getWordHeatBlink() + '</div>';
		break;
		case 17://Video
		str += getVideoBlink() + '</div>';
		break;
		default:
		str += '<table width="100%"><tr><td class="left" width="20%">Content</td><td colspan="2">This will be type specific content</td></table></div>';
		break;
	}
	
	//str += '</table></div>';
	
	return str;
}

function getFeedbackRows(){
	if (blinkData.feedback == null){
		blinkData.feedback = [ "", "" ];
		blinkData.score = 0;
		blinkData.gems = 0;
	}
	
	str = '<tr><td class="left" width="20%">Pass</td><td><textarea id="blink-feedback-pass" placeholder="Enter the text the user will see if they succeed. Leave blank for no feedback." style="width:100%; height:60px;">' + blinkData.feedback[0] + '</textarea></td></tr>';
	str += '<tr><td class="left" width="20%">Fail</td><td><textarea id="blink-feedback-fail" placeholder="Enter the text the user will see if they fail. Leave blank for no feedback." style="width:100%; height:60px;">' + blinkData.feedback[1] + '</textarea></td></tr>';
	str += '<tr><td class="left" width="20%">Score</td><td><input type="text" id="blink-score" value="' + blinkData.score + '" /></td></tr>';
	str += '<tr><td class="left" width="20%">Gems</td><td><input type="text" id="blink-gems" value="' + blinkData.gems + '" /></td></tr>';
	
	return str;
}

function addBlinkPanelEvents(){ 
	var blink = blinkData;
	
	if (blink.intro!=null) addIntroEvents(blink);
	if (blink.input!=null) addInputEvents(blink);
	if (blink.wordfill!=null) addWordfillEvents(blink);
	if (blink.statement!=null) addStatementEvents(blink);
	if (blink.dragdrop !=null) addDragdropEvents(blink);
	if (blink.question !=null) addQuestionEvents(blink);
	if (blink.rotator !=null) addRotatorEvents(blink);
	if (blink.wordheat !=null) addWordHeatEvents(blink);
	if (blink.video !=null) addVideoEvents(blink);
	if (blink.thisthat !=null) addThisThatEvents(blink);
	
	$("#blink-summary").keyup(function(){
        $("#blink-summary").blur();
        $("#blink-summary").focus();
	});
	
	$('#blink-summary').change(function(){
		var summary = $('#blink-summary')[0].value;
		//console.log("Summary: " + summary);
		blink = course.get(blinkEditInfo.index);
		blink.summary = summary;
		blinkModified = true;
		updateBlinkPanelButtons();
	});
}

function addFeedbackEvents(){
	$("#blink-feedback-pass").keyup(function(){
			$("#blink-feedback-pass").blur();
			$("#blink-feedback-pass").focus();
		});
			
	$('#blink-feedback-pass').change( function(){
		console.log(this.value);
		blinkData.feedback[0] = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	$("#blink-feedback-fail").keyup(function(){
		$("#blink-feedback-fail").blur();
		$("#blink-feedback-fail").focus();
	});
		
	$('#blink-feedback-fail').change( function(){
		console.log(this.value);
		blinkData.feedback[1] = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	$("#blink-score").keyup(function(){
		$("#blink-score").blur();
		$("#blink-score").focus();
	});
		
	$('#blink-score').change( function(){
		console.log(this.value);
		blinkData.score = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	$("#blink-gems").keyup(function(){
		$("#blink-gems").blur();
		$("#blink-gems").focus();
	});
		
	$('#blink-gems').change( function(){
		console.log(this.value);
		blinkData.gems = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
}

function setScrollHeight(){
	$("blinks").attr("overflow", "auto");
}

function enableButton(id, mode){
	var elm = $(id);
	elm[0].style.pointerEvents = (mode) ? 'auto' : 'none';
	var opacity = (mode) ? 1 : 0.5;
	elm.css('opacity', opacity);
}

function updateBlinkPanelButtons(){
	enableButton('#btn_blink_save', blinkModified);
	//enableButton('#btn_prev', index>0);
	//enableButton('#btn_next', index<(blinkTotal-1));
}

function prevBlink(){
	window.location = "blink.php?method=prev&courseId=" + courseId + "&index=" + (index-1);
}

function nextBlink(){
	window.location = "blink.php?method=next&courseId=" + courseId + "&index=" + (index+1);
}

function escapeJSON(str) {
	
	var str = str.replace(/\\n/g, '&#10;');
	//console.log(str);
	
	str1 = str.replace(/[\\\\]/g, '');
	
	var str2 = str1
		.replace(/[']/g, '&#39;')
	  	.replace(/[\\]/g, '\\\\')
		.replace(/[\"]/g, '\\\"')
		.replace(/[\/]/g, '\\/')
		.replace(/[\b]/g, '\\b')
		.replace(/[\f]/g, '\\f')
		.replace(/[\r]/g, '\\r')
		.replace(/[\t]/g, '\\t');
		
	//console.log("escapeJSON:%s\n%s\n%s", str, str1, str2);
	
	return str2;
}

function replaceQuotes(object){
	var index = 0, item;
    for(var prop in object){
		item = object[prop];
		if(typeof item=='object' && item!= null){
			item = replaceQuotes(item);
		}else if (typeof item=='string' && item!=null){
			object[prop] = item.replace(/[\"]/g, '&#34;');
		}
    }
    return object;
}

function saveBlink(){
	var blink = replaceQuotes(blinkData);
	var summary = $('#blink-summary')[0].value;
	var json = JSON.stringify(blink);
	var safe_json = escapeJSON(json);
	$('#overlay').show();
	if (blinkEditInfo.growth != null && blinkEditInfo.growth){
		var blk = course.getGA(blinkEditInfo.index);
	}else{
		var blk = course.get(blinkEditInfo.index);
	}
	console.log("saveBlink:%d, %s", blk.id, safe_json);
	$.post("blink_update.php", { id: blk.id, method:'update', summary:summary, index:blinkEditInfo.index, type:blk.type, courseId:course.id, json: safe_json })
	  .done(function( msg ) {
		  console.log('saveBlink ' + msg);
		if (blinkEditInfo.growth != null && blinkEditInfo.growth){
			var blk = course.getGA(blinkEditInfo.index);
		}else{
			var blk = course.get(blinkEditInfo.index);
		}
		if (blk.id==undefined || blk.id==null){
			var tmp = JSON.parse(msg);
			if (tmp.success) blk.id = tmp.id;
		}
		blinkModified = false;
		updateBlinkPanelButtons();
		$('#overlay').hide();
	  });
}

function showFeedback(msg){
	console.log("showFeedback:" + msg);
	var elm = $("#blink-feedback-msg");
	elm.text(msg);
	$( "#blink-feedback" ).dialog('open');
}

function getIntroBlink(mode){
	burstNotUnit = mode;
	var intro = blinkData.intro;
	if (intro==null){
		intro = { title:"", description:"", image:"" };
		blinkData.intro = intro;
	}
	var title = (intro.title!=null) ? intro.title : "";
	var description = (intro.description!=null) ? intro.description : "";
	var image = (intro.image!=null) ? intro.image : "";
	var str = '<table width="100%">';
    str += '<tr><td class="left" width="20%">Title</td><td><textarea id="intro-title" placeholder="Enter your text here" style="width:100%; height:60px;">' + title + '</textarea></td></tr>';
    str += '<tr><td class="left" width="20%">Description</td><td><textarea id="intro-description" placeholder="Enter your text here" style="width:100%; height:60px;">' + description + '</textarea></td></tr>';
	str += '<form id="intro-image-form" method="post" action="blink_update.php" enctype="multipart/form-data">';
	str += '<tr><td class="left" width="20%">Image</td><td>Choose Image<input type="file" name="image"/>';
	str += '<input type="hidden" name="method" value="intro_image" />';
	str += '<input type="submit" value="Upload" />';
	str += '</form>';
	str += '<br/><img id="intro-image" src="../courses/images/' + image + '" />';
	if (burstNotUnit){
		if (blinkData.unlockscore==null) blinkData.unlockscore = 0;
		if (blinkData.quicklink==null) blinkData.quicklink=false;
		if (blinkData.quotation==null) blinkData.quotation="";
		str += '<tr><td class="left" width="20%">Quotation</td><td><textarea id="intro-quotation" placeholder="Enter the text to use as quotation on progress screen." style="width:100%; height:60px;">' + blinkData.quotation + '</textarea></td></tr>';
		str += '<tr><td class="left" width="20%">Unlock score</td><td><input type="text" id="intro-unlock-score" value="' + blinkData.unlockscore + '" /></td></tr>';
		var checked = (blinkData.quicklink=="undefined" || !blinkData.quicklink) ? "" : "checked"; 
		str += '<tr><td class="left" width="20%">Quick link</td><td><input type="checkbox" id="intro-quicklink" ' + checked + ' /></td></tr>';
	}else{
		if (blinkData.dossier==null) blinkData.dossier = false;
		var checked = (blinkData.dossier=="undefined" || !blinkData.dossier) ? "" : "checked"; 
		str += '<tr><td class="left" width="20%">Create Dossier</td><td><input type="checkbox" id="intro-dossier" ' + checked + ' /></td></tr>';
	}
	str += '</td></tr></table>';
	return str;
}

function showIntroBlink(){
	$('#blink-content').html(getIntroBlink());
	$("blinks").attr("overflow", "hidden");
	setTimeout(setScrollHeight, 10);
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function addIntroEvents(){
	var elm = $("#intro-title");
	
	elm.keyup(function(){
			$("#intro-title").blur();
			$("#intro-title").focus();
		});
		
	elm.change( function(){
			console.log(this.value);
			blinkData.intro.title = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	
	var elm = $("#intro-quotation");
	
	elm.keyup(function(){
			$("#intro-quotation").blur();
			$("#intro-quotation").focus();
		});
		
	elm.change( function(){
			console.log(this.value);
			blinkData.quotation = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
	$("#intro-description").keyup(function(){
			$("#intro-description").blur();
			$("#intro-description").focus();
		});
			
	$('#intro-description').change( function(){
			console.log(this.value);
			blinkData.intro.description = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
	$('#intro-image-form').ajaxForm(function(data) {
			console.log("Intro image form " + data); 
			var json = JSON.parse(data);
			if (json.success){
				var src = "../courses/images/" + json.path;
				blinkData.intro.image = json.path;
				var elm = $('#intro-image');
				if (elm!=null){
					elm.attr("src", src+"?timestamp=" + new Date().getTime());
				}else{
					var str = $('#blink-content').html();
					str += '<img id="intro-image" src="../courses/images/' + json.path + ' />';
					$('#blink-content').html(str);
				}
				blinkModified=true;
				updateBlinkPanelButtons();
			}
		});
		
	if (burstNotUnit){
		$("#intro-feedback-pass").keyup(function(){
			$("#intro-feedback-pass").blur();
			$("#intro-feedback-pass").focus();
		});
			
		$('#intro-feedback-pass').change( function(){
			console.log(this.value);
			blinkData.feedback[0] = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		$("#intro-feedback-fail").keyup(function(){
			$("#intro-feedback-fail").blur();
			$("#intro-feedback-fail").focus();
		});
			
		$('#intro-feedback-fail').change( function(){
			console.log(this.value);
			blinkData.feedback[1] = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		$("#intro-unlock-score").keyup(function(){
			$("#intro-unlock-score").blur();
			$("#intro-unlock-score").focus();
		});
			
		$('#intro-unlock-score').change( function(){
			console.log(this.value);
			blinkData.unlockscore = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		$('#intro-quicklink').click (function (){
			var check = $(this);
			if (check.is (':checked')){
				// Do stuff
				blinkData.quicklink = true;
			}else{
				blinkData.quicklink = false;
			}
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}else{
		$('#intro-dossier').click (function (){
			var check = $(this);
			if (check.is (':checked')){
				// Do stuff
				blinkData.dossier = true;
			}else{
				blinkData.dossier = false;
			}
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}
}

function getMenuBlink(){
	var str = '<table width="100%"><tr><td class="left" width="20%">New</td>';
    str += '<td>';
    str += '<select id="menutype" style="width:100%;">';
    str += '<option>Select menu type</option>';
    str += '<option>Learning units</option>';
	str += '<option>All Bursts</option>';
	str += '<option>Bursts in Learning unit</option>';
    str += '<option>Growing Activities</option>';
    str += '<option>Personal Dossiers</option>';
    str += '</select>';
    str += '<td class="buttons"><a href="#"><img src="images/btn_create.png" title="Create" onclick="createMenu()" /></a></td></tr>';
    str += getMenuHTML();
	str += '</table>';
	return str;
}

function showMenuBlink(){
	$('#blink-content').html(getMenuBlink());
	$("blinks").attr("overflow", "hidden");
	setTimeout(setScrollHeight, 10);
}

function getMenuHTML(){
	var blink = blinkData;
	str = "";
	if (blink.menu!=null){
		for(var i=0; i<blink.menu.length; i++){
			str += '<tr><td class="left">Menu Item ' + (i+1) + '</td><td>' + blink.menu[i].name + '</td><td class="buttons">';
			if (blink.menu[i].dynamic){
				str += '&nbsp;';
			}else{
				str += '<a href="#"><img id="btn_locked' + (i+1) + '" src="images/btn_unlocked.png" onclick="menuItemLocked(' + (i+1) + ')" ></a>'; 
			}
			str += '</td></tr>';
		}
	}
	return str;
}

function menuItemLocked(i){
	console.log('menuItemLocked:' + i);
}

function selectLearningUnitForBurstMenu(){
	var elm = $('#learning-units-selector');
	if (elm.length==0){
		$('<div id="learning-units-selector" title="Select learning unit"/>').appendTo('body');
		var elm = $('#learning-units-selector');
		var str = '<select id="learning-units-select"><option>Select learning unit</option>';
		for(var i=0; i<course.blinks.length; i++){
			if (course.blinks[i].type==1){
				str += ('<option value="' + i + '">' + course.blinks[i].summary + '</option>');
			}
		}
		str += '</select>';
		elm = $('#learning-units-selector');
		elm.html(str);
		elm.dialog({ 
		  height: 240,
		  resizable: false,
		  modal: true,
		  buttons: {
			"OK": function() {
				var firstIdx = parseInt($('#learning-units-select').val());
				var menu = new Array();
				for(var i=firstIdx+1; i<course.blinks.length; i++){
					if (course.blinks[i].type==3){
						menu.push({ name:course.blinks[i].summary, guid:course.blinks[i].guid });
					}
					if (course.blinks[i].type==2)  break;
				}
				blink.menu = menu;
				showMenuBlink();
				$( this ).dialog( "close" );
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		  }
		});
	}else{
		elm.dialog('open');
	}
}

function createMenu(){
	yesNoFunc = createMenuConfirm;
	$('#blink-confirm-yesno').attr("title", "Create Menu");
	$('#blink-confirm-yesno-msg').text("Do you want to make a new menu?");
	$('#blink-confirm-yesno').dialog('open');
}

function createMenuConfirm(){
	blinkModified = true;
	var elm = $('#menutype');
	var type = $('#menutype option:selected').index();
	menu = new Array();
	switch(type){
		case 1://Learning units
		for(var i=0; i<course.blinks.length; i++){
			if (course.blinks[i].type==1){
				menu.push({ name:course.blinks[i].summary, guid:course.blinks[i].guid, dynamic:false });
			}
		}
		break;
		case 2://All bursts
		for(var i=0; i<course.blinks.length; i++){
			if (course.blinks[i].type==3){
				menu.push({ name:course.blinks[i].summary, guid:course.blinks[i].guid, dynamic:false });
			}
		}
		break;
		case 3://Burst in unit
		selectLearningUnitForBurstMenu();
		return;
		break;
		case 4://Growing Activity
		menu.push({name:"Growing Activity", dynamic:true});
		break;
		case 5://Personal dossier
		menu.push({name:"Personal Dossier", dynamic:true});
		break;
	}
	var blink = blinkData;
	blink.menu = menu;
	showMenuBlink();
	updateBlinkPanelButtons();
	yesNoFunc=null;
}

function getPresentationBlink(){
	data = { text:"", in:0, out:0, time:0 };
	var str = '<table width="90%"><tr><td class="left" width="20%">Add Text</td>';
    str += '<td>';
    str += '<textarea id="presentation-text" placeholder="Enter your text here" style="width:100%; height:60px;"></textarea>';
    str += '<td width="25%" class="buttons"><a href="#"><img src="images/btn_create.png" title="Create" onclick="addPresentationText()" /></a></td></tr>';
    str += getPresentationHTML();
	str += '</table>';
	return str;
}

function showPresentationBlink(){
	$('#blink-content').html(getPresentationBlink());
	$("blinks").attr("overflow", "hidden");
	setTimeout(setScrollHeight, 10);
}

function getPresentationHTML(){
	var str = "";
	var blink = blinkData;
	if (blink.presentation!=null){
		for(var i=0; i<blink.presentation.length; i++){
			str += '<tr><td class="left">Text ' + (i+1) + '</td><td><textarea id="presentation-text' + (i+1) + '" onkeyup="presentationTextKeyup(this)" onchange="presentationTextChange(this, ' + i + ')" style="width:100%; height:40px;">' + blink.presentation[i].text + '</textarea></td><td class="buttons">';
			str += '<img class="button" id="presentation_time' + i + '" src="images/btn_time' + blink.presentation[i].time + '.png" >';
			str += '<img class="button" src="images/btn_timer.png" title="Set timer" onclick="presentationTimer(' + i + ')" >';
			str += '<img id="presentation_anim_in' + i + '" class="button" src="images/btn_anim' + blink.presentation[i].in + '.png" title="Set animation in" onclick="presentationAnimation(' + i + ',false)" >';
			//str += '<img id="presentation_anim_out' + i + '" class="button" src="images/btn_anim' + blink.presentation[i].out + '.png" title="Set animation out" onclick="presentationAnimation(' + i + ',true)" >';
			str += '<img class="button" src="images/btn_delete.png" title="Delete text" onclick="presentationDelete(' + i + ')" >';
			str += '</td></tr>';
		}
	}
	return str;
}

function presentationTextKeyup(elm){
	$('#'+elm.id).blur();
    $('#'+elm.id).focus();
}

function presentationTextChange(elm, idx){
	console.log("presentationTextChange index:" + idx + " value:" + elm.value);
	var blink = blinkData;
	blink.presentation[idx].text = elm.value;
	blinkModified = true;
	updateBlinkPanelButtons();
}

function presentationTimer(idx){
	var blink = blinkData;
	blink.presentation[idx].time++;
	if (blink.presentation[idx].time>8) blink.presentation[idx].time = 0;
	$('#presentation_time' + idx).attr('src', 'images/btn_time' + blink.presentation[idx].time + '.png');
	blinkModified = true;
	updateBlinkPanelButtons();
}

function presentationAnimation( idx, out ){
	var blink = blinkData;
	if (out){
		blink.presentation[idx].out++;
		if (blink.presentation[idx].out>5) blink.presentation[idx].out = 0;
		$('#presentation_anim_out' + idx).attr('src', 'images/btn_anim' + blink.presentation[idx].out + '.png');
	}else{
		blink.presentation[idx].in++;
		if (blink.presentation[idx].in>5) blink.presentation[idx].in = 0;
		$('#presentation_anim_in' + idx).attr('src', 'images/btn_anim' + blink.presentation[idx].in + '.png');
	}
	blinkModified = true;
	updateBlinkPanelButtons();
}

function presentationDelete(idx){
	var blink = blinkData;
	blink.presentation.splice(idx, 1);
	blinkModified = true;
	showPresentationBlink();
}

function addPresentationText(){
	data.text = $('#presentation-text')[0].value;
	var blink = blinkData;
	if (blink.presentation==null) blink.presentation = new Array();
	blink.presentation.push(data);
	blinkModified = true;
	updateBlinkPanelButtons();
	showPresentationBlink();
}

function getInputBlink(){
	var blink = blinkData;
	var prompt = (blink.input!=null) ? blink.input.prompt : "";
	var prompt_title = (blink.input!=null) ? 'Prompt' : 'New';
	var str = '<table width="100%"><tr><td class="left" width="20%">' + prompt_title + '</td>';
    str += '<td>';
    str += '<textarea id="blink-input-prompt" placeholder="Enter a prompt here" style="width:100%; height:60px;">' + prompt + '</textarea>';
	if (blink.input==null){
		str += '<select id="blink-input-type">';
		str += '<option>Choose an input type</option>';
		str += '<option>Text</option>';
		str += '<option>Words</option>';
		str += '<option>Number</option>';
		str += '<option>Date</option>';
		str += '<option>True or False</option>';
		str += '<option>Slider</option>';
		str += '<option>Slider with text</option>';
		str += '</select>';
	}
    str += '<td class="buttons" width="10%">';
	//if (blink.input!=null && blink.input.locked){
	//	str += '<a href="#"><img class="button" id="input-locked" src="images/btn_locked.png" title="Toggle Locked" onclick="inputLocked()" ></a>';
	//}else{
	//	str += '<a href="#"><img class="button" id="input-locked" src="images/btn_unlocked.png" title="Toggle Locked" onclick="inputLocked()" ></a>';
	//}
	if (blink.input==null) str += '<a href="#"><img class="button" src="images/btn_create.png" title="Initialize input" onclick="createInput()" /></a></td></tr>';
    str += getInputHTML();
	str += '</table>';
	return str;
}

function showInputBlink(){
	$('#blink-content').html(getInputBlink());
	$("blinks").attr("overflow", "hidden");
	setTimeout(setScrollHeight, 10);
}

function addInputEvents(blink){	
	switch(blink.input.type){ 
		case 2://Word
		$('#input-word-count').spinner({ min:2, max:20 });
		$('#input-word-count').spinner("value", blink.input.count);
		$("#input-word-count").width(30);
		$('#input-word-count').on( "spinchange", function( event, ui ) { 
			blink.input.count = $('#input-word-count').spinner("value");
			blinkModified=true;
			updateBlinkPanelButtons(); 
		} );
		$('#input-word-characters').spinner({ min:4, max:30 });
		$('#input-word-characters').spinner("value", blink.input.characters);
		$("#input-word-characters").width(30);
		$('#input-word-characters').on( "spinchange", function( event, ui ) { 
			blink.input.characters = $('#input-word-characters').spinner("value");
			blinkModified=true;
			updateBlinkPanelButtons(); 
		} );
		break;
		case 3://Number
		$("#input-number-text").keyup(function(){
			$("#input-number-text").blur();
			$("#input-number-text").focus();
		});
	
		$('#input-number-text').change( function(){
			//console.log("blink prompt changed " + this.value);
			blink.input.text = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		for(var i=0; i<blink.input.numbers.length; i++){
			var elm = $('#input-number-value' + (i+1));
			elm.spinner();
			elm.on( "spinchange", function( event, ui ) { 
				var idx = parseInt(event.target.id.substr(18));
				var elm = $('#' + event.target.id);
				blink.input.numbers[idx-1].value = elm.spinner("value");
				console.log("value changed");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			elm.spinner("value", blink.input.numbers[i].value);
			elm.width(60);
			
			var elm = $('#input-number-min' + (i+1));
			elm.spinner();
			elm.on( "spinchange", function( event, ui ) { 
				var idx = parseInt(event.target.id.substr(16));
				var elm = $('#' + event.target.id);
				blink.input.numbers[idx-1].min = elm.spinner("value");
				console.log("min changed");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			elm.spinner("value", blink.input.numbers[i].min);
			elm.width(60);
			
			var elm = $('#input-number-max' + (i+1));
			elm.spinner();
			elm.on( "spinchange", function( event, ui ) { 
				var idx = parseInt(event.target.id.substr(16));
				var elm = $('#' + event.target.id);
				blink.input.numbers[idx-1].max = elm.spinner("value");
				console.log("max changed");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			elm.spinner("value", blink.input.numbers[i].max);
			elm.width(60);
		}
		
		break;
		case 4://Date
		$("#input-date-text").keyup(function(){
			$("#input-date-text").blur();
			$("#input-date-text").focus();
		});
	
		$('#input-date-text').change( function(){
			//console.log("blink prompt changed " + this.value);
			blink.input.text = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		for(var i=0; i<blink.input.dates.length; i++){
			var elm = $('#input-date-value' + (i+1));
			elm.datepicker();
			elm.change(function( ) { 
				var idx = parseInt(this.id.substr(16));
				blink.input.dates[idx-1].date = this.value;
				console.log("value changed");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			if (blink.input.dates[i].date!=null) elm.datepicker("setDate", blink.input.dates[i].date);
			
			var elm = $('#input-date-min' + (i+1));
			elm.datepicker();
			elm.change(function( ) { 
				var idx = parseInt(this.id.substr(14));
				blink.input.dates[idx-1].min = this.value;
				console.log("value changed");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			if (blink.input.dates[i].min!=null) elm.datepicker("setDate", blink.input.dates[i].min);
			
			var elm = $('#input-date-max' + (i+1));
			elm.datepicker();
			elm.change(function( event) { 
				var idx = parseInt(event.target.id.substr(14));
				blink.input.dates[idx-1].max = this.value;
				console.log("value changed");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			if (blink.input.dates[i].max!=null) elm.datepicker("setDate", blink.input.dates[i].max);
		}
		
		break;
		case 5://Boolean
		$("#input-boolean-text").keyup(function(){
			$("#input-boolean-text").blur();
			$("#input-boolean-text").focus();
		});
	
		$('#input-boolean-text').change( function(){
			console.log("blink text changed " + this.value);
			blink.input.text = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		for(var i=0; i<blink.input.booleans.length; i++){
			var elm = $('#input-boolean-value' + (i+1));
			elm.change(function( ) { 
				var idx = parseInt(this.id.substr(19));
				var selectedIdx = $('#' + this.id).prop('selectedIndex');
				blink.input.booleans[idx-1].value = (selectedIdx==0);
				console.log("value changed " + selectedIdx);
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			
			elm = $('#input-boolean-true' + (i+1));	
			elm.keyup(function(){
				console.log("Keyup " + this.id);
				$("#" + this.id).blur();
				$("#" + this.id).focus();
			});
		
			elm.change( function(){
				console.log("blink boolean true changed " + this.value);
				var idx = parseInt(this.id.substr(18));
				blink.input.booleans[idx-1].correct = this.value;
				blinkModified=true;
				updateBlinkPanelButtons(); 
			});
			
			elm = $('#input-boolean-false' + (i+1));	
			elm.keyup(function(){
				$("#" + this.id).blur();
				$("#" + this.id).focus();
			});
		
			elm.change( function(){
				console.log("blink boolean false changed " + this.value);
				var idx = parseInt(this.id.substr(19));
				blink.input.booleans[idx-1].wrong = this.value;
				blinkModified=true;
				updateBlinkPanelButtons(); 
			});
		}
		
		break;
		case 7:
		elm = $('#input-text');	
		elm.keyup(function(){
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink input text changed " + this.value);
			var idx = parseInt(this.id.substr(10));
			blink.input.text = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		case 6://Slider
		$('#input-min').spinner({ min:0, max:1000 });
		$('#input-min').spinner("value", blink.input.min);
		$("#input-min").width(60);
		$('#input-min').on( "spinchange", function( event, ui ) { 
			blink.input.min = $('#input-min').spinner("value");
			blinkModified=true;
			updateBlinkPanelButtons(); 
		} );
		$('#input-max').spinner({ min:0, max:1000 });
		$('#input-max').spinner("value", blink.input.max);
		$("#input-max").width(60);
		$('#input-max').on( "spinchange", function( event, ui ) { 
			blink.input.max = $('#input-max').spinner("value");
			blinkModified=true;
			updateBlinkPanelButtons(); 
		} );
		$('#input-ideal').spinner({ min:0, max:1000 });
		$('#input-ideal').spinner("value", blink.input.ideal);
		$("#input-ideal").width(60);
		$('#input-ideal').on( "spinchange", function( event, ui ) { 
			blink.input.ideal = $('#input-ideal').spinner("value");
			blinkModified=true;
			updateBlinkPanelButtons(); 
		} );
		break;
	}
	
	$("#blink-input-prompt").keyup(function(){
		$("#blink-input-prompt").blur();
		$("#blink-input-prompt").focus();
	});

	$('#blink-input-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.input.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
}
		
function getInputHTML(){
	var blink = blinkData;
	var str = "";
	if (blink.input!=null){
		switch(blink.input.type){
			case 1://Text
			//Nothing to do
			break;
			case 2://Words
			if (blink.input.time==null) blink.input.time = 0;
			if (blink.input.in==null) blink.input.in = 0;
			if (blink.input.out==null) blink.input.out = 0;
			str += '<tr><td class="left">Word</td><td>Number of words <input id="input-word-count">&nbsp;&nbsp; Character limit <input id="input-word-characters"></td><td class="buttons">';
			str += '<img class="button" id="word_time" src="images/btn_time' + blink.input.time + '.png" >';
			str += '<img class="button" src="images/btn_timer.png" title="Set timer" onclick="inputWordTimer()" >';
			str += '<img id="word_anim_in" class="button" src="images/btn_anim' + blink.input.in + '.png" title="Set animation in" onclick="inputWordAnimation(false)" >';
			str += '<img id="word_anim_out" class="button" src="images/btn_anim' + blink.input.out + '.png" title="Set animation out" onclick="inputWordAnimation(true)" >';
			str += '</td></tr>';
			break;
			case 3://Number
			if (blink.input.text==null) blink.input.text = "";
			if (blink.input.numbers==null) blink.input.numbers = new Array();
			str += '<tr><td class="left">Text</td><td><textarea id="input-number-text" width="90%" placeholder="Enter the text to use for the input for each number use ##x## where x is the number in the list below.">' + blink.input.text + '</textarea></td><td class="buttons">';
			str += '<img class="button" id="word_time" src="images/btn_plus.png" title="Add number" onclick="inputAddNumber()"></td></tr>';
			for(var i=0; i<blink.input.numbers.length; i++){
				str += '<tr><td class="left">Number ' + (i+1) + '</td>';
				str += '<td>Value <input id="input-number-value' + (i+1) + '"> Min <input id="input-number-min' + (i+1) + '"> Max <input id="input-number-max' + (i+1) + '"></td>';
				str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this number" onclick="inputDeleteNumber(' + i + ')" ></td></tr>';
			}
			break;
			case 4://Date
			if (blink.input.text==null) blink.input.text = "";
			if (blink.input.dates==null) blink.input.dates = new Array();
			str += '<tr><td class="left">Text</td><td><textarea id="input-date-text" width="90%" placeholder="Enter the text to use for the input for each date use ##x## where x is the date in the list below.">' + blink.input.text + '</textarea></td><td class="buttons">';
			str += '<img class="button" id="word_time" src="images/btn_plus.png" title="Add date" onclick="inputAddDate()"></td></tr>';
			for(var i=0; i<blink.input.dates.length; i++){
				str += '<tr><td class="left">Date ' + (i+1) + '</td>';
				str += '<td><input id="input-date-value' + (i+1) + '"> Min <input id="input-date-min' + (i+1) + '"> Max <input id="input-date-max' + (i+1) + '"></td>';
				str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this date" onclick="inputDeleteDate(' + i + ')" ></td></tr>';
			}
			break;
			case 5://True or False
			if (blink.input.text==null) blink.input.text = "";
			if (blink.input.booleans==null || blink.input.booleans.length==0){
				blink.input.booleans = new Array({value:true, correct:"True", wrong:"False"});
			}
			//str += '<tr><td class="left">Text</td><td><textarea id="input-boolean-text" width="90%" placeholder="Enter the text to use for the input for this boolean.">' + blink.input.text + '</textarea></td><td class="buttons">';
			//str += '<img class="button" id="word_time" src="images/btn_plus.png" title="Add Boolean" onclick="inputAddBoolean()"></td></tr>';
			for(var i=0; i<blink.input.booleans.length; i++){
				str += '<tr><td class="left">Boolean</td>';
				str += '<td>Correct button<select id="input-boolean-value' + (i+1) + '">';
				if (blink.input.booleans[i].value){
					str += '<option selected>True</option>';
					str += '<option>False</option>';
				}else{
					str += '<option>True</option>';
					str += '<option selected>False</option>';
				} 
				str += '</select>';
				str += ' True display word<input id="input-boolean-true' + (i+1) + '" type="text" value="' + blink.input.booleans[i].correct + '">';
				str += ' False display word<input id="input-boolean-false' + (i+1) + '" type="text" value="' + blink.input.booleans[i].wrong + '">';
				str += '<td class="buttons">&nbsp;</td></tr>';
				str += '</tr>';
			}
			break;
			case 7:
			if (blink.input.text==null) blink.input.text = "";
			str += '<tr><td class="left">Text</td><td><input id="input-text" type="text"></td><td class="button"></td></tr>';
			case 6://Slider
			if (blink.input.min==null) blink.input.min = 0;
			if (blink.input.max==null) blink.input.max = 0;
			if (blink.input.ideal==null) blink.input.ideal = 0;
			str += '<tr><td class="left">Slider</td><td>';
			str += '&nbsp;Min&nbsp;<input id="input-min">';
			str += '&nbsp;Max&nbsp;<input id="input-max">';
			str += '&nbsp;Ideal&nbsp;<input id="input-ideal"></td>';
			str += '<td class="buttons">&nbsp;</td></tr>';
			break;
		}
	}
	return str;
}

function inputAddBoolean(){
	var blink = blinkData;
	blink.input.booleans.push( { value:true, correct:"True", wrong:"False" });
	showInputBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputDeleteBoolean(idx){
	var blink = blinkData;
	blink.input.booleans.splice(idx, 1);
	showInputBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputAddNumber(){
	var blink = blinkData;
	blink.input.numbers.push( { value:0 });
	showInputBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputDeleteNumber(idx){
	var blink = blinkData;
	blink.input.numbers.splice(idx, 1);
	showInputBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputAddDate(){
	var blink = blinkData;
	blink.input.dates.push( {  });
	showInputBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputDeleteDate(idx){
	var blink = blinkData;
	blink.input.dates.splice(idx, 1);
	showInputBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputWordTimer(){
	var blink = blinkData;
	blink.input.time++;
	if (blink.input.time>8) blink.input.time = 0;
	$('#word_time').attr('src', 'images/btn_time' + blink.input.time + '.png');
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputWordAnimation(anim_out){
	var blink = blinkData;
	if (anim_out){
		blink.input.out++;
		if (blink.input.out>5) blink.input.out = 0;
		$('#word_anim_out').attr('src', 'images/btn_anim' + blink.input.out + '.png');
	}else{
		blink.input.in++;
		if (blink.input.in>5) blink.presentation[idx].in = 0;
		$('#word_anim_in').attr('src', 'images/btn_anim' + blink.input.in + '.png');
	}
	blinkModified = true;
	updateBlinkPanelButtons();
}

function createInput(){
	if (blinkEditInfo!=null){
		console.log(blinkEditInfo.growth);
		if (blinkEditInfo.growth){
			var tmp = course.getGA(blinkEditInfo.index);
			try{
				blinkData = JSON.parse(tmp.json);
			}catch(e){
				console.log("createInput: Problem parsing json");
			}
		}
	}
	var blink = blinkData;
	var type = $('#blink-input-type option:selected').index();
	console.log("input type selected is " + type);
	if (type<1){
		showFeedback("Please select an input type");
	}else{
		var prompt = $('#blink-input-prompt')[0].value;
		blink.input = { type:type, prompt:prompt, locked:false };
		if (type==2){
			blink.input.in = 0;
			blink.input.out = 0;
			blink.input.time = 0;
			blink.input.count = 6;
			blink.input.characters = 12;
		}
		blinkModified = true;
		updateBlinkPanelButtons();
		showInputBlink();
		addInputEvents(blink);
		blinkData = blink;
	}
}

function inputLocked(){
	var blink = blinkData;
	blink.input.locked = !blink.input.locked;
	if (blink.input.locked){
		$('#input-locked').attr('src', 'images/btn_locked.png');
	}else{
		$('#input-locked').attr('src', 'images/btn_unlocked.png');
	}
}

function showStatementBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getStatementBlink());
	setTimeout(setScrollHeight, 10);
}

function addStatementEvents(blink){
	var elm = $('#statement-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.statement.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.statement.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.statement.timer);
	elm.width(60);
	
	$("#statement-prompt").keyup(function(){
		$("#statement-prompt").blur();
		$("#statement-prompt").focus();
	});

	$('#statement-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.statement.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	elm = $('#statement-wrong');	
	elm.keyup(function(){
		//console.log("Keyup " + this.id);
		$("#" + this.id).blur();
		$("#" + this.id).focus();
	});

	elm.change( function(){
		console.log("blink statement wrong changed " + this.value);
		var idx = parseInt(this.id.substr(9));
		blink.statement.wrong = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	elm = $('#statement-ordered');	
	elm.change( function(){
		console.log("blink statement ordered changed " + this.checked);
		blink.statement.ordered = (this.checked);
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
		
	for(var i=0; i<blink.statement.statements.length; i++){
		elm = $('#statement' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink statement changed " + this.value);
			var idx = parseInt(this.id.substr(9));
			blink.statement.statements[idx-1] = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}
	
	addFeedbackEvents();
}

function getStatementBlink(){
	var blink = blinkData;
	if (blink.statement==null){
		blink.statement = new Object();
		blink.statement.prompt = "";
		blink.statement.wrong = "";
		blink.statement.statements = new Array();
		blink.statement.ordered = true;
		blink.statement.timer = 0;
	}
	var str = '<table width="100%"><tr><td class="left" width="20%">Prompt</td>';
    str += '<td>';
    str += '<textarea id="statement-prompt" placeholder="Enter text prompt. For each statement use | to indicate block break." style="width:100%; height:60px;">' + blink.statement.prompt + '</textarea>';
    str += '<td class="buttons">';
	var ordered = (blink.statement.ordered) ? "checked" : "";
	str += '<a href="#"><img class="button" src="images/btn_plus.png" title="Add Statement" onclick="statementAddStatement()" /></a></td></tr>';
	str += '<tr><td class="left" width="20%">Statement</td><td>Ordered<input id="statement-ordered" type="checkbox" ' + ordered + '> Time (in secs, 0 for not timed)&nbsp;<input id="statement-timer"></td><td class="buttons"></td></tr>';
	str += '<tr><td class="left">Wrong</td><td><input id="statement-wrong" value="' + blink.statement.wrong + '" style="width:100%"></td><td class="buttons"></td></tr>';
	if (blink.statement!=null && blink.statement.statements!=null){
		for(var i=0; i<blink.statement.statements.length; i++){
			str += '<tr><td class="left">Statement ' + (i+1) + '</td>';
			str += '<td><input id="statement' + (i+1) + '" value="' + blink.statement.statements[i] + '" style="width:100%"></td>';
			str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this statement" onclick="statementDeleteStatement(' + i + ')" ></td></tr>';
		}
	}
	
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function statementAddStatement(){
	var blink = blinkData;
	blink.statement.statements.push( "");
	showStatementBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function statementDeleteStatement(idx){
	var blink = blinkData;
	blink.statement.statements.splice(idx, 1);
	showStatementBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function showWordfillBlink(){
	$('#blink-content').html(getWordfillBlink());
	$("blinks").attr("overflow", "hidden");
	setTimeout(setScrollHeight, 10);
}

function getWordfillBlink(){
	var blink = blinkData;
	var text = (blink.wordfill!=null) ? blink.wordfill.text : "";
	var prompt = (blink.wordfill!=null && blink.wordfill.prompt!=null) ? blink.wordfill.prompt : "";
	var str = '<table width="100%">';
	str += '<tr><td class="left" width="20%">WordFill</td><td>Time (in secs, 0 for not timed)&nbsp;<input id="wordfill-timer"></td><td class="buttons"></td></tr>';
	str += '<tr><td class="left">Prompt</td><td><textarea id="wordfill-prompt" placeholder="Enter a prompt here" style="width:100%; height:60px;">' + prompt + '</textarea></td><td class="buttons"></td></tr>';
	str += '<tr><td class="left">Text</td>';
    str += '<td><textarea id="wordfill-text" placeholder="Enter text here. Add ##Answer## where you want a space for an answer." style="width:100%; height:60px;">' + text + '</textarea>';
    str += '<td class="buttons">';
	if (blink.input!=null && blink.input.locked){
		str += '<a href="#"><img class="button" id="input-locked" src="images/btn_locked.png" title="Toggle Locked" onclick="inputLocked()" ></a>';
	}else{
		str += '<a href="#"><img class="button" id="input-locked" src="images/btn_unlocked.png" title="Toggle Locked" onclick="inputLocked()" ></a>';
	}
	str += '<a href="#"><img class="button" src="images/btn_plus.png" title="Add block" onclick="wordfillAddWord()" /></a></td></tr>';
	if (blink.wordfill!=null && blink.wordfill.words!=null){
		for(var i=0; i<blink.wordfill.words.length; i++){
			str += '<tr><td class="left">Word ' + (i+1) + '</td>';
			str += '<td><input id="wordfill-word' + (i+1) + '" value="' + blink.wordfill.words[i] + '"></td>';
			str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this word" onclick="wordfillDeleteWord(' + i + ')" ></td></tr>';
		}
	}
	
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function addWordfillEvents(blink){
	var elm = $('#wordfill-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.wordfill.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.wordfill.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.wordfill.timer);
	elm.width(60);
	
	$("#wordfill-prompt").keyup(function(){
		$("#wordfill-prompt").blur();
		$("#wordfill-prompt").focus();
	});

	$('#wordfill-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.wordfill.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	$("#wordfill-text").keyup(function(){
		$("#wordfill-text").blur();
		$("#wordfill-text").focus();
	});

	$('#wordfill-text').change( function(){
		console.log("blink text changed " + this.value);
		blink.wordfill.text = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	for(var i=0; i<blink.wordfill.words.length; i++){
		elm = $('#wordfill-word' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink wordfill changed " + this.value);
			var idx = parseInt(this.id.substr(13));
			blink.wordfill.words[idx-1] = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}
	
	addFeedbackEvents();
}

function wordfillAddWord(){
	var blink = blinkData;
	if (blink.wordfill==null) blink.wordfill = new Object();
	if (blink.wordfill.prompt==null) blink.wordfill.prompt = "";
	if (blink.wordfill.words==null) blink.wordfill.words = new Array();
	if (blink.wordfill.text==null) blink.wordfill.text = "";
	if (blink.wordfill.timer==null) blink.wordfill.timer = 60;
	blink.wordfill.words.push("");
	showWordfillBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function wordfillDeleteWord(idx){
	var blink = blinkData;
	blink.wordfill.words.splice(idx, 1);
	showWordfillBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function showDragdropBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getDragdropBlink());
	setTimeout(setScrollHeight, 10);
}

function addDragdropEvents(blink){
	var elm = $('#dragdrop-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.dragdrop.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.dragdrop.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.dragdrop.timer);
	elm.width(60);
	
	elm = $('#dragdrop-prompt');
	elm.keyup(function(){
		var elm = $('#' + event.target.id);
		elm.blur();
		elm.focus();
	});
	
	elm.change(function(){
		blink.dragdrop.prompt = this.value;
		console.log("Prompt changed " + this.value);
		blinkModified = true;
		updateBlinkPanelButtons();
	});
		
	for(var i=0; i<blink.dragdrop.goals.length; i++){
		/*var elm = $('#dragdrop-goal-left' + (i+1));
		elm.spinner({min:0});
		elm.on( "spinchange", function( event, ui ) { 
			var idx = parseInt(this.id.substr(18));
			var elm = $('#' + event.target.id);
			blink.dragdrop.goals[idx-1].left = elm.spinner("value");
			console.log("value changed");
			blinkModified=true;
			updateBlinkPanelButtons(); 
		} );
		elm.spinner("value", blink.dragdrop.goals[i].left);
		elm.width(60);
		
		elm = $('#dragdrop-goal-top' + (i+1));
		elm.spinner({min:0});
		elm.on( "spinchange", function( event, ui ) { 
			var idx = parseInt(this.id.substr(17));
			var elm = $('#' + event.target.id);
			blink.dragdrop.goals[idx-1].top = elm.spinner("value");
			console.log("value changed");
			blinkModified=true;
			updateBlinkPanelButtons(); 
		} );
		elm.spinner("value", blink.dragdrop.goals[i].top);
		elm.width(60);*/
		
		elm = $('#dragdrop-goal-form' + (i+1));
		elm.ajaxForm(function(data) {
			console.log("goal form " + data); 
			var json = JSON.parse(data);
			if (json.success){
				var src = "../courses/images/" + json.path;
				blinkData.dragdrop.goals[json.index].image = json.path;
				$('#dragdrop-goal-image' + (json.index+1)).attr("src", src+"?timestamp=" + new Date().getTime());
			}
		});
		
		elm = $('#dragdrop-goal-text' + (i+1));
		elm.keyup(function(){
			var elm = $('#' + event.target.id);
			elm.blur();
			elm.focus();
		});
		
		elm.change(function(){
			var idx = parseInt(this.id.substr(18));
			blink.dragdrop.goals[idx-1].text = this.value;
			console.log("Item text changed " + this.value);
			blinkModified = true;
			updateBlinkPanelButtons();
		});
	}
	
	for(var i=0; i<blink.dragdrop.items.length; i++){
		/*var elm = $('#dragdrop-item-left' + (i+1));
		elm.spinner({min:0});
		elm.on( "spinchange", function( event, ui ) { 
			var idx = parseInt(this.id.substr(18));
			var elm = $('#' + event.target.id);
			blink.dragdrop.items[idx-1].left = elm.spinner("value");
			console.log("value changed");
			blinkModified=true;
			updateBlinkPanelButtons(); 
		} );
		elm.spinner("value", blink.dragdrop.items[i].left);
		elm.width(60);
		
		var elm = $('#dragdrop-item-top' + (i+1));
		elm.spinner({min:0});
		elm.on( "spinchange", function( event, ui ) { 
			var idx = parseInt(this.id.substr(17));
			var elm = $('#' + event.target.id);
			blink.dragdrop.items[idx-1].top = elm.spinner("value");
			console.log("value changed");
			blinkModified=true;
			updateBlinkPanelButtons(); 
		} );
		elm.spinner("value", blink.dragdrop.items[i].top);
		elm.width(60);*/
		elm = $('#dragdrop-goal-select' + (i+1));
		elm.change(function(){
			var idx = parseInt(this.id.substr(20));
			var index = $('#' + this.id + ' option:selected').index();
			blink.dragdrop.items[idx-1].goal = index;
			console.log("Item goal changed " + index);
			blinkModified = true;
			updateBlinkPanelButtons();
		});
		
		elm = $('#dragdrop-item-form' + (i+1));
		elm.ajaxForm(function(data) {
			console.log("goal form " + data); 
			var json = JSON.parse(data);
			if (json.success){
				var src = "../courses/images/" + json.path;
				blinkData.dragdrop.items[json.index].image = json.path;
				$('#dragdrop-item-image' + (json.index+1)).attr("src", src+"?timestamp=" + new Date().getTime());
			}
		});
		
		elm = $('#dragdrop-item-text' + (i+1));
		elm.keyup(function(){
			var elm = $('#' + event.target.id);
			elm.blur();
			elm.focus();
		});
		
		elm.change(function(){
			var idx = parseInt(this.id.substr(18));
			blink.dragdrop.items[idx-1].text = this.value;
			console.log("Goal text changed " + this.value);
			blinkModified = true;
			updateBlinkPanelButtons();
		});
	}
	
	addFeedbackEvents();
}

function getDragdropBlink(){
	var blink = blinkData;
	if (blink.dragdrop==null){
		blink.dragdrop = new Object();
		blink.dragdrop.timer = 60;
		blink.dragdrop.layout = 0;
		blink.dragdrop.goals = new Array();
		blink.dragdrop.items = new Array();
		blink.dragdrop.prompt = "Drag each item to the correct learning goal";
	}else if (blink.dragdrop.prompt == null){
		blink.dragdrop.prompt = "Drag each item to the correct learning goal";
	}
	
	str = '<table width="100%" style="background-color:#FFF;">';
	str += '<tr>';
	str += '<tr><td class="left" width="20%">Prompt</td><td><textarea id="dragdrop-prompt" placeholder="Enter your text here" style="width:100%; height:60px;">' + blink.dragdrop.prompt + '</textarea></td></tr>';
	str += '<td class="left" width="20%">Drag and Drop</td>';
    str += '<td>Time (in secs, 0 for not timed)&nbsp;<input id="dragdrop-timer"></td>';
    str += '<td class="buttons" width="20%">'
    str += '	Goal<img src="images/btn_plus.png" title="Add Goal" onclick="dragdropAddGoal()" class="button"/>';
    str += '	Item<img src="images/btn_plus.png" title="Add Item" onclick="dragdropAddItem()" class="button"/>';
    str += '</td></tr>';
	
	for (var i=0; i<blink.dragdrop.goals.length; i++){
		str += '<tr><td class="left">Goal ' + (i+1) + '</td>';
    	//str += '<td>Left<input id="dragdrop-goal-left' + (i+1) + '">&nbsp;Top<input id="dragdrop-goal-top' + (i+1) + '">&nbsp;';
		var checked = (blink.dragdrop.goals[i].isImage) ? "checked" : "";
    	str += '<td>Image<input type="checkbox" id="dragdrop-goal-imagechk' + (i+1) + '"' + checked + ' onclick="dragdropImageChk(this, true,' + (i+1) + ')"/>&nbsp;';
		str += 'Text <input type="text" id="dragdrop-goal-text' + (i+1) + '" value="' + blink.dragdrop.goals[i].text + '" style="width:300px;"/>&nbsp;';
		str += '<div id="dragdrop-goal-data' + (i+1) + '">';
		if (blink.dragdrop.goals[i].isImage){
			str += '<form id="dragdrop-goal-form' + (i+1) + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
			str += '<input type="hidden" name="method" value="drag_image">';
			str += '<input type="hidden" name="goal" value="true">';
			str += '<input type="hidden" name="array" value="' + i + '">';
			str += '<br/>Choose Image<input type="file" id="image"  />';
			str += '<input type="submit" value="Upload" />';
			str += '</form>';
    		str += '<img id="dragdrop-goal-image' + (i+1) + '" src="../courses/images/' + blink.dragdrop.goals[i].image + '"  />';
		}
    	str += '</div></td><td class="buttons"><img src="images/btn_delete.png" title="Delete Goal" onclick="dragdropDeleteGoal(' + i + ')" class="button"/></td></tr>';
	}
    
	for (var i=0; i<blink.dragdrop.items.length; i++){
		str += '<tr><td class="left">Item ' + (i+1) + '</td>';
    	//str += '<td>Left<input id="dragdrop-item-left' + (i+1) + '">&nbsp;Top<input id="dragdrop-item-top' + (i+1) + '">&nbsp;';
		str += '<td>Goal<select id="dragdrop-goal-select' + (i+1) + '">';
		for(var j=0; j<=blink.dragdrop.goals.length; j++){
			var selected = (blink.dragdrop.items[i].goal==j) ? " selected" : "";
			if (j==0){
	    		str += '<option' + selected + '>None</option>';
			}else{
    			str += '<option' + selected + '>Goal ' + j + '</option>';
			}
		}
    	str += '</select>';
		var checked = (blink.dragdrop.items[i].isImage) ? "checked" : "";
    	str += 'Image<input type="checkbox" id="dragdrop-item-imagechk' + (i+1) + '"' + checked + ' onclick="dragdropImageChk(this, false,' + (i+1) + ')"/>&nbsp;';
		str += 'Text <input type="text" id="dragdrop-item-text' + (i+1) + '" value="' + blink.dragdrop.items[i].text + '" style="width:300px;"/>&nbsp;';
		str += '<div id="dragdrop-item-data' + (i+1) + '">';
		if (blink.dragdrop.items[i].isImage){
			str += '<form id="dragdrop-item-form' + (i+1) + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
			str += '<input type="hidden" name="method" value="drag_image">';
			str += '<input type="hidden" name="goal" value="false">';
			str += '<input type="hidden" name="index" value="' + i + '">';
			str += '</br>Choose Image<input type="file" id="dragdrop-item-file' + (i+1) + '"  />';
			str += '<input type="submit" value="Upload" />';
			str += '</form>';
    		str += '<img id="dragdrop-item-image' + (i+1) + '" src="../courses/images/' + blink.dragdrop.items[i].image + '"  />';
		}
    	str += '</div></td><td class="buttons"><img src="images/btn_delete.png" title="Delete Item" onclick="dragdropDeleteItem(' + i + ')" class="button"/></td></tr>';
	}
	
	str += getFeedbackRows();
	str += '</table>';

	return str;
}

function dragdropImageChk(chk, goal, idx){
	blinkModified = true;
	var blink = blinkData;
	var elm = (goal) ? $('#dragdrop-goal-data' + idx) : $('#dragdrop-item-data' + idx);
	var str = "";
	if (goal){
		blink.dragdrop.goals[idx-1].isImage = chk.checked;
		//str += 'Text <input type="text" id="dragdrop-goal-text' + idx + '" value="' + blink.dragdrop.goals[idx-1].text + '" style="width:300px;"/>&nbsp;';
		if (blink.dragdrop.goals[idx-1].isImage){
			str += '<form id="dragdrop-goal-form' + idx + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
			str += '<input type="hidden" name="method" value="drag_image">';
			str += '<input type="hidden" name="goal" value="true">';
			str += '<input type="hidden" name="index" value="' + (idx-1) + '">';
			str += '<br/>Choose Image<input type="file" name="image"/>';
			str += '<input type="submit" value="Upload" />';
			str += '</form>';
    		str += '<img id="dragdrop-goal-image' + idx + '" src="../courses/images/' + blink.dragdrop.goals[idx-1].image + '"  />';
		}
	}else{
		blink.dragdrop.items[idx-1].isImage = chk.checked;
		//str += 'Text <input type="text" id="dragdrop-item-text' + idx + '" value="' + blink.dragdrop.items[idx-1].text + '" style="width:300px;"/>&nbsp;';
		if (blink.dragdrop.items[idx-1].isImage){
			str += '<form id="dragdrop-item-form' + idx + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
			str += '<input type="hidden" name="method" value="drag_image">';
			str += '<input type="hidden" name="goal" value="false">';
			str += '<input type="hidden" name="index" value="' + (idx-1) + '">';
			str += '</br>Choose Image<input type="file" name="image"/>';
			str += '<input type="submit" value="Upload"/>';
			str += '</form>';
    		str += '<img id="dragdrop-item-image' + idx + '" src="../courses/images/' + blink.dragdrop.items[idx-1].image + '"  />';
		}	
	}
	elm.html(str);
	updateBlinkButtons();
	addBlinkPanelEvents();
}

function dragdropAddGoal(){
	var blink = blinkData;
	blink.dragdrop.goals.push( { isImage:false, text:"", image:"" });
	showDragdropBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function dragdropDeleteGoal(idx){
	var blink = blinkData;
	blink.dragdrop.goals.splice(idx, 1);
	showDragdropBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function dragdropAddItem(){
	var blink = blinkData;
	blink.dragdrop.items.push( { goal:0, isImage:false, text:"", image:"" });
	showDragdropBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function dragdropDeleteItem(idx){
	var blink = blinkData;
	blink.dragdrop.items.splice(idx, 1);
	showDragdropBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function showQuestionBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getQuestionBlink());
	setTimeout(setScrollHeight, 10);
}

function questionUpdateTimer(){
	var elm = $('#question-timer');
	blink.question.timer = elm.spinner("value");
}

function addQuestionEvents(blink){
	var elm = $('#question-text');
	elm.keyup(function(){
		var elm = $('#' + event.target.id);
		elm.blur();
		elm.focus();
	});
	
	elm.change(function(){
		blink.question.text = this.value;
		console.log("Question text changed " + this.value);
		blinkModified = true;
		updateBlinkPanelButtons();
	});
	
	var elm = $('#question-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.question.timer = $(this).spinner("value");
		blinkModified=true;
		updateBlinkPanelButtons(); 
		console.log("Spinner change to " + blink.question.timer);
    }});
	elm.spinner("value", blink.question.timer);
	elm.width(60);
		
	for(var i=0; i<blink.question.answers.length; i++){
		var elm = $('#question-answer-text' + (i+1));
		elm.keyup(function(){
			var elm = $('#' + event.target.id);
			elm.blur();
			elm.focus();
		});
		
		elm.change(function(){
			var idx = parseInt(this.id.substr(20));
			blink.question.answers[idx-1].text = this.value;
			console.log("Answer text changed " + this.value);
			blinkModified = true;
			updateBlinkPanelButtons();
		});
	}
	
	addFeedbackEvents();
}

function getQuestionBlink(){
	var blink = blinkData;
	if (blink.question==null){
		blink.question = new Object();
		blink.question.timer = 60;
		blink.question.multiplechoice = true;
		blink.question.answers = new Array();
	}
	
	var checked = (blink.question.multiplechoice) ? "checked" : "";
	str = '<table width="100%" style="background-color:#FFF;">';
	str += '<tr>';
	str += '<td class="left" width="20%">Question</td>';
    str += '<td><textarea id="question-text" placeholder="Enter your text here" style="width:100%; height:60px;">' + blink.question.text + '</textarea><br/>';
	str += 'Time (in secs, 0 for not timed)&nbsp;<input id="question-timer">';
	str += '&nbsp;Multiple Choice<input type="checkbox" id="question-typechk" ' + checked + ' onclick="questionTypeChk(this)"/></td>';
    str += '<td class="buttons" width="20%">'
    str += '	<img src="images/btn_plus.png" title="Add Answer" onclick="questionAddAnswer()" class="button"/>';
    str += '</td></tr>';
	
	for (var i=0; i<blink.question.answers.length; i++){
		str += '<tr><td class="left">Answer ' + (i+1) + '</td>';
    	//str += '<td>Left<input id="dragdrop-goal-left' + (i+1) + '">&nbsp;Top<input id="dragdrop-goal-top' + (i+1) + '">&nbsp;';
		var checked = (blink.question.answers[i].correct) ? "checked" : "";
		str += '<td>Text <input type="text" id="question-answer-text' + (i+1) + '" value="' + blink.question.answers[i].text + '" placeholder="Enter answer" style="width:300px;"/>&nbsp;';
    	str += 'Correct<input type="checkbox" id="question-answer-correctchk' + (i+1) + '"' + checked + ' onclick="questionAnswerChk(this, ' + i + ')"/></td>';
    	str += '</td><td class="buttons"><img src="images/btn_delete.png" title="Delete Answer" onclick="questionDeleteAnswer(' + i + ')" class="button"/></td></tr>';
	}
	
	str += getFeedbackRows();
	str += '</table>';

	return str;
}

function questionTypeChk(chk, idx){
	blinkModified = true;
	var blink = blinkData;
	blink.question.multiplechoice = chk.checked;
	updateBlinkButtons();
	addBlinkPanelEvents();
}

function questionAnswerChk(chk, idx){
	blinkModified = true;
	var blink = blinkData;
	blink.question.answers[idx].correct = chk.checked;
	updateBlinkButtons();
	addBlinkPanelEvents();
}

function questionAddAnswer(){
	var blink = blinkData;
	blink.question.answers.push( { correct:false, text:"" });
	showQuestionBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function questionDeleteAnswer(idx){
	var blink = blinkData;
	blink.question.answers.splice(idx, 1);
	showQuestionBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function showWordHeatBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getWordHeatBlink());
	setTimeout(setScrollHeight, 10);
}

function addWordHeatEvents(blink){
	var elm = $('#wordheat-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.wordheat.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.wordheat.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.wordheat.timer);
	elm.width(60);
	
	$("#wordheat-prompt").keyup(function(){
		$("#wordheat-prompt").blur();
		$("#wordheat-prompt").focus();
	});

	$('#wordheat-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.wordheat.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	for(var i=0; i<blink.wordheat.words.length; i++){
		elm = $('#wordheat-word' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink word changed " + this.value);
			var idx = parseInt(this.id.substr(13));
			blink.wordheat.words[idx-1].word = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		elm = $('#wordheat-layer' + (i+1));
		elm.spinner({min:0, max:5, stop: function( event, ui ) {
			var idx = parseInt(this.id.substr(14));
			blink.wordheat.words[idx].layer = $(this).spinner("value");
			console.log("Spinner change to " + blink.wordheat.words[idx].layer);
			blinkModified=true;
			updateBlinkPanelButtons(); 
		}});
		elm.spinner("value", blink.wordheat.words[i].layer);
		elm.width(60);
	}
	
	addFeedbackEvents();
}

function getWordHeatBlink(){
	var blink = blinkData;
	if (blink.wordheat==null){
		blink.wordheat = new Object();
		blink.wordheat.prompt = "";
		blink.wordheat.words = new Array();
		blink.wordheat.timer = 0;
	}
	
	var str = '<table width="100%"><tr><td class="left" width="20%">Prompt</td>';
    str += '<td>';
    str += '<textarea id="wordheat-prompt" placeholder="Enter text prompt." style="width:100%; height:60px;">' + blink.wordheat.prompt + '</textarea>';
    str += '<td class="buttons">';
	str += '<a href="#"><img class="button" src="images/btn_plus.png" title="Add Word" onclick="wordheatAddWord()" /></a></td></tr>';
	str += '<tr><td class="left" width="20%">WordHeat</td><td>Time (in secs, 0 for not timed)&nbsp;<input id="wordheat-timer"></td><td class="buttons"></td></tr>';
	if (blink.wordheat!=null && blink.wordheat.words!=null){
		for(var i=0; i<blink.wordheat.words.length; i++){
			str += '<tr><td class="left">Word ' + (i+1) + '</td>';
			str += '<td><input id="wordheat-word' + (i+1) + '" value="' + blink.wordheat.words[i].word + '" style="width:200px">&nbsp;Recommended layer (0 if unconcerned)&nbsp;<input id="wordheat-layer' + (i+1) + '"></td>';
			str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this word" onclick="wordheatDeleteWord(' + i + ')" ></td></tr>';
		}
	}
	
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function wordheatAddWord(){
	var blink = blinkData;
	blink.wordheat.words.push( { word:"", layer:5 } );
	showWordHeatBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function wordheatDeleteWord(idx){
	var blink = blinkData;
	blink.wordheat.words.splice(idx, 1);
	showWordHeatBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function showVideoBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getVideoBlink());
	setTimeout(setScrollHeight, 10);
}

function addVideoEvents(blink){
	$("#video-prompt").keyup(function(){
		$("#video-prompt").blur();
		$("#video-prompt").focus();
	});

	$('#video-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.video.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	$("#video-vimeo").keyup(function(){
		$("#video-vimeo").blur();
		$("#video-vimeo").focus();
	});

	$('#video-vimeo').change( function(){
		console.log("blink vimeo changed " + this.value);
		blink.video.vimeo = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
}

function getVideoBlink(){
	var blink = blinkData;
	if (blink.video==null){
		blink.video = new Object();
		blink.video.prompt = "";
		blink.video.vimeo = "";
	}
	
	var str = '<table width="100%"><tr><td class="left" width="20%">Prompt</td>';
    str += '<td>';
    str += '<textarea id="video-prompt" placeholder="Enter text prompt." style="width:100%; height:60px;">' + blink.video.prompt + '</textarea>';
    str += '<td class="buttons"></td>';
	str += '<tr><td class="left" width="20%">Vimeo</td><td><input id="video-vimeo" value="' + blink.video.vimeo + '" style="width:200px"></td><td class="buttons"></td></tr>';
	str += '</table>';
	return str;
}

function showRotatorBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getRotatorBlink());
	setTimeout(setScrollHeight, 10);
}

function addRotatorEvents(blink){
	var elm = $('#rotator-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.rotator.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.rotator.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.rotator.timer);
	elm.width(60);
	
	$("#rotator-prompt").keyup(function(){
		$("#rotator-prompt").blur();
		$("#rotator-prompt").focus();
	});

	$('#rotator-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.rotator.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	for(var i=0; i<blink.rotator.statements.length; i++){
		elm = $('#rotator' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink statement changed " + this.value);
			var idx = parseInt(this.id.substr(7));
			blink.rotator.statements[idx-1] = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}
	
	addFeedbackEvents();
}

function getRotatorBlink(){
	var blink = blinkData;
	if (blink.rotator==null){
		blink.rotator = new Object();
		blink.rotator.prompt = "";
		blink.rotator.statements = new Array();
		blink.rotator.timer = 0;
	}
	var str = '<table width="100%"><tr><td class="left" width="20%">Prompt</td>';
    str += '<td>';
    str += '<textarea id="rotator-prompt" placeholder="Enter text prompt." style="width:100%; height:60px;">' + blink.rotator.prompt + '</textarea>';
    str += '<td class="buttons">';
	str += '<a href="#"><img class="button" src="images/btn_plus.png" title="Add Rotator" onclick="rotatorAddStatement()" /></a></td></tr>';
	str += '<tr><td class="left" width="20%">Statement</td><td>Time (in secs, 0 for not timed)&nbsp;<input id="rotator-timer"></td><td class="buttons"></td></tr>';
	if (blink.rotator!=null && blink.rotator.statements!=null){
		for(var i=0; i<blink.rotator.statements.length; i++){
			str += '<tr><td class="left">Statement ' + (i+1) + '</td>';
			str += '<td><input id="rotator' + (i+1) + '" value="' + blink.rotator.statements[i] + '" style="width:100%"></td>';
			str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this statement" onclick="rotatorDeleteStatement(' + i + ')" ></td></tr>';
		}
	}
	
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function rotatorAddStatement(){
	var blink = blinkData;
	blink.rotator.statements.push( "");
	showRotatorBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function rotatorDeleteStatement(idx){
	var blink = blinkData;
	blink.rotator.statements.splice(idx, 1);
	showRotatorBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function showThisThatBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getThisThatBlink());
	setTimeout(setScrollHeight, 10);
}

function addThisThatEvents(blink){
	var elm = $('#thisthat-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.thisthat.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.thisthat.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.thisthat.timer);
	elm.width(60);
	
	for(var i=0; i<blink.thisthat.questions.length; i++){
		elm = $('#thisthat-question' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink question changed " + this.value);
			var idx = parseInt(this.id.substr(17));
			blink.thisthat.questions[idx-1].text = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		var elm = $('#thisthat-value' + (i+1));
			elm.change(function( ) { 
				var idx = parseInt(this.id.substr(14));
				var selectedIdx = $('#' + this.id).prop('selectedIndex');
				blink.thisthat.questions[idx-1].value = (selectedIdx==0);
				console.log("value changed " + selectedIdx);
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			
		elm = $('#thisthat-correct' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink correct changed " + this.value);
			var idx = parseInt(this.id.substr(16));
			blink.thisthat.questions[idx-1].correct = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		elm = $('#thisthat-wrong' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink wrong changed " + this.value);
			var idx = parseInt(this.id.substr(14));
			blink.thisthat.questions[idx-1].wrong = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}
	
	addFeedbackEvents();
}

function getThisThatBlink(){
	var blink = blinkData;
	if (blink.thisthat==null){
		blink.thisthat = new Object();
		blink.thisthat.questions = new Array();
		blink.thisthat.timer = 0;
	}
	var str = '<table width="100%">';
    str += '<td class="buttons">';
	str += '<tr><td class="left" width="20%">This or That</td><td>Time (in secs, 0 for not timed)&nbsp;<input id="thisthat-timer"></td><td class="buttons">';
	str += '<a href="#"><img class="button" src="images/btn_plus.png" title="Add Question" onclick="thisthatAddQuestion()" /></a></td></tr>';
	if (blink.thisthat!=null && blink.thisthat.questions!=null){
		for(var i=0; i<blink.thisthat.questions.length; i++){
			str += '<tr><td class="left">Question ' + (i+1) + '</td>';
			str += '<td><textarea id="thisthat-question' + (i+1) + '" style="width:100%; height:60px;">' + blink.thisthat.questions[i].text + '</textarea><br/>';
			str += 'Correct button<select id="thisthat-value' + (i+1) + '">';
			if (blink.thisthat.questions[i].value){
				str += '<option selected>True</option>';
				str += '<option>False</option>';
			}else{
				str += '<option>True</option>';
				str += '<option selected>False</option>';
			} 
			str += '</select>';
			str += ' True display word<input id="thisthat-true' + (i+1) + '" type="text" value="' + blink.thisthat.questions[i].correct + '">';
			str += ' False display word<input id="thisthat-false' + (i+1) + '" type="text" value="' + blink.thisthat.questions[i].wrong + '">';
			str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this question" onclick="thisthatDeleteQuestion(' + i + ')" ></td></tr>';
		}
	}
	
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function thisthatAddQuestion(){
	var blink = blinkData;
	blink.thisthat.questions.push( {text:"", correct:"True", wrong:"False", value:true } );
	showThisThatBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function thisthatDeleteQuestion(idx){
	var blink = blinkData;
	blink.thisthat.questions.splice(idx, 1);
	showThisThatBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}