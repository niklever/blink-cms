// JavaScript Document
function showThisThatBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getThisThatBlink());
	setTimeout(setScrollHeight, 10);
}

function addThisThatEvents(blink){
	var elm = $('#thisthat-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.thisthat.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.thisthat.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.thisthat.timer);
	elm.width(60);
	
	elm = $('#thisthat-noscore');	
	elm.change( function(){
		console.log("blink no score changed " + this.checked);
		blink.thisthat.noscore = (this.checked);
		blinkModified=true;
		updateBlinkPanelButtons(); 
		return;
	});
	
	for(var i=0; i<blink.thisthat.questions.length; i++){
		elm = $('#thisthat-question' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink question changed " + this.value);
			var idx = parseInt(this.id.substr(17));
			blink.thisthat.questions[idx-1].text = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		var elm = $('#thisthat-value' + (i+1));
			elm.change(function( ) { 
				var idx = parseInt(this.id.substr(14));
				var selectedIdx = $('#' + this.id).prop('selectedIndex');
				blink.thisthat.questions[idx-1].value = (selectedIdx==0);
				console.log("value changed " + selectedIdx);
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			
		elm = $('#thisthat-correct' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink correct changed " + this.value);
			var idx = parseInt(this.id.substr(16));
			blink.thisthat.questions[idx-1].correct = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		elm = $('#thisthat-wrong' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink wrong changed " + this.value);
			var idx = parseInt(this.id.substr(14));
			blink.thisthat.questions[idx-1].wrong = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		addFeedbackEventsForItem( blink.thisthat.questions[i], i, true);
	}
	
	addFeedbackEvents();
}

function getThisThatBlink(){
	var blink = blinkData;
	if (blink.thisthat==null){
		blink.thisthat = new Object();
		blink.thisthat.questions = new Array();
		blink.thisthat.timer = 0;
		blink.thisthat.noscore = false;
	}
	if (blink.thisthat.noscore==null) blink.thisthat.noscore = false;
	var str = '<table width="100%">';
    str += '<td class="buttons">';
	var checked = (blink.thisthat.noscore) ? "checked" : "";
	str += '<tr><td class="left" width="20%">This or That</td><td>Time (in secs, 0 for not timed)&nbsp;<input id="thisthat-timer">';
	str += ' No scoring? <input id="thisthat-noscore" type="checkbox" ' + checked + '></td><td class="buttons">';
	str += '<a href="#"><img class="button" src="images/btn_plus.png" title="Add Question" onclick="thisthatAddQuestion()" /></a></td></tr>';
	if (blink.thisthat!=null && blink.thisthat.questions!=null){
		for(var i=0; i<blink.thisthat.questions.length; i++){
			str += '<tr><td class="left">Question ' + (i+1) + '</td>';
			str += '<td><textarea id="thisthat-question' + (i+1) + '" style="width:100%; height:60px;">' + blink.thisthat.questions[i].text + '</textarea><br/>';
			str += 'Correct button<select id="thisthat-value' + (i+1) + '">';
			if (blink.thisthat.questions[i].value){
				str += '<option selected>True</option>';
				str += '<option>False</option>';
			}else{
				str += '<option>True</option>';
				str += '<option selected>False</option>';
			} 
			str += '</select>';
			str += ' True display word<input id="thisthat-correct' + (i+1) + '" type="text" value="' + blink.thisthat.questions[i].correct + '">';
			str += ' False display word<input id="thisthat-wrong' + (i+1) + '" type="text" value="' + blink.thisthat.questions[i].wrong + '">';
			str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this question" onclick="thisthatDeleteQuestion(' + i + ')" ></td></tr>';
			str += getFeedbackRowsForItem(blink.thisthat.questions[i], i, "Question", true);
		}
	}
	
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function thisthatAddQuestion(){
	var blink = blinkData;
	blink.thisthat.questions.push( {text:"", correct:"True", wrong:"False", value:true } );
	showThisThatBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function thisthatDeleteQuestion(idx){
	var blink = blinkData;
	blink.thisthat.questions.splice(idx, 1);
	showThisThatBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}