// JavaScript Document
function getSimonBlink(mode){
	var simon = blinkData.simon;
	if (simon==null){
		simon = { prompt:"", sounds:[], phrase:"", wrong:"", time:0, score:0, gems:0, sound:"" };
		blinkData.simon = simon;
	}
	if (simon.wrong==null) simon.wrong="";
	var str = '<table width="100%">';
    str += '<tr><td class="left" width="20%">Prompt</td><td><textarea id="simon-prompt" placeholder="Enter your prompt text here" style="width:100%; height:60px;">' + simon.prompt + '</textarea></td><td class="buttons"></td></tr>';
	str += getPromptSoundHTML(simon);
    str += '<tr><td class="left" width="20%">Phrase</td><td><textarea id="simon-phrase" placeholder="Enter your phrase here, use | to seperate blocks." style="width:100%; height:60px;">' + simon.phrase + '</textarea></td>';
	str += '<td class="buttons"><a href="#"><img class="button" src="images/btn_create.png" title="Update sounds" onclick="simonUpdateSounds()" ></a></td></tr>';
	str += '<tr><td class="left" width="20%">Wrong</td><td><textarea id="simon-wrong" placeholder="Enter your wrong words here, use | to seperate blocks." style="width:100%; height:60px;">' + simon.wrong + '</textarea></td>';
	str += '<td class="buttons"><a href="#"><img class="button" src="images/btn_create.png" title="Update sounds" onclick="simonUpdateSounds( )" ></a></td></tr>';		
	var blocks1 = simon.phrase.split("|");
	var blocks2 = simon.wrong.split("|");
	var count = blocks1.length + blocks2.length;
	if (count != blinkData.simon.sounds.length){
		while (count>blinkData.simon.sounds.length) blinkData.simon.sounds.push("");
		while (count<blinkData.simon.sounds.length) blinkData.simon.sounds.pop();
	}
	if (simon.phrase!="" && blocks1.length>0){
		for(var index in blocks1){
			str += '<form id="simon-sound-form' + index + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
			str += '<tr><td class="left" width="20%">' + blocks1[index] + '</td><td><input type="file" name="sound"/>';
			str += '<input type="hidden" name="method" value="sound" />';
			str += '<input type="hidden" name="id" value="' + index + '" />';
			str += '<input type="submit" value="Upload" />';
			str += '</form>';
			str += '<td class="buttons"><a href="#"><img class="button" src="images/btn_delete_sound.png" title="Update sounds" onclick="simonDeleteSound(' + index + ')" ></a>';
			str += '<audio id="simon-sound' + index + '" controls><source src="../courses/sounds/' + simon.sounds[index] + '" type="audio/mpeg"></audio></td></tr>';
		}
	}
	if (simon.wrong!="" && blocks2.length>0){
		for(var index in blocks2){
			var idx = parseInt(index) + blocks1.length;
			str += '<form id="simon-sound-form' + idx + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
			str += '<tr><td class="left" width="20%">' + blocks2[index] + '</td><td><input type="file" name="sound"/>';
			str += '<input type="hidden" name="method" value="sound" />';
			str += '<input type="hidden" name="id" value="' + idx + '" />';
			str += '<input type="submit" value="Upload" />';
			str += '</form>';
			str += '<td class="buttons"><a href="#"><img class="button" src="images/btn_delete_sound.png" title="Update sounds" onclick="simonDeleteSound(' + idx + ')" ></a>';
			str += '<audio id="simon-sound' + idx + '" controls><source src="../courses/sounds/' + simon.sounds[idx] + '" type="audio/mpeg"></audio></td></tr>';
		}
	}
	str += '</td></tr>';
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function showSimonBlink(){
	$('#blink-content').html(getSimonBlink());
	$("blinks").attr("overflow", "hidden");
	setTimeout(setScrollHeight, 10);
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function addSimonEvents(){
	var elm = $("#simon-prompt");
	
	elm.keyup(function(){
			$("#simon-prompt").blur();
			$("#simon-prompt").focus();
		});
		
	elm.change( function(){
			console.log(this.value);
			blinkData.simon.prompt = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	
	var elm = $("#simon-phrase");
	
	elm.keyup(function(){
			$("#simon-phrase").blur();
			$("#simon-phrase").focus();
		});
		
	elm.change( function(){
			console.log(this.value);
			blinkData.simon.phrase = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
	var elm = $("#simon-wrong");
	
	elm.keyup(function(){
			$("#simon-wrong").blur();
			$("#simon-wrong").focus();
		});
		
	elm.change( function(){
			console.log(this.value);
			blinkData.simon.wrong = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
	for(var index in blinkData.simon.sounds ){	
		var elm = $('#simon-sound-form' + index);
		elm.ajaxForm(function(data) {
			console.log("Simon sound form " + data); 
			var json = JSON.parse(data);
			if (json.success){
				var src = "../courses/sounds/" + json.path;
				blinkData.simon.sounds[json.id] = json.path;
				var elm = $('#simon-sound' + json.id);
				if (elm!=null){
					elm.attr("src", src+"?timestamp=" + new Date().getTime());
				}
				blinkModified=true;
				updateBlinkPanelButtons();
			}
		});
	}
	
	addFeedbackEvents();
	addPromptSoundEvents(blinkData.simon);
}

function simonUpdateSounds(){
	var blocks1 = blinkData.simon.phrase.split("|");
	var blocks2 = blinkData.simon.wrong.split("|");
	var count = blocks1.length + blocks2.length;
	if (count != blinkData.simon.sounds.length){
		while (count>blinkData.simon.sounds.length) blinkData.simon.sounds.push("");
		while (count<blinkData.simon.sounds.length) blinkData.simon.sounds.pop();
	}
	showSimonBlink();
}

function simonDeleteSound(idx){
	blinkData.simon.sounds[idx] = "";
	var elm = $("#simon-sound" + idx);
	if (elm!=null) elm.attr("src", "");
	blinkModified = true;
	updateBlinkPanelButtons();
}