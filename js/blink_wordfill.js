// JavaScript Document
function showWordfillBlink(){
	$('#blink-content').html(getWordfillBlink());
	$("blinks").attr("overflow", "hidden");
	setTimeout(setScrollHeight, 10);
}

function getWordfillBlink(){
	var blink = blinkData;
	if (blink.wordfill==null){
		blink.wordfill = { text:"", prompt:"", sound:"", words:[] };
	}
	if (blink.wordfill.words == null) blink.wordfill.words = [];
	var text = (blink.wordfill!=null) ? blink.wordfill.text : "";
	var prompt = (blink.wordfill!=null && blink.wordfill.prompt!=null) ? blink.wordfill.prompt : "";
	var str = '<table width="100%">';
	str += '<tr><td class="left" width="20%">WordFill</td><td>Time (in secs, 0 for not timed)&nbsp;<input id="wordfill-timer"></td><td class="buttons"></td></tr>';
	str += '<tr><td class="left">Prompt</td><td><textarea id="wordfill-prompt" placeholder="Enter a prompt here" style="width:100%; height:60px;">' + prompt + '</textarea></td><td class="buttons"></td></tr>';
	str += getPromptSoundHTML(blink.wordfill);
	str += '<tr><td class="left">Text</td>';
    str += '<td><textarea id="wordfill-text" placeholder="Enter text here. Add ##Answer## where you want a space for an answer." style="width:100%; height:60px;">' + text + '</textarea>';
    str += '<td class="buttons">';
	str += '<a href="#"><img class="button" src="images/btn_plus.png" title="Add block" onclick="wordfillAddWord()" /></a></td></tr>';
	if (blink.wordfill!=null && blink.wordfill.words!=null){
		for(var i=0; i<blink.wordfill.words.length; i++){
			str += '<tr><td class="left">Word ' + (i+1) + '</td>';
			str += '<td><input id="wordfill-word' + (i+1) + '" value="' + blink.wordfill.words[i] + '"></td>';
			str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this word" onclick="wordfillDeleteWord(' + i + ')" ></td></tr>';
		}
	}
	
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function addWordfillEvents(blink){
	var elm = $('#wordfill-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.wordfill.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.wordfill.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.wordfill.timer);
	elm.width(60);
	
	$("#wordfill-prompt").keyup(function(){
		$("#wordfill-prompt").blur();
		$("#wordfill-prompt").focus();
	});

	$('#wordfill-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.wordfill.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	$("#wordfill-text").keyup(function(){
		$("#wordfill-text").blur();
		$("#wordfill-text").focus();
	});

	$('#wordfill-text').change( function(){
		console.log("blink text changed " + this.value);
		blink.wordfill.text = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	for(var i=0; i<blink.wordfill.words.length; i++){
		elm = $('#wordfill-word' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink wordfill changed " + this.value);
			var idx = parseInt(this.id.substr(13));
			blink.wordfill.words[idx-1] = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}
	
	addFeedbackEvents();
	addPromptSoundEvents(blink.wordfill.sound);
}

function wordfillAddWord(){
	var blink = blinkData;
	if (blink.wordfill==null) blink.wordfill = new Object();
	if (blink.wordfill.prompt==null) blink.wordfill.prompt = "";
	if (blink.wordfill.words==null) blink.wordfill.words = new Array();
	if (blink.wordfill.text==null) blink.wordfill.text = "";
	if (blink.wordfill.timer==null) blink.wordfill.timer = 60;
	blink.wordfill.words.push("");
	showWordfillBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function wordfillDeleteWord(idx){
	var blink = blinkData;
	blink.wordfill.words.splice(idx, 1);
	showWordfillBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}