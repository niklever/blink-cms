// JavaScript Document

function showOrderItemsBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getOrderItemsBlink());
	setTimeout(setScrollHeight, 10);
}

function addOrderItemsEvents(blink){
	var elm = $('#orderitems-timer');
	elm.spinner({min:0, stop: function( event, ui ) {
		blink.orderitems.timer = $(this).spinner("value");
		console.log("Spinner change to " + blink.orderitems.timer);
		blinkModified=true;
		updateBlinkPanelButtons(); 
    }});
	elm.spinner("value", blink.orderitems.timer);
	elm.width(60);
	
	$("#orderitems-prompt").keyup(function(){
		$("#orderitems-prompt").blur();
		$("#orderitems-prompt").focus();
	});

	$('#orderitems-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.orderitems.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	$('#orderitems-ordered').click(function() {
	  	blink.orderitems.ordered = $(this).is(':checked');
	});
	
	for(var i=0; i<blink.orderitems.slots.length; i++){
		elm = $('#slots' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink slot changed " + this.value);
			var idx = parseInt(this.id.substr(5));
			blink.orderitems.slots[idx-1] = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
	}
	
	for(var i=0; i<blink.orderitems.items.length; i++){
		elm = $('#items' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink item changed " + this.value);
			var idx = parseInt(this.id.substr(5));
			blink.orderitems.items[idx-1].text = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		var elm = $('#items-slot' + (i+1));
		elm.spinner({min:0, stop: function( event, ui ) {
			var idx = parseInt(this.id.substr(10));
			blink.orderitems.items[idx-1].slot = $(this).spinner("value");
			console.log("Spinner change to " + blink.orderitems.items[idx-1].slot);
			blinkModified=true;
			updateBlinkPanelButtons(); 
		}});
		elm.spinner("value", blink.orderitems.items[i].slot);
		elm.width(60);
	}
	
	addFeedbackEvents();
	addPromptSoundEvents(blink.orderitems);
}

function getOrderItemsBlink(){
	var blink = blinkData;
	if (blink.orderitems==null){
		blink.orderitems = new Object();
		blink.orderitems.slots = new Array();
		blink.orderitems.items = new Array();
		blink.orderitems.ordered = true;
		blink.orderitems.timer = 0;
		blink.orderitems.sound = "";
	}
	var str = '<table width="100%"><tr><td class="left" width="20%">Prompt</td>';
    str += '<td>';
    str += '<textarea id="orderitems-prompt" placeholder="Enter text prompt. For each statement use | to indicate block break." style="width:100%; height:60px;">' + blink.orderitems.prompt + '</textarea>';
    str += '<td class="buttons">';
	var ordered = (blink.orderitems.ordered) ? "checked" : "";
	str += '<a href="#"><img class="button" src="images/btn_plusitem.png" title="Add Item" onclick="orderitemsAdd(false)" /></a>';
	str += '<a href="#"><img class="button" src="images/btn_plusslot.png" title="Add Slot" onclick="orderitemsAdd(true)" /></a></td></tr>';
	str += getPromptSoundHTML(blink.orderitems);
	str += '<tr><td class="left" width="20%">Order Items</td><td>Ordered<input id="orderitems-ordered" type="checkbox" ' + ordered + '> Time (in secs, 0 for not timed)&nbsp;<input id="orderitems-timer"></td><td class="buttons"></td></tr>';
	for(var i=0; i<blink.orderitems.items.length; i++){
		str += '<tr><td class="left">Item ' + (i+1) + '</td>';
		str += '<td><input id="items' + (i+1) + '" value="' + blink.orderitems.items[i].text + '" style="width:200px"> Slot (0 for any slot) <input id="items-slot' + (i+1) + '"></td>';
		str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this item" onclick="orderitemsDelete(' + i + ', false)" ></td></tr>';
	}
	for(var i=0; i<blink.orderitems.slots.length; i++){
		str += '<tr><td class="left">Slot ' + (i+1) + '</td>';
		str += '<td><input id="slots' + (i+1) + '" value="' + blink.orderitems.slots[i] + '" style="width:200px"></td>';
		str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this item" onclick="orderitemsDelete(' + i + ', true)" ></td></tr>';
	}
	
	str += getFeedbackRows();
	str += '</table>';
	return str;
}

function orderitemsAdd(slot){
	var blink = blinkData;
	if (slot){
		blink.orderitems.slots.push( "");
	}else{
		blink.orderitems.items.push( {text:"", slot:0} );
	}
	showOrderItemsBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function orderitemsDelete(idx, slot){
	var blink = blinkData;
	if (slot){
		blink.orderitems.slots.splice(idx, 1);
	}else{
		blink.orderitems.items.splice(idx, 1);
	}
	showOrderItemsBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}