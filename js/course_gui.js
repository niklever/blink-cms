// JavaScript Document
var scrollTop = 0; 

function newBlink(where){
	var growth = $('#blink_growth').is (':checked');
	var type = $('#new_blink_type option:selected').index();
	if (type>2) type+=2;
	if (type==2) type++;
	var summary = $('#new_blink_summary')[0].value;
	console.log('newBlink type:' + type + ' summary:' + summary);
	var index;
	
	if (growth){
		if (where=='start'){
			index = 0;
		}else if(where=='end'){
			index = course.growth.length;
		}else if(where=='before'){
			index = blinkFirst-1;
		}else if(where=='after'){
			index = blinkLast;
			while(!course.growth[index].visible && index<(course.growth.length-1)){
				console.log("newBlink index:" + index + " blink(" + course.growth[index].toString() + ")\n");
				index++;
			}
			//while((course.blinks[index].type==2 || course.blinks[index].type==4) && course.blinks[index].visible) index--;
		}
		if (index<0) index = 0;
		if (index>course.growth.length) index = course.growth.length;
		
		var blink = new Blink(type, summary);
		course.addGA(index, blink);
		
		if (type==1){
			//Learning unit add closing blink
			blink = new Blink(2, summary);
			course.addGA(index + 1, blink);
		}else if (type==3){
			//Burst add closing blink
			blink = new Blink(4, summary);
			course.addGA(index + 1, blink);
		}
		
		//blinkEditInfo = { index:index, id:0, growth:growth };
	}else{
		if (where=='start'){
			index = 0;
		}else if(where=='end'){
			index = course.blinks.length;
		}else if(where=='before'){
			index = blinkFirst-1;
		}else if(where=='after'){
			index = blinkLast;
			while(!course.blinks[index].visible && index<(course.blinks.length-1)){
				console.log("newBlink index:" + index + " blink(" + course.blinks[index].toString() + ")\n");
				index++;
			}
			//while((course.blinks[index].type==2 || course.blinks[index].type==4) && course.blinks[index].visible) index--;
		}
		if (index<0) index = 0;
		if (index>course.blinks.length) index = course.blinks.length;
		
		var blink = new Blink(type, summary);
		course.add(index, blink);
		
		if (type==1){
			//Learning unit
			blink = new Blink(2, summary);
			course.add(index+1, blink);
		}else if (type==3){
			//Burst
			blink = new Blink(4, summary);
			course.add(index+1, blink);
		}
	}
	
	blinkFirst = blinkLast = -1;
	
	updateBlinksTable();
	
	$('#new_blink_type option')[0].selected = true;
	$('#new_blink_summary')[0].value = "";	
	
	updateInsertButtons();
}

function buildUnit(idx){
	console.log("build unit " + idx);
	$.post("course_update.php", { courseId: course.id, index:idx, type: "build_unit" })
	  .done(function( msg ) {
		console.log( "Unit build: " + msg );
		var result = JSON.parse(msg);
		if (result.success){
			showFeedbackC(result.msg, "Unit built");
		}else{
			showFeedbackC(result.error, "Unit build error");
		}
	  });
}

function addCourseEvents(){
	$( "#blinks").selectable({ 
				filter: "td.blink",
				distance: 1,
				selected: function (event, ui) {
					if ($(ui.selected).hasClass('click-selected')) {
						$(ui.selected).removeClass('ui-selected click-selected');
					} else {
						$(ui.selected).addClass('click-selected');
					}
				},
				unselected: function (event, ui) {
					$(ui.unselected).removeClass('click-selected');
				},
				stop: function() {
					var result = $( this ).find( ".ui-selected" ).map(function() {
				  		return this.id;
				}).get().join(", ");
				blinkFirst = blinkLast = -1;
				blinksSelected = new Array();
				if (result!=""){
					var tokens = result.split(", ");
					var id;
					for(var i=0; i<tokens.length; i++){
						if (tokens[i].substr(0, 2)=='ga'){
							id = parseInt(tokens[i].substr(2));
						}else{
							id = parseInt(tokens[i].substr(5));
						}
						blinksSelected.push(id);
						if (blinkFirst==-1 || blinkFirst>id) blinkFirst=id;
						if (blinkLast==-1 || blinkLast<id) blinkLast=id;
					}
					console.log("Blinks selected " + result + " first:" + blinkFirst + " last:" + blinkLast);
				}
				updateBlinkButtons();
				updateInsertButtons();
			  } });
	
	
	if (loggedIn){
		 $('.sidebar1').css('display', 'none');
		 $('.content').css('width', '100%');
	}
			  
	updateBlinksTable();
	blinksModified = false;
	
	$(window).resize(function() {
		windowResized();
    });
	
	$(window).trigger('resize');
	
	$('#iconForm').ajaxForm(function(data) {
		console.log("iconForm " + data); 
		var json = JSON.parse(data);
		if (json.success){
			var src = imagePath + json.path;
			$("#iconImg").attr("src", src+"?timestamp=" + new Date().getTime());
		}
	});
	$('#imageForm').ajaxForm(function(data) { 
		console.log("imageForm " + data);
		var json = JSON.parse(data);
		if (json.success){
			var src = imagePath + json.path;
			$("#imageImg").attr("src", src+"?timestamp=" + new Date().getTime());
		}
	});
	
	$('#new_blink_type').change( function(){
		updateInsertButtons();
		console.log("new blink type changed " + this.value + " " + $('#new_blink_type option:selected').index());
	});
	
	$("#new_blink_summary").keyup(function(){
        $("#new_blink_summary").blur();
        $("#new_blink_summary").focus();
	});

	$('#new_blink_summary').change( function(){
		//console.log("new blink summary changed " + this.value);
		updateInsertButtons();
	});
	
	$( "#delete-blink-confirm" ).dialog({
      resizable: false,
      height:240,
      modal: true,
	  autoOpen: false,
      buttons: {
        "Delete blink": function() {
			if (blinkGrowth){
				course.deleteGA(blinkIdx);
			}else{
				course.delete(blinkIdx);
			}
			if (blinkEditInfo!=null && blinkEditInfo.index==blinkIdx) blinkEditInfo = null;
			blinkIdx = -1;
			updateBlinksTable();
          	$( this ).dialog( "close" );
        },
        Cancel: function() {
			blinkIdx = -1;
          	$( this ).dialog( "close" );
        }
      }
    });
	
	$( "#delete-blinks-confirm" ).dialog({
      resizable: false,
      height:240,
      modal: true,
	  autoOpen: false,
      buttons: {
        "Delete blinks": function() {
			while(blinksSelected.length>0) course.delete(blinksSelected.pop()-1);
			blinkEditInfo = null;
			blinksModified = true;
			updateBlinkButtons();
			updateBlinksTable();
          	$( this ).dialog( "close" );
        },
        Cancel: function() {
			blinkIdx = -1;
          	$( this ).dialog( "close" );
        }
      }
    });
	
    $( "#blinks-feedback" ).dialog({
      height: 240,
	  resizable: false,
	  autoOpen: false,
      modal: true
    });
	
	$( "#blink-feedback" ).dialog({
      height: 240,
	  resizable: false,
	  autoOpen: false,
      modal: true
    });
	
	$( "#blink-confirm-yesno" ).dialog({
      resizable: false,
      height:240,
      modal: true,
	  autoOpen: false,
      buttons: {
        "Yes": function() {
			if (yesNoFunc!=null) yesNoFunc();
          	$( this ).dialog( "close" );
        },
		"No": function() {
          	$( this ).dialog( "close" );
        },
        Cancel: function() {
			blinkIdx = -1;
          	$( this ).dialog( "close" );
        }
      }
    });
	//showFeedback("hello");
	
	$('<div id="overlay"/>').css({
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        height: $(window).height() + 'px',
		opacity:0.6,
        background: 'grey url(images/busy.gif) no-repeat center'
    }).hide().appendTo('body');
	
	updateInsertButtons();
	updateBlinkButtons();
}

function showCourseOverview(mode){
	var str = "";
	if (mode){
		str += '<a href="#" onclick="showCourseOverview(false)">Close overview</a>&nbsp;&nbsp;<span class="small">Last modified ' + modifiedDate + '</span>';
		$('#overview').show("slow", function() {
			windowResized();
		  });
	}else{
		str += '<a href="#" onclick="showCourseOverview(true)">Show overview</a>&nbsp;&nbsp;<span class="small">Last modified ' + modifiedDate + '</span><br/><br/>';
		$('#overview').hide("slow", function() {
			updateBlinksTable();
		  });
	}
	$('#overview-btn').html(str);
}

function liteBlink(elmName, idx, growth){
	var elm = $(elmName);
	var blink;
	if (growth!=null && growth){
		blink = course.getGA(idx);
	}else{
		blink = course.get(idx);
	}
	blink.lite = !blink.lite;
	console.log(elmName + " index:" + idx + " set to " + blink.lite);
	if (blink.lite){
		elm.attr("src", "images/btn_lite_checked.png?timestamp=" + new Date().getTime());
	}else{
		elm.attr("src", "images/btn_lite_unchecked.png?timestamp=" + new Date().getTime());
	}
	if (blink.type==1 || blink.type==3){
		//Must add all blinks in folder
		var closingType = blink.type + 1;
		var mode = blink.lite;
		do{
			idx++;
			blink = course.get(idx);
			blink.lite = mode;
			elm = $('#blinklite' + (idx + 1));
			if (mode){
				elm.attr("src", "images/btn_lite_checked.png");
			}else{
				elm.attr("src", "images/btn_lite_unchecked.png");
			}		
		}while(blink.type!=closingType && idx<(course.count()-1));
	}
	blinksModified = true;
	updateBlinkButtons();
}

function copyBlink(idx){
	var src = course.get(idx);
	var dest = new Blink(src.type, src.summary);
	dest.json = src.json;
	if (blinkLast==-1){
		index = course.blinks.length;
	}else{
		index = blinkLast;
		while(!course.blinks[index].visible && index<(course.blinks.length-1)){
			console.log("newBlink index:" + index + " blink(" + course.blinks[index].toString() + ")\n");
			index++;
		}
	}
	course.add(index, dest);
	$.post("blink_update.php", { method:'copy', id:src.id, guid:dest.guid, index:index })
	  .done(function( json ) {
		  	console.log("copyBlink " + json);
			var data = JSON.parse(json);
			console.log("copyBlink: " + data.msg);
			dest.id = data.id;	
			updateBlinksTable();
	  });
	blinksModified = true;
	updateBlinkButtons();
}

function showFeedbackC(msg, title){
	console.log("showFeedbackC:" + msg);
	var elm = $("#blinks-feedback-msg");
	elm.text(msg);
	elm = $("#blinks-feedback");
	if (title==null){
		elm.dialog( "option", "title", "Blinks saved");
	}else{
		elm.dialog( "option", "title", title);
	}
	elm.dialog('open');
}

function saveCourse(newCourse){
	for(var i=0; i<course.blinks.length; i++){
		var blink = course.get(i);
		console.log("saveCourse blink:" + i + " json:" + blink.json);
		/*var json = JSON.parse(blink.json);
		if (json.feedback!=null && json.feedback!="undefined"){
			if (json.feedback instanceof Array){
				json.feedback[0] = replaceQuotes(json.feedback[0]);
				json.feedback[1] = replaceQuotes(json.feedback[1]);
			}else{
				json.feedback = replaceQuotes(json.feedback);
			}
		}
		blink.json = escapeJSON(JSON.stringify(json));*/
		blink.json = escapeJSON(blink.json);
	}
	for(var i=0; i<course.growth.length; i++){
		var blink = course.getGA(i);
		blink.json = escapeJSON(blink.json);
	}
	var str = JSON.stringify(course);
	//str = str.escapeJSON();
	//console.log('saveCourse new:' + newCourse + " json:" + str);
	$('#overlay').show();
	var type = (newCourse) ? 'duplicate_course' : 'course';
	$.post("course_update.php", { id: course.id, new:newCourse, type: type, value: str })
	  .done(function( data ) {
		console.log( "saveCourse: " + data );
		var msg = "";
		if (data.charAt(0)=='{'){
			//New course
			var info = JSON.parse(data);
			var url = "course.php?id=" + info.id;
			window.history.pushState({path:url},'',url);
			course.id = info.id;
			course.title = info.title;
			$('#title')[0].value = info.title;
			msg = info.msg;
		}else{
			msg = data;
			if (msg.indexOf('course saved to database')>-1){
				var date = new Date();
				var hours = date.getHours();
				var mins = String(date.getMinutes());
				
				if (mins.length<2) mins = '0' + mins;
				var ampm = 'am';
				if (hours>=12){
					ampm = 'pm';
					hours-=12;
				}
				hours = String(hours);
				if (hours.length<2) hours = '0' + hours;
				modifiedDate = hours + ':' + mins + ampm + ' ' + $.datepicker.formatDate('M dd - yy', date);
				if ($('#overview').css('display')=='block'){
					str = '<a href="#" onclick="showCourseOverview(false)">Close overview</a>&nbsp;&nbsp;<span class="small">Last modified ' + modifiedDate + '</span>';
				}else{
					str = '<a href="#" onclick="showCourseOverview(true)">Show overview</a>&nbsp;&nbsp;<span class="small">Last modified ' + modifiedDate + '</span><br/><br/>';
				}
				$('#overview-btn').html(str);
			}
		}
		showFeedbackC(msg);
		blinksModified= false;
		updateBlinkButtons();
		$('#overlay').hide();
	  });
}

function expandBlinks(mode){
	for(var i=0; i<course.count(); i++){
		var blink = course.get(i);
		blink.expanded = mode;
	}
	for(var i=0; i<course.countGA(); i++){
		var blink = course.getGA(i);
		blink.expanded = mode;
	}
	updateBlinksTable();
}

function deleteBlinks(){
	console.log('deleteBlinks');
	$('#delete-blinks-confirm').dialog("open");
}

function deleteBlink(idx, growth){
	blinkIdx = idx;
	blinkGrowth = (growth != null && growth);
	$('#delete-blink-confirm').dialog("open");
}

function summaryBlink(idx, growth){
	console.log("Summary pressed " + idx);
}

function buildCourse(){
	console.log('buildCourse ' + course.id);
	$.post("course_update.php", { courseId: course.id, type: "build_course" })
	  .done(function( msg ) {
		console.log( "Course build: " + msg );
		var result = JSON.parse(msg);
		if (result.success){
			showFeedbackC(result.msg, "Course built");
		}else{
			showFeedbackC(result.error, "Course build error");
		}
	  });
}

function updateBlinkButtons(){
	var count = course.count();
	//console.log('updateBlinkButtons count:' + count + ' selected:' + blinksSelected.join(','));
	//blinkFirst and blinkLast are set by the selectable event for the blink table
	enableButton('#btn_save', blinksModified);
	enableButton('#btn_delete', blinksSelected.length>0);
	enableButton('#btn_build', count>0);
	enableButton('#btn_expand', count>0);
	enableButton('#btn_collapse', count>0);
	enableButton('#btn_save_as', count>0);
}

function updateInsertButtons(){
	var type = $('#new_blink_type option:selected').index();
	var summary = $('#new_blink_summary')[0].value;
	//console.log('updateInsertButtons type:' + type + ' summary:' + summary);
	//blinkFirst and blinkLast are set by the selectable event for the blink table
	enableButton('#btn_start', (type!=0 && summary!=""));
	enableButton('#btn_end', (type!=0 && summary!=""));
	enableButton('#btn_before', (type!=0 && summary!="" && blinkFirst!=-1 && blinkLast!=-1));
	enableButton('#btn_after', (type!=0 && summary!="" && blinkFirst!=-1 && blinkLast!=-1));
}

function enableButton(id, mode){
	var elm = $(id);
	elm[0].style.pointerEvents = (mode) ? 'auto' : 'none';
	var opacity = (mode) ? 1 : 0.5;
	elm.css('opacity', opacity);
}

function showError(str){
	var msg = $('#msg');
	msg.text(str);
	msg.css('display', 'block');
}

function moveBlink(dir, idx, growth){
	if (growth){
		if (dir=='up'){
			course.moveGA(idx, true);
		}else if (dir=='down'){
			course.moveGA(idx, false);
		}
	}else{
		if (dir=='up'){
			course.move(idx, true);
		}else if (dir=='down'){
			course.move(idx, false);
		}
	}
	
	updateBlinksTable();
}

function editBlink(idx, id, growth){
	console.log("Edit blink index:" + idx + " id:" + id + " growth:" + growth + " blinkData:" + JSON.stringify(blinkData));
	
	if (blinkEditInfo!=null){
		//Update the json
		var blink;
		if (growth!=null && growth){
			blink = course.getGA(blinkEditInfo.index);
		}else{
			blink = course.get(blinkEditInfo.index);
		}
		var str = JSON.stringify(blinkData);
		//var str = JSON.stringify(blink.json);
		str = str.escapeJSON();
		blink.json = str;
	}
	
	if (idx==-1){
		blinkEditInfo = null;
		//clearInterval(iframeResizeID);
	}else{
		blinkEditInfo = { index:idx, id:id, growth:growth };
		//iframeResizeID = setInterval(iframeResize, 300);
	}
	updateBlinksTable();
}

function iframeResize(){
	setIframeHeight("blink-inline");
}

function expandBlink(idx, mode, growth){
	var blink;
	if (growth!=null && growth){
		blink = course.getGA(idx);
	}else{
		blink = course.get(idx);
	}
	blink.expanded = mode;
	var closingType = blink.type+1;
	do{
		blink = course.get(idx++);
		//blink.expanded = mode;
	}while(blink.type!=closingType && idx<course.count());
	if (blink.type==closingType) blink.expanded = mode;
	updateBlinksTable();
}

function updateBlinksTable(){
	var str = '<table width="100%">';
	var icons = ['icon_learning_start', 'icon_learning_end', 'icon_burst_start', 'icon_burst_end', 'icon_menu', 'icon_blink'];
	var indent = 0;
	var folderEmpty = false;
	
	for(var i=0; i<course.count(); i++){
		var blink = course.get(i);
		var icon = (blink.type<icons.length) ? icons[blink.type-1] : 'icon_blink';
		var canCopy = (icon=='icon_blink');
		if (canCopy && blink.json!=null){
			try{
				var obj = JSON.parse(blink.json);
			 	if (Object.keys(obj).length>1) icon += "_edited"; 
			}catch(e){
				console.log("Problem parsing JSON index:" + i + " " + blink.json);
			}
		}
		var summary = (blink.summary==null) ? 'No summary' : blink.summary;
		blink.visible = false;
		if (blink.type==2 || blink.type==4){ 
			indent-=2; 
			if (folderEmpty){
				//Show empty row
				str += '<tr><td>&nbsp;</td><td><span class="small">Empty</span></td><td class="buttons">&nbsp;</td>';
				folderEmpty = false;
			}
			blink.visible = blink.expanded;
			continue; 
		}
		var indentstr = "";
		for(var j=0; j<indent; j++) indentstr += "&nbsp;";
		if (blink.type==1){
			if (blink.expanded){
				str += '<tr><td width="15%">' + indentstr + '<img src="images/icon_learning_end.png" width="32" height="32" onclick="expandBlink(' + i + ', false)" />&nbsp;' + (i+1) + '</td><td class="blink" id="blink' + (i+1) + '">' + blink.summary + '<br /><span class="small">' + Blink.types[blink.type-1] + '</span></td>';
			}else{
				str += '<tr><td width="15%">' + indentstr + '<img src="images/icon_learning_start.png" width="32" height="32" onclick="expandBlink(' + i + ', true)" />&nbsp;' + (i+1) + '</td><td class="blink" id="blink' + (i+1) + '">' + blink.summary + '<br /><span class="small">' + Blink.types[blink.type-1] + '</span></td>';
			}
		}else if (blink.type==3){
			if (blink.expanded){
				str += '<tr><td width="15%">' + indentstr + '<img src="images/icon_burst_end.png" width="32" height="32" onclick="expandBlink(' + i + ', false)" />&nbsp;' + (i+1) + '</td><td class="blink" id="blink' + (i+1) + '" onclick="summaryBlink(' + i + ')">' + blink.summary + '<br /><span class="small">' + Blink.types[blink.type-1] + '</span></td>';
			}else{
				str += '<tr><td width="15%">' + indentstr + '<img src="images/icon_burst_start.png" width="32" height="32" onclick="expandBlink(' + i + ', true)" />&nbsp;' + (i+1) + '</td><td class="blink" id="blink' + (i+1) + '" onclick="summaryBlink(' + i + ')">' + blink.summary + '<br /><span class="small">' + Blink.types[blink.type-1] + '</span></td>';
			}
		}else{
			str += '<tr><td width="15%">' + indentstr + '<img src="images/' + icon + '.png" width="32" height="32" />&nbsp;' + (i+1) + '</td><td class="blink" id="blink' + (i+1) + '" onclick="summaryBlink(' + i + ')">' + blink.summary + '<br /><span class="small">' + Blink.types[blink.type-1] + '</span></td>';
		}
		str += '<td width="28%" class="buttons">';
		if (blink.type==1 || blink.type==3){
			str += '<img src="images/btn_build.png" class="button" width="24" height="24" alt="Build unit" title="Build unit" onclick="buildUnit(' + i + ')"/>';
		}else{
			str += '<img src="images/btn_copy.png" class="button" width="24" height="24" alt="Copy blink" title="Copy blink" onclick="copyBlink(' + i + ')"/>';
		}
		if (blink.lite){
			str += '<img id="blinklite' + (i+1) + '" class="button" src="images/btn_lite_checked.png" onclick="liteBlink(\'#blinklite' + (i+1) + '\', ' + i + ')" alt="Lite" title="Click to select whether this blink is in the preview version of the course" />';
		}else{
			str += '<img id="blinklite' + (i+1) + '" class="button" src="images/btn_lite_unchecked.png" onclick="liteBlink(\'#blinklite' + (i+1) + '\', ' + i + ')" alt="Lite" title="Click to select whether this blink is in the preview version of the course" />';
		}
		if (i==0){
			str += '<img src="images/btn_up.png" class="button" width="24" height="24" alt="Up" title="Move up disabled" style="opacity:0.5;"/>';
		}else{
			str += '<img src="images/btn_up.png" class="button" width="24" height="24" alt="Up" title="Move up" onclick="moveBlink(\'up\', ' + i + ')"/>';
		}
		var noMoveDown = false;
		if (blink.type==1 || blink.type==3){
			var endIdx = -1;
			//Folder
			for(var j=i+1; j<course.count(); j++){
				if (course.blinks[j].type==(blink.type+1)){
					endIdx = j;
					break;
				}
			}
			noMoveDown = (endIdx==(course.count()-1));
		}else{
			noMoveDown = (i==(course.count()-1));
		}
		if (noMoveDown){
			str += '<img src="images/btn_down.png" class="button" width="24" height="24" alt="Down" title="Move down disabled" style="opacity:0.5;"/>';
		}else{
			str += '<img src="images/btn_down.png" class="button" width="24" height="24" alt="Down" title="Move down" onclick="moveBlink(\'down\', ' + i + ')"/>';
		}
		if (blinkEditInfo!=null && i==blinkEditInfo.index && !blinkEditInfo.growth){
			str += '<img src="images/btn_edit_close.png" class="button" width="24" height="24" alt="Edit" title="Edit" onclick="editBlink(-1)"/>';
		}else{
			str += '<img src="images/btn_edit_open.png" class="button" width="24" height="24" alt="Edit" title="Edit" onclick="editBlink(' + i + ',' + blink.id + ')"/>';
		}
		str += '<img src="images/btn_delete.png" class="button" width="24" height="24" alt="Delete" title="Delete" onclick="deleteBlink(' + i + ')"/>';
		str += '</td></tr>';
		blink.visible = true;
		if (blinkEditInfo!=null && i==blinkEditInfo.index && !blinkEditInfo.growth){
			//str += '<tr><td colspan="3"><iframe id="blink-inline" src="blink-inline.php?id=' + blinkEditInfo.id + '" class="blink-inline" onload="setIframeHeight(this.id)" ></iframe></td><tr>';
			str += '<tr><td colspan="3">' + getBlinkHTML(course.get(i)) + '</td><tr>';
		}
		folderEmpty = false;
		if (blink.type==1 || blink.type==3){
			if (blink.expanded){
				indent+=2;
				var closingType = blink.type+1;
				blink = course.get(i+1);
				if (blink.type==closingType) folderEmpty = true;
			}else{
				//Find the closing blink
				var closingtype = blink.type+1;
				do{
					i++;
					blink = course.get(i);
					if (blink!=null) blink.visible = false;
					if (i>=course.count()||blink==null) break;
				}while(blink.type!=closingtype);
				if (blink==null){
					blink = { type:4 };
					course.push(blink);
				}
			}
		}
	}
	
	str += '<tr><td colspan="3">Growth Activities</td></tr>';
	str += getGrowthActivitiesHTML();
	str += '</table>';
	
	scrollTop = $('#blinks').scrollTop();
	$('#blinks').height('auto');
	$('#blinks').html(str);
	if (blinkEditInfo!=null){
		addBlinkPanelEvents();
	}
	setTimeout(setBlinkScroll, 50);
	
	blinksModified = true;
	updateBlinkButtons();
	windowResized();
}

function getGrowthActivitiesHTML(){
	var icons = ['icon_learning_start', 'icon_learning_end', 'icon_burst_start', 'icon_burst_end', 'icon_menu', 'icon_blink'];
	var indent = 0;
	var folderEmpty = false;
	
	var str = "";
	for(var i=0; i<course.countGA(); i++){
		var blink = course.getGA(i);
		var icon = (blink.type<icons.length) ? icons[blink.type-1] : 'icon_blink';
		var canCopy = (icon=='icon_blink');
		if (canCopy && blink.json!=null){
			try{
				var obj = JSON.parse(blink.json);
			 	if (Object.keys(obj).length>1) icon += "_edited"; 
			}catch(e){
				console.log("Problem parsing JSON index:" + i);
			}
		}
		var summary = (blink.summary==null) ? 'No summary' : blink.summary;
		blink.visible = false;
		if (blink.type==2 || blink.type==4){ 
			indent-=2; 
			if (folderEmpty){
				//Show empty row
				str += '<tr><td>&nbsp;</td><td><span class="small">Empty</span></td><td class="buttons">&nbsp;</td>';
				folderEmpty = false;
			}
			blink.visible = blink.expanded;
			continue; 
		}
		var indentstr = "";
		for(var j=0; j<indent; j++) indentstr += "&nbsp;";
		if (blink.type==1){
			if (blink.expanded){
				str += '<tr><td width="15%">' + indentstr + '<img src="images/icon_learning_end.png" width="32" height="32" onclick="expandBlink(' + i + ', false, true)" />&nbsp;' + (i+1) + '</td><td class="blink" id="ga' + (i+1) + '">' + blink.summary + '<br /><span class="small">' + Blink.types[blink.type-1] + '</span></td>';
			}else{
				str += '<tr><td width="15%">' + indentstr + '<img src="images/icon_learning_start.png" width="32" height="32" onclick="expandBlink(' + i + ', true, true)" />&nbsp;' + (i+1) + '</td><td class="blink" id="ga' + (i+1) + '">' + blink.summary + '<br /><span class="small">' + Blink.types[blink.type-1] + '</span></td>';
			}
		}else if (blink.type==3){
			if (blink.expanded){
				str += '<tr><td width="15%">' + indentstr + '<img src="images/icon_burst_end.png" width="32" height="32" onclick="expandBlink(' + i + ', false, true)" />&nbsp;' + (i+1) + '</td><td class="blink" id="ga' + (i+1) + '" onclick="summaryBlink(' + i + ', true)">' + blink.summary + '<br /><span class="small">' + Blink.types[blink.type-1] + '</span></td>';
			}else{
				str += '<tr><td width="15%">' + indentstr + '<img src="images/icon_burst_start.png" width="32" height="32" onclick="expandBlink(' + i + ', true, true)" />&nbsp;' + (i+1) + '</td><td class="blink" id="ga' + (i+1) + '" onclick="summaryBlink(' + i + ', true)">' + blink.summary + '<br /><span class="small">' + Blink.types[blink.type-1] + '</span></td>';
			}
		}else{
			str += '<tr><td width="15%">' + indentstr + '<img src="images/' + icon + '.png" width="32" height="32" />&nbsp;' + (i+1) + '</td><td class="blink" id="ga' + (i+1) + '" onclick="summaryBlink(' + i + ', true)">' + blink.summary + '<br /><span class="small">' + Blink.types[blink.type-1] + '</span></td>';
		}
		str += '<td width="28%" class="buttons">';
		if (blink.lite){
			str += '<img id="galite' + (i+1) + '" class="button" src="images/btn_lite_checked.png" onclick="liteBlink(\'#galite' + (i+1) + '\', ' + i + ', true)" alt="Lite" title="Click to select whether this blink is in the preview version of the course" />';
		}else{
			str += '<img id="galite' + (i+1) + '" class="button" src="images/btn_lite_unchecked.png" onclick="liteBlink(\'#galite' + (i+1) + '\', ' + i + ', true)" alt="Lite" title="Click to select whether this blink is in the preview version of the course" />';
		}
		if (i==0){
			str += '<img src="images/btn_up.png" class="button" width="24" height="24" alt="Up" title="Move up disabled" style="opacity:0.5;"/>';
		}else{
			str += '<img src="images/btn_up.png" class="button" width="24" height="24" alt="Up" title="Move up" onclick="moveBlink(\'up\', ' + i + ', true)"/>';
		}
		var noMoveDown = false;
		if (blink.type==1 || blink.type==3){
			var endIdx = -1;
			//Folder
			for(var j=i+1; j<course.countGA(); j++){
				if (course.growth[j].type==(blink.type+1)){
					endIdx = j;
					break;
				}
			}
			noMoveDown = (endIdx==(course.countGA()-1));
		}else{
			noMoveDown = (i==(course.countGA()-1));
		}
		if (noMoveDown){
			str += '<img src="images/btn_down.png" class="button" width="24" height="24" alt="Down" title="Move down disabled" style="opacity:0.5;"/>';
		}else{
			str += '<img src="images/btn_down.png" class="button" width="24" height="24" alt="Down" title="Move down" onclick="moveBlink(\'down\', ' + i + ', true)"/>';
		}
		if (blinkEditInfo!=null && i==blinkEditInfo.index && blinkEditInfo.growth){
			str += '<img src="images/btn_edit_close.png" class="button" width="24" height="24" alt="Edit" title="Edit" onclick="editBlink(-1,' + blink.id + ', true)"/>';
		}else{
			str += '<img src="images/btn_edit_open.png" class="button" width="24" height="24" alt="Edit" title="Edit" onclick="editBlink(' + i + ',' + blink.id + ', true)"/>';
		}
		str += '<img src="images/btn_delete.png" class="button" width="24" height="24" alt="Delete" title="Delete" onclick="deleteBlink(' + i + ', true)"/>';
		str += '</td></tr>';
		blink.visible = true;
		if (blinkEditInfo!=null && i==blinkEditInfo.index && blinkEditInfo.growth){
			//str += '<tr><td colspan="3"><iframe id="blink-inline" src="blink-inline.php?id=' + blinkEditInfo.id + '" class="blink-inline" onload="setIframeHeight(this.id)" ></iframe></td><tr>';
			str += '<tr><td colspan="3">' + getBlinkHTML(course.getGA(i)) + '</td><tr>';
		}
		folderEmpty = false;
		if (blink.type==1 || blink.type==3){
			if (blink.expanded){
				indent+=2;
				var closingType = blink.type+1;
				blink = course.getGA(i+1);
				if (blink.type==closingType) folderEmpty = true;
			}else{
				//Find the closing blink
				var closingtype = blink.type+1;
				do{
					i++;
					blink = course.getGA(i);
					blink.visible = false;
					if (i>=course.countGA()) break;
				}while(blink.type!=closingtype);
			}
		}
	}
	
	return str;
}

function setBlinkScroll(){
	$('#blinks').scrollTop(scrollTop);
}

function update(mode){
	var elm = $('#' + mode );
	console.log('update(' + mode + ') value:' + elm[0].value);
	
	switch(mode){
		case 'title':
		course.title = elm[0].value;
		break;
		case 'summary':
		course.summary = elm[0].value;
		break;
		case 'description':
		course.description = elm[0].value;
		break;
	}
	
	$.post("course_update.php", { id: course.id, type: mode, value: elm[0].value })
	  .done(function( msg ) {
			console.log( "Course update: " + msg );
	  });
}

function getDocHeight(doc) {
    doc = doc || document;
    // from http://stackoverflow.com/questions/1145850/get-height-of-entire-document-with-javascript
    var body = doc.body, html = doc.documentElement;
    var height = Math.max( body.scrollHeight, body.offsetHeight, 
        html.clientHeight, html.scrollHeight, html.offsetHeight );
    return height;
}

function setIframeHeight(id) {
    var ifrm = document.getElementById(id);
    var doc = ifrm.contentDocument? ifrm.contentDocument: ifrm.contentWindow.document;
    ifrm.style.visibility = 'hidden';
    ifrm.style.height = "10px"; // reset to minimal height in case going from longer to shorter doc
    // some IE versions need a bit added or scrollbar appears
    ifrm.style.height = getDocHeight( doc ) + 4 + "px";
    ifrm.style.visibility = 'visible';
	windowResized();
}

function windowResized(){
	var height = $(window).height() - $('.header').height() - $('.footer').height();
    $('.content').height(height);
	$('#blinks').height('auto');
	var blinks_height = $('#blinks').height();
	var position = $('#blinks').position();
	var available_height = height - position.top + $('.header').height();
	if (blinks_height>available_height) $('#blinks').height(available_height);
	$('#overview').height('auto');
	var overviewDisplay = $('#overview').css('display');
	if (overviewDisplay!='none'){
		var overview_height = $('#overview').height();
		position = $('#overview').position();
		available_height = height - position.top + $('.header').height();
		if (overview_height>available_height) $('#overview').height(available_height);
	}
}