// JavaScript Document

function showConversationBlink(){
	$("blinks").attr("overflow", "hidden");
	$('#blink-content').html(getConversationBlink());
	setTimeout(setScrollHeight, 10);
}

function addConversationEvents(blink){
	for(var i=0; i<blink.conversation.length; i++){
		elm = $('#conversation' + (i+1));	
		elm.keyup(function(){
			//console.log("Keyup " + this.id);
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink conversation changed " + this.value);
			var idx = parseInt(this.id.substr(12));
			blink.conversation[idx-1].text = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		$('#conversation-image-form' + (i+1)).ajaxForm(function(data) {
			console.log("Conversation image form " + data); 
			var json = JSON.parse(data);
			if (json.success){
				var src = "../courses/images/" + json.path;
				blinkData.conversation[json.id-1].image = json.path;
				var elm = $('#conversation-image' + json.id);
				if (elm!=null) elm.attr("src", src+"?timestamp=" + new Date().getTime());
				blinkModified=true;
				updateBlinkPanelButtons();
			}
		});
		
		$('#conversation-sound-form' + (i+1)).ajaxForm(function(data) {
			console.log("Conversation sound form " + data); 
			var json = JSON.parse(data);
			if (json.success){
				var src = "../courses/sounds/" + json.path;
				blinkData.conversation[json.id-1].sound = json.path;
				var elm = $('#conversation-sound' + json.id);
				if (elm!=null) elm.attr("src", src+"?timestamp=" + new Date().getTime());
				blinkModified=true;
				updateBlinkPanelButtons();
			}
		});
	}
	
	addFeedbackEvents(true, false, false);
}

function getConversationBlink(){
	var blink = blinkData;
	if (blink.conversation==null){
		blink.conversation = new Array();
		blink.feedback = "";
	}
	var str = '<table width="100%"><tr><td class="left" width="20%">Conversation</td><td></td><td class="buttons">';
	str += '<a href="#"><img class="button" src="images/btn_plus.png" title="Add Section" onclick="conversationAddSection()" /></a></td></tr>';
	for(var i=0; i<blink.conversation.length; i++){
		var section = blink.conversation[i];
		str += '<tr><td class="left">Section ' + (i+1) + '</td>';
		str += '<td><textarea id="conversation' + (i+1) + '" style="width:100%;">' + section.text + '</textarea><br>';
		str += '<form id="conversation-image-form' + (i+1) + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
		str += 'Choose Image<input type="file" name="image"/>';
		str += '<input type="hidden" name="method" value="conversation_image" />';
		str += '<input type="hidden" name="id" value="' + (i+1) + '" />';
		str += '<input type="submit" value="Upload" />';
		str += '</form>';
		str += '<br/><img id="conversation-image' + (i+1) + '" src="../courses/images/' + section.image + '" /><br>';
		str += '<form id="conversation-sound-form' + (i+1) + '" method="post" action="blink_update.php" enctype="multipart/form-data">';
		str += 'Choose Sound<input type="file" name="sound"/>';
		str += '<input type="hidden" name="method" value="sound" />';
		str += '<input type="hidden" name="id" value="' + (i+1) + '" />';
		str += '<input type="submit" value="Upload" />';
		str += '</form>';
		str += '<audio id="conversation-sound' + (i+1) + '" controls><source src="../courses/sounds/' + section.sound + '" type="audio/mpeg"></audio>';
		str += '<td class="buttons">';
		str += '<img class="button" src="images/btn_delete.png" title="Delete this statement" onclick="conversationDeleteSection(' + i + ')" >';
		str += '<img class="button" src="images/btn_delete_image.png" title="Delete image" onclick="conversationDeleteImage(' + i + ')" >';
		str += '<img class="button" src="images/btn_delete_sound.png" title="Delete audio" onclick="conversationDeleteSound(' + i + ')" >';
		str += '</td></tr>';
	}

	str += getFeedbackRows(true, false, false);
	str += '</table>';
	return str;
}

function conversationAddSection(){
	var blink = blinkData;
	var section = { text:"", image:"", sound:"" };
	blink.conversation.push( section );
	showConversationBlink();
	addConversationEvents(blinkData);
	blinkModified = true;
	updateBlinkPanelButtons();
}

function conversationDeleteSection(idx){
	var blink = blinkData;
	blink.conversation.splice(idx, 1);
	showConversationBlink();
	addConversationEvents(blinkData);
	blinkModified = true;
	updateBlinkPanelButtons();
}

function conversationDeleteImage(idx){
	blinkData.conversation[idx].image = "";
	showConversationBlink();
	addConversationEvents(blinkData);
}

function conversationDeleteSound(idx){
	blinkData.conversation[idx].sound = "";
	showConversationBlink();
	addConversationEvents(blinkData);
}