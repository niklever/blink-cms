// JavaScript Document
function getInputBlink(){
	var blink = blinkData;
	var prompt = (blink.input!=null) ? blink.input.prompt : "";
	var sound = (blink.input!=null && blink.input.sound!=null) ? blink.input.sound : "";
	var prompt_title = (blink.input!=null) ? 'Prompt' : 'New';
	var str = '<table width="100%"><tr><td class="left" width="20%">' + prompt_title + '</td>';
    str += '<td>';
    str += '<textarea id="blink-input-prompt" placeholder="Enter a prompt here" style="width:100%; height:60px;">' + prompt + '</textarea>';
	if (blink.input==null){
		str += '<select id="blink-input-type">';
		str += '<option>Choose an input type</option>';
		str += '<option>Text</option>';
		str += '<option>Words</option>';
		str += '<option>Number</option>';
		str += '<option>Date</option>';
		str += '<option>True or False</option>';
		str += '<option>Slider</option>';
		str += '<option>Slider with text</option>';
		str += '</select>';
	}
    str += '<td class="buttons" width="10%">';
	//if (blink.input!=null && blink.input.locked){
	//	str += '<a href="#"><img class="button" id="input-locked" src="images/btn_locked.png" title="Toggle Locked" onclick="inputLocked()" ></a>';
	//}else{
	//	str += '<a href="#"><img class="button" id="input-locked" src="images/btn_unlocked.png" title="Toggle Locked" onclick="inputLocked()" ></a>';
	//}
	if (blink.input==null){
		 str += '<a href="#"><img class="button" src="images/btn_create.png" title="Initialize input" onclick="createInput()" /></a></td></tr>';
	}else{
		str += getPromptSoundHTML(blink.input);
    	str += getInputHTML();
	}
	
	str += '</table>';
	return str;
}

function showInputBlink(){
	$('#blink-content').html(getInputBlink());
	$("blinks").attr("overflow", "hidden");
	setTimeout(setScrollHeight, 10);
}

function addInputEvents(blink){	
	$("#input-prompt").keyup(function(){
		$("#input-prompt").blur();
		$("#input-prompt").focus();
	});

	$('#input-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.input.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
		
	switch(blink.input.type){ 
		case 2://Word
		var elm = $('#input-word-count');
		elm.spinner({min:2, max:20, stop: function( event, ui ) {
			blink.input.count = $('#input-word-count').spinner("value");
			console.log("Blink input count changed to " + blink.input.count);
			blinkModified=true;
			updateBlinkPanelButtons(); 
		}});
		elm.spinner("value", blink.input.count);
		elm.width(60);
		
		var elm = $('#input-word-characters');
		elm.spinner({min:4, max:30, stop: function( event, ui ) {
			blink.input.characters = $('#input-word-characters').spinner("value");
			console.log("Blink input characters changed to " + blink.input.characters);
			blinkModified=true;
			updateBlinkPanelButtons(); 
		}});
		elm.spinner("value", blink.input.characters);
		elm.width(60);
		break;
		case 3://Number
		$("#input-number-text").keyup(function(){
			$("#input-number-text").blur();
			$("#input-number-text").focus();
		});
	
		$('#input-number-text').change( function(){
			//console.log("blink prompt changed " + this.value);
			blink.input.text = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		for(var i=0; i<blink.input.numbers.length; i++){
			var elm = $('#input-number-value' + (i+1));
			elm.spinner();
			elm.on( "spinchange", function( event, ui ) { 
				var idx = parseInt(event.target.id.substr(18));
				var elm = $('#' + event.target.id);
				blink.input.numbers[idx-1].value = elm.spinner("value");
				console.log("value changed");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			elm.spinner("value", blink.input.numbers[i].value);
			elm.width(60);
			
			var elm = $('#input-number-min' + (i+1));
			elm.spinner();
			elm.on( "spinchange", function( event, ui ) { 
				var idx = parseInt(event.target.id.substr(16));
				var elm = $('#' + event.target.id);
				blink.input.numbers[idx-1].min = elm.spinner("value");
				console.log("min changed");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			elm.spinner("value", blink.input.numbers[i].min);
			elm.width(60);
			
			var elm = $('#input-number-max' + (i+1));
			elm.spinner();
			elm.on( "spinchange", function( event, ui ) { 
				var idx = parseInt(event.target.id.substr(16));
				var elm = $('#' + event.target.id);
				blink.input.numbers[idx-1].max = elm.spinner("value");
				console.log("max changed");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			elm.spinner("value", blink.input.numbers[i].max);
			elm.width(60);
		}
		
		break;
		case 4://Date
		var elm = $('#input-date-range');
		elm.on("click", function() {
			blink.input.date.range = $(this).is(':checked');
			showInputBlink();
			addInputEvents(blink);
			return;
		});
		
		if (!blink.input.date.range){
			var elm = $('#input-date-value');
			elm.datepicker({changeMonth: true, changeYear: true});
			elm.change(function( ) { 
				blink.input.date.value = this.value;
				console.log("value changed");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			if (blink.input.date!=null) elm.datepicker("setDate", blink.input.date.value);
		}else{
			var elm = $('#input-date-min');
			elm.datepicker({changeMonth: true, changeYear: true});
			elm.change(function( ) { 
				blink.input.date.min = this.value;
				console.log("value changed");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			if (blink.input.date.min!=null) elm.datepicker("setDate", blink.input.date.min);
			
			var elm = $('#input-date-max');
			elm.datepicker({changeMonth: true, changeYear: true});
			elm.change(function( event) { 
				blink.input.date.max = this.value;
				console.log("value changed");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			if (blink.input.date.max!=null) elm.datepicker("setDate", blink.input.date.max);	
		}
		addFeedbackEvents();
		break;
		case 5://Boolean
		$("#input-boolean-text").keyup(function(){
			$("#input-boolean-text").blur();
			$("#input-boolean-text").focus();
		});
	
		$('#input-boolean-text').change( function(){
			console.log("blink text changed " + this.value);
			blink.input.text = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		
		for(var i=0; i<blink.input.booleans.length; i++){
			var elm = $('#input-boolean-value' + (i+1));
			elm.change(function( ) { 
				var idx = parseInt(this.id.substr(19));
				var selectedIdx = $('#' + this.id).prop('selectedIndex');
				blink.input.booleans[idx-1].value = (selectedIdx==0);
				console.log("value changed " + selectedIdx);
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			
			elm = $('#input-boolean-true' + (i+1));	
			elm.keyup(function(){
				console.log("Keyup " + this.id);
				$("#" + this.id).blur();
				$("#" + this.id).focus();
			});
		
			elm.change( function(){
				console.log("blink boolean true changed " + this.value);
				var idx = parseInt(this.id.substr(18));
				blink.input.booleans[idx-1].correct = this.value;
				blinkModified=true;
				updateBlinkPanelButtons(); 
			});
			
			elm = $('#input-boolean-false' + (i+1));	
			elm.keyup(function(){
				$("#" + this.id).blur();
				$("#" + this.id).focus();
			});
		
			elm.change( function(){
				console.log("blink boolean false changed " + this.value);
				var idx = parseInt(this.id.substr(19));
				blink.input.booleans[idx-1].wrong = this.value;
				blinkModified=true;
				updateBlinkPanelButtons(); 
			});
		}
		
		break;
		case 7:
		elm = $('#input-text');	
		elm.keyup(function(){
			$("#" + this.id).blur();
			$("#" + this.id).focus();
		});
	
		elm.change( function(){
			console.log("blink input text changed " + this.value);
			var idx = parseInt(this.id.substr(10));
			blink.input.text = this.value;
			blinkModified=true;
			updateBlinkPanelButtons(); 
		});
		case 6://Slider
		$('#input-isnumber').on("click", function() {
			blink.input.isNumber = $(this).is(':checked');
			showInputBlink();
			addInputEvents(blink);
			return;
		});
		
		if (blink.input.isNumber){
			$('#input-min').spinner({ min:0, max:1000 });
			$('#input-min').spinner("value", blink.input.min);
			$("#input-min").width(60);
			$('#input-min').on( "spinchange", function( event, ui ) { 
				blink.input.min = $('#input-min').spinner("value");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			$('#input-max').spinner({ min:0, max:1000 });
			$('#input-max').spinner("value", blink.input.max);
			$("#input-max").width(60);
			$('#input-max').on( "spinchange", function( event, ui ) { 
				blink.input.max = $('#input-max').spinner("value");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			$('#input-ideal').spinner({ min:0, max:1000 });
			$('#input-ideal').spinner("value", blink.input.ideal);
			$("#input-ideal").width(60);
			$('#input-ideal').on( "spinchange", function( event, ui ) { 
				blink.input.ideal = $('#input-ideal').spinner("value");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			
			$('#input-integer').on("click", function() {
				blink.input.integer = $(this).is(':checked');
				blinkModified=true;
				updateBlinkPanelButtons(); 
			});
		}else{
			$('#input-useimages').on("click", function() {
				blink.input.useImages = $(this).is(':checked');
				showInputBlink();
				addInputEvents(blink);
				return;
			});
		
			$('#input-ideal').spinner({ min:0, max:100 });
			$('#input-ideal').spinner("value", blink.input.ideal);
			$("#input-ideal").width(60);
			$('#input-ideal').on( "spinchange", function( event, ui ) { 
				blink.input.ideal = $('#input-ideal').spinner("value");
				blinkModified=true;
				updateBlinkPanelButtons(); 
			} );
			
			if (blink.input.useImages){
				var elm = $('#input-leftimage-form');
				elm.ajaxForm(function(data) {
					console.log("Left image form " + data); 
					var json = JSON.parse(data);
					if (json.success){
						var src = "../courses/images/" + json.path;
						blinkData.input.left = json.path;
						var elm = $('#input-leftimage');
						if (elm!=null){
							elm.attr("src", src+"?timestamp=" + new Date().getTime());
						}
						blinkModified=true;
						updateBlinkPanelButtons();
					}
				});
				var elm = $('#input-rightimage-form');
				elm.ajaxForm(function(data) {
					console.log("Right image form " + data); 
					var json = JSON.parse(data);
					if (json.success){
						var src = "../courses/images/" + json.path;
						blinkData.input.right = json.path;
						var elm = $('#input-rightimage');
						if (elm!=null){
							elm.attr("src", src+"?timestamp=" + new Date().getTime());
						}
						blinkModified=true;
						updateBlinkPanelButtons();
					}
				});
			}else{
				$("#input-left").keyup(function(){
					$("#input-left").blur();
					$("#input-left").focus();
				});
				
				$('#input-left').change( function(){
					console.log("blink left changed " + this.value);
					blink.input.left = this.value;
					blinkModified=true;
					updateBlinkPanelButtons(); 
				});
				
				$("#input-right").keyup(function(){
					$("#input-right").blur();
					$("#input-right").focus();
				});
			
				$('#input-right').change( function(){
					console.log("blink right changed " + this.value);
					blink.input.right = this.value;
					blinkModified=true;
					updateBlinkPanelButtons(); 
				});
			}
		}
		break;
	}
	
	$("#blink-input-prompt").keyup(function(){
		$("#blink-input-prompt").blur();
		$("#blink-input-prompt").focus();
	});

	$('#blink-input-prompt').change( function(){
		console.log("blink prompt changed " + this.value);
		blink.input.prompt = this.value;
		blinkModified=true;
		updateBlinkPanelButtons(); 
	});
	
	addFeedbackEvents();
	addPromptSoundEvents(blink.input);
}
		
function getInputHTML(){
	var blink = blinkData;
	var str = "";
	if (blink.input!=null){
		switch(blink.input.type){
			case 1://Text
			//Nothing to do
			break;
			case 2://Words
			if (blink.input.time==null) blink.input.time = 0;
			if (blink.input.in==null) blink.input.in = 0;
			if (blink.input.out==null) blink.input.out = 0;
			str += '<tr><td class="left">Word</td><td>Number of words <input id="input-word-count">&nbsp;&nbsp; Character limit <input id="input-word-characters"></td><td class="buttons">';
			str += '<img class="button" id="word_time" src="images/btn_time' + blink.input.time + '.png" >';
			str += '<img class="button" src="images/btn_timer.png" title="Set timer" onclick="inputWordTimer()" >';
			str += '<img id="word_anim_in" class="button" src="images/btn_anim' + blink.input.in + '.png" title="Set animation in" onclick="inputWordAnimation(false)" >';
			str += '<img id="word_anim_out" class="button" src="images/btn_anim' + blink.input.out + '.png" title="Set animation out" onclick="inputWordAnimation(true)" >';
			str += '</td></tr>';
			break;
			case 3://Number
			if (blink.input.text==null) blink.input.text = "";
			if (blink.input.numbers==null) blink.input.numbers = new Array();
			str += '<tr><td class="left">Text</td><td><textarea id="input-number-text" width="90%" placeholder="Enter the text to use for the input for each number use ##x## where x is the number in the list below.">' + blink.input.text + '</textarea></td><td class="buttons">';
			str += '<img class="button" id="word_time" src="images/btn_plus.png" title="Add number" onclick="inputAddNumber()"></td></tr>';
			for(var i=0; i<blink.input.numbers.length; i++){
				str += '<tr><td class="left">Number ' + (i+1) + '</td>';
				str += '<td>Value <input id="input-number-value' + (i+1) + '"> Min <input id="input-number-min' + (i+1) + '"> Max <input id="input-number-max' + (i+1) + '"></td>';
				str += '<td class="buttons"><img class="button" src="images/btn_delete.png" title="Delete this number" onclick="inputDeleteNumber(' + i + ')" ></td></tr>';
			}
			break;
			case 4://Date
			if (blink.input.date==null){
				var now = new Date();
				var date = now.getDate();
				var month = now.getMonth() + 1;
				var year = now.getFullYear();
				var dateStr = (month<10) ? ('0' + month + '/') : (month + '/');
				dateStr += (date<10) ? ('0' + date + '/' + year) : (date + '/' + year);
				
				blink.input.date = { value:dateStr, min:dateStr, max:dateStr, range:false };
			}
			str += '<tr><td class="left">Date</td>';
			if (blink.input.date.range==null) blink.input.date.range = false;
			if (blink.input.date.range){
				str += '<td> Range? <input id="input-date-range" type="checkbox" checked>';
				str += ' Min <input id="input-date-min"> Max <input id="input-date-max"></td>';
			}else{
				str += '<td> Range? <input id="input-date-range" type="checkbox"> Date <input id="input-date-value"></td>';
			}
			str += '<td class="buttons"></td></tr>';
			break;
			case 5://True or False
			if (blink.input.text==null) blink.input.text = "";
			if (blink.input.booleans==null || blink.input.booleans.length==0){
				blink.input.booleans = new Array({value:true, correct:"True", wrong:"False"});
			}
			//str += '<tr><td class="left">Text</td><td><textarea id="input-boolean-text" width="90%" placeholder="Enter the text to use for the input for this boolean.">' + blink.input.text + '</textarea></td><td class="buttons">';
			//str += '<img class="button" id="word_time" src="images/btn_plus.png" title="Add Boolean" onclick="inputAddBoolean()"></td></tr>';
			for(var i=0; i<blink.input.booleans.length; i++){
				str += '<tr><td class="left">Boolean</td>';
				str += '<td>Correct button<select id="input-boolean-value' + (i+1) + '">';
				if (blink.input.booleans[i].value){
					str += '<option selected>True</option>';
					str += '<option>False</option>';
				}else{
					str += '<option>True</option>';
					str += '<option selected>False</option>';
				} 
				str += '</select>';
				str += ' True display word<input id="input-boolean-true' + (i+1) + '" type="text" value="' + blink.input.booleans[i].correct + '">';
				str += ' False display word<input id="input-boolean-false' + (i+1) + '" type="text" value="' + blink.input.booleans[i].wrong + '">';
				str += '<td class="buttons">&nbsp;</td></tr>';
				str += '</tr>';
			}
			break;
			case 7:
			if (blink.input.text==null) blink.input.text = "";
			str += '<tr><td class="left">Text</td><td><input id="input-text" type="text" style="width:100%;"></td><td class="button"></td></tr>';
			case 6://Slider
			if (blink.input.isNumber==null) blink.input.isNumber = false;
			var checked = (blink.input.isNumber) ? "checked" : "";
			str += '<tr><td class="left">Slider</td><td>Is number? <input id="input-isnumber" type="checkbox" ' + checked + '>';
			if (blink.input.isNumber){
				if (blink.input.min==null) blink.input.min = 0;
				if (blink.input.max==null) blink.input.max = 0;
				if (blink.input.ideal==null) blink.input.ideal = 0;
				if (blink.input.integer==null) blink.input.integer = true;
				str += '&nbsp;Min&nbsp;<input id="input-min">';
				str += '&nbsp;Max&nbsp;<input id="input-max">';
				str += '&nbsp;Ideal&nbsp;<input id="input-ideal">';
				checked = (blink.input.integer) ? "checked" : "";
				str += '&nbsp;Integer? <input id="input-integer" type="checkbox" ' + checked + '></td>';
				str += '<td class="buttons">&nbsp;</td></tr>';
			}else{
				if (blink.input.useImages==null) blink.input.useImages = false;
				checked = (blink.input.useImages) ? "checked" : "";
				str += '&nbsp;Use images? <input id="input-useimages" type="checkbox" ' + checked + '>';
				str += '&nbsp;Ideal&nbsp;<input id="input-ideal"></td>';
				str += '<td class="buttons">&nbsp;</td></tr>';
				if (blink.input.left==null) blink.input.left = "";
				if (blink.input.right==null) blink.input.right = "";
				if (blink.input.ideal==null) blink.input.ideal = 50;
				if (!blink.input.useImages){
					str += '<tr><td class="left">Left side</td><td><input id="input-left" type="text" value="' + blink.input.left + '" style="width:100%;"></td><td class="buttons">&nbsp;</td></tr>';
					str += '<tr><td class="left">Right side</td><td><input id="input-right" type="text" value="' + blink.input.right + '" style="width:100%;"></td><td class="buttons">&nbsp;</td></tr>';
				}else{
					str += '<tr><td class="left">Left side</td>';
					str += '<td><form id="input-leftimage-form" method="post" action="blink_update.php" enctype="multipart/form-data">';
					str += '<input type="file" name="image"/>';
					str += '<input type="hidden" name="method" value="intro_image" />';
					str += '<input type="submit" value="Upload" />';
					str += '</form>';
					str += '<br/><img id="input-leftimage" src="../courses/images/' + blink.input.left + '" /></td><td class="buttons"></td></tr>';
					str += '<tr><td class="left">Right side</td>';
					str += '<td><form id="input-rightimage-form" method="post" action="blink_update.php" enctype="multipart/form-data">';
					str += '<input type="file" name="image"/>';
					str += '<input type="hidden" name="method" value="intro_image" />';
					str += '<input type="submit" value="Upload" />';
					str += '</form>';
					str += '<br/><img id="input-rightimage" src="../courses/images/' + blink.input.right + '" /></td><td class="buttons"></td></tr>';
				}
			}
			
			break;
		}
	}
	str += getFeedbackRows();
			
	return str;
}

function inputAddBoolean(){
	var blink = blinkData;
	blink.input.booleans.push( { value:true, correct:"True", wrong:"False" });
	showInputBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputDeleteBoolean(idx){
	var blink = blinkData;
	blink.input.booleans.splice(idx, 1);
	showInputBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputAddNumber(){
	var blink = blinkData;
	blink.input.numbers.push( { value:0 });
	showInputBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputDeleteNumber(idx){
	var blink = blinkData;
	blink.input.numbers.splice(idx, 1);
	showInputBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputAddDate(){
	var blink = blinkData;
	blink.input.dates.push( {  });
	showInputBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputDeleteDate(idx){
	var blink = blinkData;
	blink.input.dates.splice(idx, 1);
	showInputBlink();
	addBlinkPanelEvents();
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputWordTimer(){
	var blink = blinkData;
	blink.input.time++;
	if (blink.input.time>8) blink.input.time = 0;
	$('#word_time').attr('src', 'images/btn_time' + blink.input.time + '.png');
	blinkModified = true;
	updateBlinkPanelButtons();
}

function inputWordAnimation(anim_out){
	var blink = blinkData;
	if (anim_out){
		blink.input.out++;
		if (blink.input.out>5) blink.input.out = 0;
		$('#word_anim_out').attr('src', 'images/btn_anim' + blink.input.out + '.png');
	}else{
		blink.input.in++;
		if (blink.input.in>5) blink.presentation[idx].in = 0;
		$('#word_anim_in').attr('src', 'images/btn_anim' + blink.input.in + '.png');
	}
	blinkModified = true;
	updateBlinkPanelButtons();
}

function createInput(){
	if (blinkEditInfo!=null){
		console.log(blinkEditInfo.growth);
		if (blinkEditInfo.growth){
			var tmp = course.getGA(blinkEditInfo.index);
			try{
				blinkData = JSON.parse(tmp.json);
			}catch(e){
				console.log("createInput: Problem parsing json");
			}
		}
	}
	var blink = blinkData;
	var type = $('#blink-input-type option:selected').index();
	console.log("input type selected is " + type);
	if (type<1){
		showFeedback("Please select an input type");
	}else{
		var prompt = $('#blink-input-prompt')[0].value;
		blink.input = { type:type, prompt:prompt, locked:false };
		if (type==2){
			blink.input.in = 0;
			blink.input.out = 0;
			blink.input.time = 0;
			blink.input.count = 6;
			blink.input.characters = 12;
		}
		blinkModified = true;
		updateBlinkPanelButtons();
		showInputBlink();
		addInputEvents(blink);
		blinkData = blink;
	}
}

function inputLocked(){
	var blink = blinkData;
	blink.input.locked = !blink.input.locked;
	if (blink.input.locked){
		$('#input-locked').attr('src', 'images/btn_locked.png');
	}else{
		$('#input-locked').attr('src', 'images/btn_unlocked.png');
	}
}