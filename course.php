<? 
	require_once('connect.php');
	require_once('simplejson.php');
	$msg = "";
	
	session_start();
	$loggedIn = $_SESSION['loggedIn'];
	$admin = $_SESSION['admin'];
	$name = $_SESSION['name'];
	$timeoffset = (isset($_SESSION['timeoffset'])) ? $_SESSION['timeoffset'] : 0;
	
	if (isset($loggedIn) && $loggedIn && $admin){
		$method = $_REQUEST['method'];
		$id = $_REQUEST['id'];
		if (isset($method)){
			switch($method){
				case 'new':
				$title = mysql_real_escape_string($_REQUEST['title']);
				$summary = mysql_real_escape_string($_REQUEST['summary']);
				$description = mysql_real_escape_string($_REQUEST['description']);
				$editDate = date('Y-m-d H:i:s', time());
				$sql = "INSERT INTO course (title, summary, description, editDate) VALUES('$title', '$summary', '$description', '$editDate')";
				$result = mysql_query($sql);
				if ($result){
					$id = mysql_insert_id();
					$iconUrl = saveImage('icon', $_FILES['icon'], $id);
					$imageUrl = saveImage('image', $_FILES['image'], $id);
					$sql = "UPDATE course SET iconURL='$iconUrl',imageURL='$imageUrl' WHERE id=$id";
					$result = mysql_query($sql);
				}else{
					$msg .= 'Problem with query<br/>'.$sql.'<br/>';
				}
				break;
			}
		}
			
		$sql = "SELECT * FROM course WHERE id=$id";
		$result = mysql_query($sql);
		$course = mysql_fetch_assoc($result);
		$modifiedTime = strtotime($course['editDate']) - $timeoffset*60;	
		$modifiedDate = date('h:ia M d - Y', $modifiedTime);
		$sql = "SELECT * FROM blink WHERE courseId=$id  AND growth_activity=FALSE ORDER BY `index` ASC";
		$result = mysql_query($sql);
		$blinks = array();
		if ($result) while($row=mysql_fetch_assoc($result)){
			$blinks[] = $row;//str_replace('&#34;', '\"', $row) ;
		}
		$sql = "SELECT * FROM blink WHERE courseId=$id  AND growth_activity=TRUE ORDER BY `index` ASC";
		$result = mysql_query($sql);
		$growth = array();
		if ($result) while($row=mysql_fetch_assoc($result)){
			$growth[] = $row;//str_replace('&#34;', '\"', $row) ;
		}
		//var_dump($blinks);	
	}
	
	$timestamp = time();
		
	mysql_close($conn);
	
function saveImage($type, $data, $id){
	global $msg;
	
	if ($data['type'] != "image/jpeg"){
		$msg .= "$type is not a jpeg image<br/>"; 
		return "";
	}
	
	$name = "$type$id.jpg";
	$path = "../courses/images/$name";
	
	// VARS FOR FILE 
	$dwidth = ($type=='image') ? 480 : 127;
	$dheight = ($type=='image') ? 320 : 127;

	$img = imagecreatefromjpeg($data['tmp_name']);
	$swidth = imagesx( $img );
	$sheight = imagesy( $img );

	$tmpimg = imagecreatetruecolor( $dwidth, $dheight );
	imagecopyresampled( $tmpimg, $img, 0, 0, 0, 0, $dwidth, $dheight, $swidth, $sheight );

	// Save image into a file.
	imagejpeg( $tmpimg, $path, 80);

	// release the memory
	imagedestroy($tmpimg);
	imagedestroy($img);

	return $name;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/blink_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- Preview at https://64.34.168.15:8443/sitepreview/http/blinktrainingsystem.com -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Blink Admin - Course - <? echo $course['title']; ?>( <? echo $course['id']; ?> )</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link href="main.css" rel="stylesheet" type="text/css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
  $(function() {
    $( document ).tooltip();
  });
  </script>
<!-- InstanceBeginEditable name="head" -->
<style>
  #blinks .ui-selecting { background: #FECA40; }
  #blinks .ui-selected { background: #F39814; color: white; }
</style>
<script src="js/jquery.form.js"></script> 
<script src="js/course.js"></script>
<script src="js/course_gui.js"></script>
<script src="js/blink_gui.js"></script>
<script src="js/blink_simon.js"></script>
<script src="js/blink_presentation.js"></script>
<script src="js/blink_intro.js"></script>
<script src="js/blink_input.js"></script>
<script src="js/blink_orderitems.js"></script>
<script src="js/blink_ebook.js"></script>
<script src="js/blink_statement.js"></script>
<script src="js/blink_wordfill.js"></script>
<script src="js/blink_dragdrop.js"></script>
<script src="js/blink_question.js"></script>
<script src="js/blink_wordheat.js"></script>
<script src="js/blink_video.js"></script>
<script src="js/blink_rotator.js"></script>
<script src="js/blink_thisthat.js"></script>
<script src="js/blink_catch.js"></script>
<script src="js/blink_conversation.js"></script>
<script src="js/blink_multiple.js"></script>
<script>
var data = new Object();
data.id = "<? echo $course['id']; ?>";
data.title = "<? echo str_replace('"', "\\\"", $course['title']); ?>";
data.summary = "<? echo str_replace('"', "\\\"", $course['summary']); ?>";
data.description = "<? echo str_replace('"', "\\\"", $course['description']); ?>";
data.iconUrl = "<? echo $course['iconURL']; ?>";
data.imageUrl = "<? echo $course['imageURL']; ?>";
data.blinks = new Array();
data.growth = new Array();
var imagePath = "<? echo $imagePath; ?>";
var blink;
<?
	foreach($blinks as $blink){
		$json = str_replace('%27', '\\\'', $blink['json']);
		echo 'blink = new Blink('.$blink['type'].',"'.str_replace('"', "\\\"", $blink['summary']).'",'.$blink['lite'].',"'.$blink['guid'].'",'.$blink['id'].',\''.$json.'\');'."\n"; 
		echo 'data.blinks.push(blink);'."\n";
	}
	foreach($growth as $blink){
		$json = str_replace('%27', '\\\'', $blink['json']);
		echo 'blink = new Blink('.$blink['type'].',"'.str_replace('"', "\\\"", $blink['summary']).'",'.$blink['lite'].',"'.$blink['guid'].'",'.$blink['id'].',\''.$json.'\');'."\n"; 
		echo 'data.growth.push(blink);'."\n";
	}
?>
var course = new Course(data);
console.log(course.toString());
var iconFile;
var imageFile;
var blinkFirst = blinkLast = blinkIdx = -1;
var blinksSelected = new Array();
var blinksModified = false;
var summary;
var loggedIn = <? echo $loggedIn; ?>;
var blinkEditInfo;
var modifiedDate = "<? echo $modifiedDate; ?>";
var iframeResizeID;
var editedBlinkIdx;
var timeoffset = <? echo $timeoffset; ?>;
var blinkData;
var blinkGrowth;

$(document).ready(function() {
	addCourseEvents();
});

String.prototype.escapeJSON = function () {
	  return this;
		//.replace(/[\n]/g, '\\n')
		//.replace(/[\r]/g, '\\r')
		//.replace(/[\t]/g, '\\t');
	};
</script>
<!-- InstanceEndEditable -->
</head>

<body>
<div class="container"> 
  <div class="header"><span class="heading">Blink Training System - CMS <!-- InstanceBeginEditable name="PageName" -->- <? echo $course['title']; ?>( <? echo $course['id']; ?> )<!-- InstanceEndEditable --></span>
    <!-- end .header --></div>
  <div class="sidebar1">
    <ul class="nav">
      <!-- InstanceBeginEditable name="sidebar" -->
      <?
	  	if ($loggedIn){
			echo '<li><a href="logout.php">Logout <span class="small">('.$name.')</span></a></li>';
      		echo '<li><a href="courses.php">Courses</a></li>';
			echo '<li><a href="usercourses.php">User Courses</a></li>';
		}else{
      		echo '<li><a href="login.php">Login</a></li>';
      		echo '<li><a href="register.php">Register</a></li>';
		}
	  ?>
      <!-- InstanceEndEditable -->
    </ul>
    <!-- end .sidebar1 --></div>
  <div class="content">
    <!-- InstanceBeginEditable name="content" -->
    <?
    	echo '<div id="content-inset">';
		if (!$loggedIn){
			//Logged in
			echo '<div id="msg" class="error" style="clear:both;">You need to be logged in to view this page.</div></div>';
		}else{
			if ($msg!=""){
				echo '<div id="msg" class="error">'.$msg.'</div>';
			}else{
				echo '<div id="msg" class="error" style="display:none;">'.$msg.'</div>';
			}
			echo '<a href="courses.php">Back</a>&nbsp;&nbsp;<a href="logout.php">Logout <span class="small">('.$name.')</span></a>&nbsp;&nbsp;';
			echo '<div id="overview-btn"><a href="#" onclick="showCourseOverview(true)">Show overview</a>&nbsp;&nbsp;<span class="small">Last modified '.$modifiedDate.'</span><br/><br/></div><div style="clear:both;"></div>';
			echo '<div id="overview" style="display:none;">';
			echo '<table width="100%">';
        	echo '<tr><td class="left" width="20%">Title</td><td><input type="text" id="title" placeholder="Enter the course title" class="text_field" value="'.$course['title'].'"/></td><td width="15%"><input type="button" value="update" onClick="update(\'title\')" /></td></tr>';
        	echo '<tr><td class="left">Summary</td><td><textarea placeholder="Enter the course summary" id="summary">'.$course['summary'].'</textarea></td><td><input type="button" value="update" onClick="update(\'summary\')" /></td></tr>';
        	echo '<tr><td class="left">Description</td><td><textarea placeholder="Enter the course description" id="description">'.$course['description'].'</textarea></td><td><input type="button" value="update" onClick="update(\'description\')" /></td></tr>';
			echo '<form id="iconForm" method="post" action="course_update.php" enctype="multipart/form-data"><input type="hidden" name="type" value="icon"><input type="hidden" name="id" value="'.$course['id'].'">';
        	echo '<tr><td class="left">Icon</td><td><img id="iconImg" src="'.$imagePath.$course['iconURL'].'?timestamp='.$timestamp.'" /></td><td><input type="file" value="update" id="icon" name="icon"/><br><input type="submit" value="upload"><br><span class="small">Icons must be jpeg images 127x127</span></td></tr></form>';
			echo '<form id="imageForm" method="post" action="course_update.php" enctype="multipart/form-data"><input type="hidden" name="type" value="image"><input type="hidden" name="id" value="'.$course['id'].'">';
        	echo '<tr><td class="left">Image</td><td><img id="imageImg" src="'.$imagePath.$course['imageURL'].'?timestamp='.$timestamp.'" width="480" height="300" /></td><td><input type="file" value="update" id="image" name="image"/><br><input type="submit" value="upload"><br><span class="small">Images must be jpegs 480x320</span></td></tr></form>';
    		echo '</table></div>';
		
			echo '<h2>Blinks</h2>';
			echo '<table width="100%">';
			echo '<tr><td class="left" width="10%">Add</td><td>';
			echo '<select id="new_blink_type">';
			echo '<option>Choose type</option>';
			echo '<option>Learning unit</option>';
			echo '<option>Burst</option>';
			echo '<option>Blink - E book</option>';
			echo '<option>Blink - Presentation</option>';
			echo '<option>Blink - Input</option>';
			echo '<option>Blink - Build a statement</option>';
			echo '<option>Blink - Word fill</option>';
			echo '<option>Blink - Drag and drop</option>';
			echo '<option>Blink - Simon</option>';
			echo '<option>Blink - Order items</option>';
			echo '<option>Blink - This or that</option>';
			echo '<option>Blink - Question</option>';
			echo '<option>Blink - Statement rotator</option>';
			echo '<option>Blink - Word heat</option>';
			echo '<option>Blink - Video</option>';
			echo '<option>Blink - Catch Game</option>';	
			echo '<option>Blink - Conversation</option>';	
			echo '<option>Blink - Multiple Answers</option>';	
			echo '</select>';
			echo '<input id="new_blink_summary" type="text" style="width:200px;" placeholder="Enter blink summary"/>';
			echo '<br/><input type="checkbox" id="blink_growth" />Growth Activity&nbsp;';
			echo '</td>';
			echo '<td width="20%" class="buttons">';
			echo '<img src="images/btn_insert_start.png" id="btn_start" class="button" width="24" height="24" alt="Insert at start" title="Insert at start" onclick="newBlink(\'start\')"/>';
			echo '<img src="images/btn_insert_before.png" id="btn_before" class="button" width="24" height="24" alt="Insert before selected" title="Insert before selected" onclick="newBlink(\'before\')"/>';
			echo '<img src="images/btn_insert_after.png" id="btn_after" class="button" width="24" height="24" alt="Insert after selected" title="Insert after selected" onclick="newBlink(\'after\')"/>';
			echo '<img src="images/btn_insert_end.png" id="btn_end" class="button" width="24" height="24" alt="Insert at end" title="Insert at end" onclick="newBlink(\'end\')"/>&nbsp;&nbsp;';
			echo '</td>';
			echo '<td width="30%" class="buttons">';
			echo '<img src="images/btn_delete.png" id="btn_delete" class="button" width="24" height="24" alt="Delete" title="Delete selected" onclick="deleteBlinks()"/>&nbsp;';
			echo '<img src="images/btn_collapse.png" id="btn_collapse" class="button" width="24" height="24" alt="Save" title="Collapse all Blinks" onclick="expandBlinks(false)"/>';
			echo '<img src="images/btn_expand.png" id="btn_expand" class="button" width="24" height="24" alt="Save" title="Expand all Blinks" onclick="expandBlinks(true)"/>&nbsp;';
			echo '<img src="images/btn_save.png" id="btn_save" class="button" width="24" height="24" alt="Save" title="Save all blinks" onclick="saveCourse(false)"/>';
			echo '<img src="images/btn_save_as.png" id="btn_save_as" class="button" width="24" height="24" alt="Save" title="Save as new course" onclick="saveCourse(true)"/>&nbsp;';
			echo '<img src="images/btn_build.png" id="btn_build" class="button" width="24" height="24" alt="Build course" title="Build course" onclick="buildCourse()" />';
			echo '</td></tr>';
			echo '</table><br />';
			echo '<div id="blinks"></div>';
			echo '</div>';
			echo '<div id="delete-blink-confirm" title="Delete blink?">';
			echo '  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This blink will be permanently deleted and cannot be recovered. Are you sure?</p>';
			echo '</div>';
			echo '<div id="delete-blinks-confirm" title="Delete blinks?">';
			echo '  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>The selected blinks will be permanently deleted and cannot be recovered. Are you sure?</p>';
			echo '</div>';
			echo '<div id="blinks-feedback" title="Course saved">';
			echo '	<div id="blinks-feedback-msg">msg</div>';
			echo '</div>';
			echo '<div id="blink-confirm-yesno" title="Confirm">';
			echo '	<div id="blink-confirm-yesno-msg">msg</div>';
			echo '</div>';
			echo '<div id="blink-feedback" title="Blink saved">';
			echo '	<div id="blink-feedback-msg">msg</div>';
			echo '</div>';
			echo '<div id="blink-iframe" class="iframeclass"></div>';
		}
	?>
    
    <!-- InstanceEndEditable -->
    <!-- end .content --></div>
  <div class="footer">
    Copyright © 2014 by Blink Training Systems, LLC
    <!-- end .footer --></div> 
  <!-- end .container --></div> 
</body>
<!-- InstanceEnd --></html>
